/* BEGIN: Keyboard
 */
function JendoKeyboard(el, args) {
    this.kbOptions = args;
    if (jendo.isNull(args)) {
        this.kbOptions = {
            // да показва ли keyboard бутона
            showButton: true,
            // default, light
            theme: 'default'
        };
    }
    else {
        if (!('showButton' in this.kbOptions)) {
            this.kbOptions.showButton = true;
        }
        if (!('theme' in this.kbOptions)) {
            this.kbOptions.theme = 'default';
        }
    }
    this.themeClass = (this.kbOptions.theme === 'default' ? 'jd-keyboard-default' :
        this.kbOptions.theme === 'light' ? 'jd-keyboard-light' : this.kbOptions.theme);
    // Keyboard
    this.characteKeys = [{
        lang: 'EN',
        keys: [
            ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'],
            ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'],
            ['z', 'x', 'c', 'v', 'b', 'n', 'm']
        ]
    }, {
        lang: 'BG',
        keys: [
            ['я', 'в', 'е', 'р', 'т', 'ъ', 'у', 'и', 'о', 'п', 'ч'],
            ['а', 'с', 'д', 'ф', 'г', 'х', 'й', 'к', 'л', 'ш', 'щ'],
            ['з', 'ь', 'ц', 'ж', 'б', 'н', 'м', 'ю']
        ]
    }];
    this.characteKeysIndex = 0;
    this.symbolKeys = [
        ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '[', ']'],
        ['@', '#', '$', '%', '_', '&', '-', '+', '(', ')', '<', '>'],
        [ ',', '/', '*', '"', '\'', ': ', '; ', '!', '?', '\\']
    ];
    this.jdEnterKey = null;
    this.jdKeyboard = this.createCharsKB(args);
    this.jdSelItem = null;
    this.isCapsLock = false;
    this.isShift = false;
    // основен елемент
    this.jdEl = el;
    var jdInput = this.jdEl.find('input');
    for (var i = 0; i < jdInput.items.length; i++) {
        this.attach(jdInput.items[i]);
    }
    var jdTTarea = this.jdEl.find('textarea');
    for (var t = 0; t < jdTTarea.items.length; t++) {
        this.attach(jdTTarea.items[t]);
    }
    var self = this;
    // затваря Keyboard
    document.addEventListener('click', function (e) {
        if (self.jdSelItem !== null && self.jdSelItem.first() !== e.target) {
            if (self.jdKeyboard !== null && !self.jdKeyboard.isContains(e.target)) {
                if (e.target.getAttribute('data-role') !== 'keyboard') {
                    self.jdSelItem = null;
                    self.hide();
                }
            }
        }
    }, true);
}

JendoKeyboard.prototype.attach = function (el) {
    var self = this;
    var jdItem = jendo(el);
    // Добавя keyboard бутона
    if (this.kbOptions.showButton) {
        var jdParent = jdItem.parent();
        var jdBtn = jendo.createButton()
            .attr('data-role', 'keyboard');
        jdBtn.append(jendo.createElement('i')
            .attr('class', 'far fa-keyboard')
            .attr('aria-hidden', 'true')
            .attr('data-role', 'keyboard')
        );
        jdParent.insertAfter(jdBtn.first(), jdItem);
        jdBtn.click(function (e) {
            if (self.jdSelItem !== null && self.jdSelItem === jdItem) {
                self.hide();
                self.jdSelItem = null;
            }
            else {
                self.jdSelItem = jdItem;
                jdItem.focus();
                self.show();
            }
        });
    }
    jdItem.focus(function (e) {
        if (self.jdSelItem === null || self.jdSelItem !== jdItem) {
            self.jdSelItem = jdItem;
            self.show();
        }
    });
}

JendoKeyboard.prototype.createCharsKB = function (args) {
    var self = this;
    var jdKBContent = jendo.isNull(this.jdKeyboard)
        ? jendo(document.body).appendDiv().class('jd-keyboard ' + this.themeClass)
        : this.jdKeyboard.html('');
    var jdKB = jdKBContent.appendDiv().class('jd-keyboard-chars');
    var characteLang = this.characteKeys[this.characteKeysIndex].lang;
    var characteKeys = this.characteKeys[this.characteKeysIndex].keys;
    for (var l = 0; l < characteKeys.length && l < 3; l++) {
        var kbLine = characteKeys[l];
        var jdLine = jdKB.appendDiv().class('jd-keyboard-row');
        if (l === 1) {
            // Caps Lock
            jdLine.appendButton().html('&#x21ea;')
                .class('jd-keyboard-key jd-keyboard-fn')
                .click(function (e) {
                    if (self.isCapsLock) {
                        self.isCapsLock = false;
                        jendo(e.target).html('&#x21ea;');
                    }
                    else {
                        self.isCapsLock = true;
                        jendo(e.target).html('&#x21eb;');
                    }
                    self.setKBUpperCase();
                });
        }
        else if (l === 2) {
            // Shift
            jdLine.appendButton().html('&#8679;')
                .class('jd-keyboard-key jd-keyboard-fn')
                .click(function (e) {
                    if (self.isShift) {
                        self.isShift = false;
                    }
                    else {
                        self.isShift = true;
                    }
                    self.setKBUpperCase();
                });
        }
        // chars
        for (var i = 0; i < kbLine.length; i++) {
            var key = kbLine[i];
            jdLine.appendButton(key)
                .class('jd-keyboard-key jd-keyboard-ch')
                .click(function (e) {
                    self.inputItemChar(e.data.key);
                }, { key: key });
        }
        // backspace
        if (l === 2) {
            jdLine.appendButton().html('&#x232B;')
                .class('jd-keyboard-key jd-keyboard-bs')
                .click(function (e) {
                    if (self.jdSelItem !== null) {
                        self.jdSelItem.focus();
                        var dItem = self.jdSelItem.first();
                        var itemText = self.jdSelItem.val();
                        var pos = dItem.selectionStart - 1;
                        self.jdSelItem.val(itemText.substring(0, pos) +
                            itemText.substring(dItem.selectionEnd, itemText.length));
                        dItem.selectionStart = pos;
                        dItem.selectionEnd = pos;
                    }
                });
        }
    }
    var jdEndLine = jdKB.appendDiv().class('jd-keyboard-row');
    jdEndLine.appendButton().html('?123')
        .class('jd-keyboard-key jd-keyboard-knm')
        .click(function (e) {
            self.createSymbolsKB(args);
        });
    jdEndLine.appendButton().html(characteLang)
        .class('jd-keyboard-key jd-keyboard-knm')
        .click(function (e) {
            self.characteKeysIndex += 1;
            if (self.characteKeysIndex >= self.characteKeys.length) {
                self.characteKeysIndex = 0;
            }
            self.createCharsKB(args);
        });
    jdEndLine.appendButton().html('&nbsp;')
        .class('jd-keyboard-key jd-keyboard-space')
        .click(function (e) {
            self.inputItemChar(' ');
        });
    // &#x21a9; 	&#9166;
    self.jdEnterKey = jdEndLine.appendButton().html('&#x21a9;')
        .class('jd-keyboard-key jd-keyboard-rn')
        .click(function (e) {
            self.hide();
        });
    return jdKBContent;
}

JendoKeyboard.prototype.createSymbolsKB = function (args) {
    var self = this;
    var jdKBContent = jendo.isNull(this.jdKeyboard)
        ? jendo(document.body).appendDiv().class('jd-keyboard')
        : this.jdKeyboard.html('');
    var jdKB = jdKBContent.appendDiv().class('jd-keyboard-chars');
    for (var l = 0; l < this.symbolKeys.length && l < 3; l++) {
        var kbLine = this.symbolKeys[l];
        var jdLine = jdKB.appendDiv().class('jd-keyboard-row');
        // symbols
        for (var i = 0; i < kbLine.length; i++) {
            var key = kbLine[i];
            jdLine.appendButton(key)
                .class('jd-keyboard-key jd-keyboard-ch')
                .click(function (e) {
                    self.inputItemChar(e.data.key);
                }, { key: key });
        }
        // backspace
        if (l === 2) {
            jdLine.appendButton().html('&#x232B;')
                .class('jd-keyboard-key jd-keyboard-bs')
                .click(function (e) {
                    if (self.jdSelItem !== null) {
                        self.jdSelItem.focus();
                        var dItem = self.jdSelItem.first();
                        var itemText = self.jdSelItem.val();
                        var pos = dItem.selectionStart - 1;
                        self.jdSelItem.val(itemText.substring(0, pos) +
                            itemText.substring(dItem.selectionEnd, itemText.length));
                        dItem.selectionStart = pos;
                        dItem.selectionEnd = pos;
                    }
                });
        }
    }
    var jdEndLine = jdKB.appendDiv().class('jd-keyboard-row');
    jdEndLine.appendButton().html('ABC')
        .class('jd-keyboard-key jd-keyboard-knm')
        .click(function (e) {
            self.createCharsKB(args);
        });
    jdEndLine.appendButton().html('&nbsp;')
        .class('jd-keyboard-key jd-keyboard-space')
        .click(function (e) {
            self.inputItemChar(' ');
        });
    jdEndLine.appendButton('.')
        .class('jd-keyboard-key jd-keyboard-ch')
        .click(function (e) {
            self.inputItemChar(e.data.key);
        }, { key: '.' });
    self.jdEnterKey = jdEndLine.appendButton().html('&#x21a9;')
        .class('jd-keyboard-key jd-keyboard-rn')
        .click(function (e) {
            self.hide();
        });
    return jdKBContent;
}

JendoKeyboard.prototype.inputItemChar = function (key) {
    if (this.jdSelItem !== null) {
        this.jdSelItem.focus();
        var dItem = this.jdSelItem.first();
        var itemText = this.jdSelItem.val();
        var iPos = dItem.selectionStart + 1;
        var iKey = key;
        if (this.isShift) {
            iKey = iKey.toUpperCase();
            this.isShift = false;
            this.setKBUpperCase();
        }
        else if (this.isCapsLock) {
            iKey = iKey.toUpperCase();
        }
        this.jdSelItem.val(itemText.substring(0, dItem.selectionStart) +
            iKey + itemText.substring(dItem.selectionEnd, itemText.length));
        dItem.selectionStart = iPos;
        dItem.selectionEnd = iPos;
    }
}

JendoKeyboard.prototype.setKBUpperCase = function () {
    if (this.isShift || this.isCapsLock) {
        this.jdKeyboard.addClass('jd-keyboard-upper');
    }
    else {
        this.jdKeyboard.removeClass('jd-keyboard-upper');
    }
}

JendoKeyboard.prototype.show = function () {
    if (this.jdKeyboard !== null) {
        this.jdKeyboard.addClass('jd-keyboard-show');
    }
}

JendoKeyboard.prototype.hide = function () {
    if (this.jdKeyboard !== null) {
        this.jdKeyboard.removeClass('jd-keyboard-show');
    }
}

JendoKeyboard.prototype.enterKey = function (func) {
    if (this.jdEnterKey !== null) {
        this.jdEnterKey.click(func);
    }
}

// Keyboard
jendo.keyboard = function (element, args) {
    if (!jendo.isNull(element)) {
        var targetElement = jendo(element);
        if (targetElement.length > 0) {
            return new JendoKeyboard(targetElement, args);
        }
    }
};

JendoSelector.prototype.keyboard = function (args) {
    if (jendo.isNull(args)) {
        args = {};
    }
    return new JendoKeyboard(this, args);
};
/* END: Keyboard
 */
