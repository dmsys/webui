/*#region Inline List
*/
function JendoInlineList(element, options) {
    // Jendo елемент
    this.jdElement = element;
    if (this.jdElement) {
        // DOM елемент
        // this.dElement = this.jdElement.first();
        this.jdList = this.jdElement.find('ul');
        if (this.jdList.length == 0) {
            this.jdList = this.jdElement.appendUL();
        }
    }
}

JendoInlineList.prototype.add = function (text, value) {
    if (this.jdList) {
        var self = this;
        var jdItem = this.jdList.appendLI();
        jdItem.attr('jd-value', value);
        jdItem.appendSpan(text);
        var jdBtn = jdItem.appendButton().html('&times;');
        jdBtn.click(function (e) {
            self.jdList.removeChild(jdItem);
        });
        return jdItem;
    }
    else {
        return undefined;
    }
}

JendoInlineList.prototype.get = function (index) {
    if (this.jdList) {
        if (index) {
            var ix = parseInt(index);
            if (isNaN(index)) {
                return null;
            }
            else {
                var cNodes = this.jdList.first().childNodes;
                var eItem = cNodes[ix];
                if (eItem) {
                    var jdItem = jendo(eItem);
                    return {
                        value: jdItem.attr('jd-value'),
                        text: jdItem.find('span').text()
                    }
                }
                else {
                    return null;
                }
            }
        }
        else {
            var items = [];
            var cNodes = this.jdList.first().childNodes;
            for (var i = 0; i < cNodes.length; i++) {
                var jdItem = jendo(cNodes[i]);
                items.push({
                    value: jdItem.attr('jd-value'),
                    text: jdItem.find('span').text()
                });
            }
            return items;
        }
    }
    else {
        return undefined;
    }
}

JendoInlineList.prototype.getValue = function (index) {
    if (this.jdList) {
        if (index) {
            var ix = parseInt(index);
            if (isNaN(index)) {
                return null;
            }
            else {
                var cNodes = this.jdList.first().childNodes;
                var eItem = cNodes[ix];
                if (eItem) {
                    var jdItem = jendo(eItem);
                    return jdItem.attr('jd-value');
                }
                else {
                    return null;
                }
            }
        }
        else {
            var items = [];
            var cNodes = this.jdList.first().childNodes;
            for (var i = 0; i < cNodes.length; i++) {
                var jdItem = jendo(cNodes[i]);
                var vl = jdItem.attr('jd-value');
                if (!jendo.isNull(vl)) {
                    items.push(vl);
                }
            }
            return items;
        }
    }
    else {
        return undefined;
    }
}

JendoInlineList.prototype.getText = function (index) {
    if (this.jdList) {
        if (index) {
            var ix = parseInt(index);
            if (isNaN(index)) {
                return null;
            }
            else {
                var cNodes = this.jdList.first().childNodes;
                var eItem = cNodes[ix];
                if (eItem) {
                    var jdItem = jendo(eItem);
                    return jdItem.find('span').text();
                }
                else {
                    return null;
                }
            }
        }
        else {
            var items = [];
            var cNodes = this.jdList.first().childNodes;
            for (var i = 0; i < cNodes.length; i++) {
                var jdItem = jendo(cNodes[i]);
                items.push(jdItem.find('span').text());
            }
            return items;
        }
    }
    else {
        return undefined;
    }
}

JendoInlineList.prototype.clear = function () {
    if (this.jdList) {
        this.jdList.text('');
    }
}

JendoInlineList.prototype.size = function () {
    if (this.jdList) {
        return this.jdList.first().childNodes.length;
    }
    else {
        return 0;
    }
}

jendo.inlinelist = function (element, args) {
    if (!jendo.isNull(element)) {
        var targetElement = jendo(element);
        if (targetElement.length > 0) {
            return new JendoInlineList(targetElement, args);
        }
        else {
            return new JendoInlineList(undefined, args);
        }
    }
    else {
        return new JendoInlineList(undefined, args);
    }
}

JendoSelector.prototype.inlinelist = function (args) {
    if (jendo.isNull(args)) {
        args = {};
    }
    return new JendoInlineList(this, args);
}
/*#endregion Inline List
*/