﻿/* BEGIN: Breadcrumb
 */
// Jendo Breadcrumb
function JendoBreadcrumb(value) {
    // Основните елементи на Breadcrumb
    this.section = value;
}
// Добавя елементи към Breadcrumb
JendoBreadcrumb.prototype.append = function (data) {
    if (!jendo.isNull(this.section) && !jendo.isNull(data)) {
        var bcItem = this.section.appendLI();
        // Link
        if (!jendo.isNullOrWhiteSpace(data.link)) {
            var bcA = bcItem.appendA(data.link, data.text);
            if (!jendo.isNullOrWhiteSpace(data.target)) {
                bcA.attr('target', data.target);
            }
        }
        // Button
        else if (!jendo.isNullOrEmpty(data.args)) {
            var bcB = bcItem.appendButton(data.text)
                .click(function () {
                    // Dispatch the event.
                    document.dispatchEvent(new CustomEvent('breadcrumb.navigate', {
                        detail: data.args
                    }));
                });
        }
        else {
            bcItem.appendSpan(data.text);
        }
    }
}
// Премахва последния елемент
JendoBreadcrumb.prototype.removeLast = function () {
    if (!jendo.isNull(this.section)) {
        this.section.removeChild(this.section.lastChild());
    }
}
// Зарежда елементите на Breadcrumb
JendoBreadcrumb.prototype.load = function (data) {
    if (!jendo.isNull(this.section) && !jendo.isNull(data)) {
        for (var i = 0; i < data.length; i++) {
            this.append(data[i]);
        }
    }
    return this;
}
// Event: Навигация
JendoBreadcrumb.prototype.navigate = function (func) {
    document.addEventListener('breadcrumb.navigate', func, false);
    return this;
}
// Jendo Breadcrumb
jendo.breadcrumb = function (element) {
    if (jendo.isNull(element)) {
        return new JendoBreadcrumb(null);
    }
    else {
        var jdElement = jendo(element);
        // Ако елемента е открит
        if (jdElement.length > 0) {
            var jdUL = jdElement.find('ul');
            // Създава Breadcrumb, ако не е открит
            if (jdUL.length == 0) {
                jdUL = jdElement.appendUL();
                jdUL.addClass("jd-breadcrumb");
            }
            return new JendoBreadcrumb(jdUL);
        }
        else {
            return new JendoBreadcrumb(null);
        }
    }
}
/* END: Breadcrumb
 */
