﻿/* BEGIN: Timepicker
 */
function JendoTimepicker(element, options) {
    // Jendo елемент
    this.jdElement = element;
    // DOM елемент
    this.dElement = this.jdElement.first();
    // Създава timepicker, ако не е създаден
    if (!this.isInit()) {
        var self = this;
        if (jendo.isNull(this.dElement.options)) {
            this.dElement.options = {
                // формат на часа
                format: 'HH:mm',
                // да показва ли timepicker бутона
                showButton: false,
                // Страна от коята да се отваря
                position: 'left'
            };
            if (!jendo.isNull(options)) {
                if (typeof options.format === 'string') {
                    this.dElement.options.format = options.format;
                }
                if (typeof options.showButton === 'boolean') {
                    this.dElement.options.showButton = options.showButton;
                }
                if (typeof options.position === 'string') {
                    this.dElement.options.position = options.position;
                }
            }
        }
        this.jdTime = {};
        this.jdElement.on('change', function (e) {
            self.validate();
        });
        this.jdElement.on('blur', function (e) {
            if (e.target.value == self.getBlankValue()) {
                e.target.value = '';
            }
        });
        this.jdElement.on('click', function (e) {
            var tVal = e.target.value;
            var caretPos = e.target.selectionStart;
            if (tVal == '') {
                tVal = self.getBlankValue();
                e.target.value = tVal;
            }
            if (caretPos == 0) {
                var dIx = tVal.indexOf(' ', caretPos - 2);
                if (dIx > 0) {
                    e.target.setSelectionRange(0, 1);
                }
                else {
                    e.target.setSelectionRange(0, 2);
                }
            }
            if (caretPos >= tVal.length) {
                e.target.setSelectionRange(tVal.length - 2, tVal.length);
            }
            else {
                var selIx = tVal.indexOf(':', caretPos - 2);
                if (selIx > caretPos) {
                    var dIx = tVal.indexOf(' ', caretPos - 2);
                    if (dIx > 0) {
                        if (dIx < caretPos) {
                            e.target.setSelectionRange(dIx + 1, dIx + 3);
                        }
                        else {
                            e.target.setSelectionRange(0, 1);
                        }
                    }
                    else {
                        e.target.setSelectionRange(0, 2);
                    }
                }
                else {
                    e.target.setSelectionRange(selIx + 1, selIx + 3);
                }
            }
        });
        var tpParent = this.jdElement.parent();
        // Добавя timepicker
        this.dTimepicker = this.createTimepicker(this.jdElement.attrId() + '_timepicker');
        tpParent.insertAfter(this.dTimepicker, this.jdElement);
        // this.dTimepicker.srctxt = this.dElement;
        var jdTimepicker = jendo(this.dTimepicker);

        // Добавя timepicker бутона
        if (this.dElement.options.showButton) {
            /*
            var dBtn = document.createElement('i');
            dBtn.setAttribute('class', 'far fa-clock jd-timepicker-icon');
            dBtn.setAttribute('aria-hidden', 'true');
            tpParent.insertAfter(dBtn, this.jdElement);
            */
            var dBtn = document.createElement('button');
            dBtn.setAttribute('type', 'button');
            dBtn.setAttribute('class', 'jd-timepicker-btn');
            tpParent.insertAfter(dBtn, this.jdElement);
            self.dTimepicker.srcbtn = dBtn;

            var dBtnI = document.createElement('i');
            dBtnI.setAttribute('class', 'far fa-clock');
            dBtnI.setAttribute('aria-hidden', 'true');
            dBtn.appendChild(dBtnI);

            jendo(dBtn).click(function (source) {
                if (jdTimepicker.isVisible()) {
                    jdTimepicker.hide();
                }
                else {
                    if (self.dElement.options.position === 'right') {
                        jdTimepicker.left((self.jdElement.left() + self.jdElement.width()) - 228);
                    }
                    else {
                        jdTimepicker.left(self.jdElement.left());
                    }
                    // валедира часа
                    var time = self.validate();
                    // задава часа
                    self.jdTime.day.innerText = time.day;
                    self.jdTime.hour.innerText = time.hour;
                    self.jdTime.minute.innerText = time.minute;
                    self.jdTime.second.innerText = time.second;
                    // показва панела
                    jdTimepicker.show();
                    // ф-ии за затваряне
                    _JendoFuncCloseElement = self.hideElement;
                    _JendoLastOpenElement = jdTimepicker;
                }
            });
        }
        this.isInit(true);
    }
}

// създаден ли е timepicker
JendoTimepicker.prototype.isInit = function (value) {
    if (jendo.isNull(value)) {
        var isInit = this.dElement.jdp_IsInit;
        return (jendo.isNull(isInit) ? false : isInit);
    }
    else {
        this.dElement.jdp_IsInit = value;
        return this;
    }
};

// Скрива timepicker
JendoTimepicker.prototype.hide = function () {
    jendo(this.dTimepicker).hide();
    _JendoFuncCloseElement = null;
    _JendoLastOpenElement = null;
};

// Скрива timepicker
JendoTimepicker.prototype.hideElement = function (source) {
    var dTimepicker = _JendoLastOpenElement.first();
    if (!_JendoLastOpenElement.isContains(source)
        // && !dTimepicker.srctxt.contains(source)
        && !dTimepicker.srcbtn.contains(source)) {
        _JendoLastOpenElement.hide();
        _JendoFuncCloseElement = null;
        _JendoLastOpenElement = null;
    }
};

// Задава форматиране на датата
JendoTimepicker.prototype.format = function (pattern) {
    if (jendo.isNullOrWhiteSpace(pattern)) {
        return this.dElement.options.format;
    }
    else {
        this.dElement.options.format = pattern;
        this.validate();
        return this;
    }
};

JendoTimepicker.prototype.getBlankValue = function () {
    return this.dElement.options.format
        .replace(/d/g, '_')
        .replace(/H/g, '_')
        .replace(/m/g, '_')
        .replace(/s/g, '_')
        .replace(/h/g, '_');
};

JendoTimepicker.prototype.validate = function () {
    var tVal = this.jdElement.val().trim();
    if (tVal == '') {
        return { day: 0, hour: 0, minute: 0, second: 0 };
    }
    var time = tVal.split(':');
    var tDay = 0;
    var tHour = 0;
    var tHour12 = 0;
    var tMinute = 0;
    var tSecond = 0;
    if (time.length >= 1) {
        var dTime = time[0].split(' ');
        if (dTime.length === 1) {
            tHour = parseInt(time[0]);
        }
        else {
            tDay = parseInt(dTime[0]);
            tHour = parseInt(dTime[1]);
            if (isNaN(tDay)) {
                tDay = 0;
            }
        }
        if (isNaN(tHour)) {
            tHour = 0;
        }
        else if (tHour < 0 || tHour > 24) {
            tHour = 0;
        }
        tHour12 = (tHour > 12 ? tHour - 12 : tHour);
    }
    if (time.length >= 2) {
        tMinute = parseInt(time[1]);
        if (isNaN(tMinute)) {
            tMinute = 0;
        }
        else if (tMinute < 0 || tMinute > 60) {
            tMinute = 0;
        }
    }
    if (time.length >= 3) {
        tSecond = parseInt(time[2]);
        if (isNaN(tSecond)) {
            tSecond = 0;
        }
        else if (tSecond < 0 || tSecond > 60) {
            tSecond = 0;
        }
    }
    this.jdElement.val(this.dElement.options.format
        .replace(/d/g, tDay.toString())
        .replace(/HH/g, tHour.toString().padStart(2, '0'))
        .replace(/H/g, tHour.toString())
        .replace(/mm/g, tMinute.toString().padStart(2, '0'))
        .replace(/m/g, tMinute.toString())
        .replace(/ss/g, tSecond.toString().padStart(2, '0'))
        .replace(/s/g, tSecond.toString())
        .replace(/hh/g, tHour12.toString().padStart(2, '0'))
        .replace(/h/g, tHour12.toString())
    );
    return {
        day: tDay,
        hour: tHour,
        minute: tMinute,
        second: tSecond
    };
};

JendoTimepicker.prototype.setTextTime = function () {
    var tDay = parseInt(this.jdTime.day.innerText);
    var tHour = parseInt(this.jdTime.hour.innerText);
    var tHour12 = (tHour > 12 ? tHour - 12 : tHour);
    var tMinute = parseInt(this.jdTime.minute.innerText);
    var tSecond = parseInt(this.jdTime.second.innerText);

    this.jdElement.val(this.dElement.options.format
        .replace(/d/g, tDay.toString())
        .replace(/HH/g, tHour.toString().padStart(2, '0'))
        .replace(/H/g, tHour.toString())
        .replace(/mm/g, tMinute.toString().padStart(2, '0'))
        .replace(/m/g, tMinute.toString())
        .replace(/ss/g, tSecond.toString().padStart(2, '0'))
        .replace(/s/g, tSecond.toString())
        .replace(/hh/g, tHour12.toString().padStart(2, '0'))
        .replace(/h/g, tHour12.toString())
    );
}

// Да се отваря в лявата страна
JendoTimepicker.prototype.positionLeft = function () {
    this.dElement.options.position = 'left';
    return this;
};

// Да се отваря в дясната страна
JendoTimepicker.prototype.positionRight = function () {
    this.dElement.options.position = 'right';
    return this;
};

// създава Timepicker
JendoTimepicker.prototype.createTimepicker = function (timepickerId) {
    var tp = document.createElement('div');
    tp.setAttribute('class', 'jd-timepicker');
    tp.id = timepickerId;

    var tpContent = document.createElement('div');
    tpContent.setAttribute('class', 'jd-tp-content');
    tp.appendChild(tpContent);

    this.jdTime.day = this.appendTimeItem({
        parent: tpContent,
        separator: ' ',
        value: '0',
        max: 999
    });
    this.jdTime.hour = this.appendTimeItem({
        parent: tpContent,
        separator: ' ',
        value: '0',
        max: 24
    });
    this.jdTime.minute = this.appendTimeItem({
        parent: tpContent,
        separator: ':',
        value: '0',
        max: 60
    });
    this.jdTime.second = this.appendTimeItem({
        parent: tpContent,
        separator: ':',
        value: '0',
        max: 60
    });
    return tp;
};

JendoTimepicker.prototype.appendTimeItem = function (args) {
    var self = this;
    var tpItem = document.createElement('div');
    tpItem.setAttribute('class', 'jd-tp-col');
    args.parent.appendChild(tpItem);

    var tpISep = document.createElement('div');
    tpISep.setAttribute('class', 'jd-tp-sep');
    tpISep.innerText = args.separator;
    tpItem.appendChild(tpISep);
    var tpIVal = document.createElement('div');
    tpIVal.setAttribute('class', 'jd-tp-col');
    args.parent.appendChild(tpIVal);

    var tpValUp = document.createElement('button');
    tpValUp.setAttribute('type', 'button');
    tpValUp.setAttribute('class', 'jd-tp-btn');
    tpValUp.innerHTML = '&#11165;';
    tpIVal.appendChild(tpValUp);
    var tpVal = document.createElement('div');
    tpVal.setAttribute('class', 'jd-tp-val');
    tpVal.innerText = args.value;
    tpIVal.appendChild(tpVal);
    var tpValDown = document.createElement('button');
    tpValDown.setAttribute('type', 'button');
    tpValDown.setAttribute('class', 'jd-tp-btn');
    tpValDown.innerHTML = '&#11167;';
    tpIVal.appendChild(tpValDown);

    jendo(tpValUp).click(function (e) {
        var jdVal = e.data.jdVal;
        var iVal = parseInt(jdVal.text()) + 1;
        jdVal.text(iVal >= e.data.maxVal ? iVal - e.data.maxVal : iVal);
        self.setTextTime();
    }, {
        jdVal: jendo(tpVal),
        maxVal: args.max
    });
    jendo(tpValDown).click(function (e) {
        var jdVal = e.data.jdVal;
        var iVal = parseInt(jdVal.text()) - 1;
        jdVal.text(iVal < 0 ? e.data.maxVal + iVal : iVal);
        self.setTextTime();
    }, {
        jdVal: jendo(tpVal),
        maxVal: args.max
    });
    return tpVal;
}

jendo.timepicker = function (element, args) {
    if (!jendo.isNull(element)) {
        var targetElement = jendo(element);
        if (targetElement.length > 0) {
            return new JendoTimepicker(targetElement, args);
        }
    }
};

JendoSelector.prototype.timepicker = function (args) {
    if (jendo.isNull(args)) {
        args = {};
    }
    return new JendoTimepicker(this, args);
};
/* END: Timepicker
 */