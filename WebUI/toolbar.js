﻿/* BEGIN: Toolbar
 */
function JendoToolbar(element, options) {
    // Jendo елемент
    this.jdElement = element;
    if (jendo.isNull(options)) {
        return this;
    }
    else if (!jendo.isNull(options.menu)) {
        var jdTBMenu = this.jdElement.appendDiv()
            .class('jd-toolbar jd-toolbar-default');
        if ('style' in options) {
            jdTBMenu.addClass(options.style);
        }
        this.loadMenu(jdTBMenu, options);
    }
    else if (!jendo.isNull(options.tabs)) {
        var tBar = this.jdElement.appendDiv()
            .class('jd-toolbar jd-toolbar-default');
        var tabMenu = tBar.appendUL().class('jd-tb-tabs');
        // Селецтиран таб
        var selTab = null;
        // Зарежда табовете и менютата
        for (var t = 0; t < options.tabs.length; t++) {
            var menuItem = options.tabs[t];
            // меню на таба
            var tabItemMenu = this.loadMenu(tBar, menuItem);
            tabItemMenu.hide();
            // Tab
            var menuBtn = tabMenu.appendLI()
                .appendButton()
                .class('jd-tb-button')
                .text(menuItem.title)
                .click(function (e) {
                    var tiBtn = jendo(e.target);
                    // Премахва active от всички табове
                    tiBtn.parent().parent().find('button').removeClass('jd-tb-active');
                    // Добавя active
                    tiBtn.addClass("jd-tb-active");

                    var tiMenu = e.data.tabMenu;
                    // Скрива всички менюта на табовете
                    tiMenu.parent().find('.jd-tb-menu').hide();
                    // Показва менюто на избрания таб
                    tiMenu.show();
                }, {
                    tabMenu: tabItemMenu
                });
            menuBtn.prop('menu', tabItemMenu);
            // Селектиран ли е таба
            if (menuItem.selected || selTab === null) {
                selTab = menuBtn;
            }
        }
        // Селектира таба
        if (selTab !== null) {
            selTab.addClass("jd-tb-active");
            selTab.prop('menu').show();
        }
        return this;
    }
}

/* Зарежда toolbar меню
 */
JendoToolbar.prototype.loadMenu = function (jdTab, tbMenu) {
    var jdMenu = jdTab.appendUL().class('jd-tb-menu');
    for (var i = 0; i < tbMenu.menu.length; i++) {
        var item = tbMenu.menu[i];
        var jdMBtn = jdMenu.appendLI()
            .appendButton().class('jd-tb-button');
        if ('tooltip' in item) {
            jdMBtn.attr('title', item.tooltip);
        }
        if ('glyphs' in item) {
            jdMBtn.appendI()
                .class(item.glyphs)
                .attr('aria-hidden', true);
        }
        if ('title' in item) {
            jdMBtn.appendSamp().text(item.title);
        }
        if ('click' in item) {
            jdMBtn.click(item.click);
        }
    }
    return jdMenu;
};

// Toolbar
jendo.toolbar = function (element, options) {
    return new JendoToolbar(jendo(element), options);
};
/* END: Toolbar
 */