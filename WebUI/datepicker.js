/* BEGIN: Datepicker
 */
function JendoDatepicker(element, options) {
    // Jendo елемент
    this.jdElement = element;
    // DOM елемент
    this.dElement = this.jdElement.first();
    this.dpOptions = this.dElement.options;
    if (jendo.isNull(this.dpOptions)) {
        this.dpOptions = {
            // Отместване за дните от седмицата
            weekdayStart: 1,
            // формат на датата
            format: 'yyyy-MM-dd',
            // изглед при отваряне / days, months, years/
            viewMode: 'days',
            // да показва ли datepicker бутона
            showButton: false,
            // Страна от коята да се отваря
            position: 'left'
        };
        this.dElement.options = this.dpOptions;
    }
    // Създава datepicker, ако не е създаден
    if (!this.isInit()) {
        var dpParent = this.jdElement.parent();
        // Добавя datepicker бутона
        if (this.dpOptions.showButton) {
            var dBtn = document.createElement('i');
            dBtn.setAttribute('class', 'fas fa-calendar');
            dBtn.setAttribute('aria-hidden', 'true');
            dpParent.insertAfter(dBtn, this.jdElement);
        }
        // Добавя календара
        this.dDatepicker = this.createDatepicker(this.jdElement.attrId() + '_datepicker');
        dpParent.insertAfter(this.dDatepicker, this.jdElement);
        var self = this;
        this.jdDatepicker = jendo(this.dDatepicker);

        this.jdElement.click(function (source) {
            if (self.jdDatepicker.isVisible()) {
                self.hide();
            }
            else {
                self.show();
            }
        });
        this.jdElement.keyDown((e) => {
            // 9: Tab
            if (e.keyCode === 9) {
                self.hide();
            }
        });
        this.jdElement.keyUp((e) => {
            // 27: Escape
            if (e.keyCode === 27) {
                self.hide();
            }
            else if (!self.jdDatepicker.isVisible()) {
                self.show();
            }
        });
        this.isInit(true);
    }
}

// създаден ли е datepicker
JendoDatepicker.prototype.isInit = function (value) {
    if (jendo.isNull(value)) {
        var isInit = this.dElement.jdp_IsInit;
        return (jendo.isNull(isInit) ? false : isInit);
    }
    else {
        this.dElement.jdp_IsInit = value;
        return this;
    }
};

// Скрива календара
JendoDatepicker.prototype.hide = function () {
    this.jdDatepicker.hide();
    _JendoFuncCloseElement = null;
    _JendoLastOpenElement = null;
};

// Скрива календара
JendoDatepicker.prototype.hideElement = function (source) {
    if (!_JendoLastOpenElement.isContains(source)) {
        _JendoLastOpenElement.hide();
        _JendoFuncCloseElement = null;
        _JendoLastOpenElement = null;
    }
};

// Задава форматиране на датата
JendoDatepicker.prototype.format = function (pattern) {
    if (jendo.isNullOrWhiteSpace(pattern)) {
        return this.dpOptions.format;
    }
    else {
        this.dpOptions.format = pattern;
        // ако има ст-ст
        if (!jendo.isNull(this.dElement.dateValue)) {
            this.jdElement.val(jendo.dateFormatUI(this.dElement.dateValue, pattern));
        }
        return this;
    }
};

// Да се отваря в лявата страна
JendoDatepicker.prototype.positionLeft = function () {
    this.dpOptions.position = 'left';
    return this;
};

// Да се отваря в дясната страна
JendoDatepicker.prototype.positionRight = function () {
    this.dpOptions.position = 'right';
    return this;
};

// Създава календар
JendoDatepicker.prototype.createDatepicker = function (datepickerId) {
    var dp = document.createElement('div');
    dp.setAttribute('class', 'datepicker');
    dp.id = datepickerId;

    var dpDate = jendo.now();
    this.loadDPDays(dp, dpDate.getFullYear(), dpDate.getMonth(), dpDate.getDate());

    return dp;
};

// Заглавната част на календара
JendoDatepicker.prototype.addDPHead = function (content, titleText, titleClick, leftClick, rightClick) {
    var dpHead = document.createElement('div');
    dpHead.setAttribute('class', 'dp-head');
    content.appendChild(dpHead);
    // назад
    var dphLeft = document.createElement('div');
    dphLeft.setAttribute('class', 'dph-left');
    dpHead.appendChild(dphLeft);
    var dphlButton = document.createElement('button');
    dphlButton.setAttribute('type', 'button');
    dphlButton.innerText = '<';
    dphlButton.onclick = leftClick;
    dphLeft.appendChild(dphlButton);
    // заглавие
    var dphTitle = document.createElement('div');
    dphTitle.setAttribute('class', 'dph-title');
    dpHead.appendChild(dphTitle);
    var dphtButton = document.createElement('button');
    dphtButton.setAttribute('type', 'button');
    dphtButton.innerText = titleText;
    dphtButton.onclick = titleClick;
    dphTitle.appendChild(dphtButton);

    // напред
    var dphRight = document.createElement('div');
    dphRight.setAttribute('class', 'dph-right');
    dpHead.appendChild(dphRight);
    var dphrButton = document.createElement('button');
    dphrButton.setAttribute('type', 'button');
    dphrButton.innerText = '>';
    dphrButton.onclick = rightClick;
    dphRight.appendChild(dphrButton);
};

// Дни от месеца
JendoDatepicker.prototype.loadDPDays = function (content, dYear, dMonth, dDate) {
    content.innerText = '';
    var jdp = this;
    // Заглавната част
    this.addDPHead(content, jendo.lang.monthNames[dMonth] + ' ' + dYear,
        function (source) {
            jdp.loadDPMonths(content, dYear, dMonth, dDate);
        },
        function (source) {
            var hDate = new Date(dYear, dMonth - 1, dDate);
            jdp.loadDPDays(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
        },
        function (source) {
            var hDate = new Date(dYear, dMonth + 1, dDate);
            jdp.loadDPDays(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
        });

    // Начало: Основната част 
    var dpBody = document.createElement('div');
    dpBody.setAttribute('class', 'dp-month');
    content.appendChild(dpBody);

    // Начало: Дни от седмицата
    var pWeekday = document.createElement('div');
    pWeekday.setAttribute('class', 'dpm-weekdays');
    dpBody.appendChild(pWeekday);
    // дните след стартовия ден
    for (var iwd = this.dpOptions.weekdayStart; iwd < jendo.lang.weekdayNames.length; iwd++) {
        var wd = document.createElement('div');
        wd.setAttribute('class', 'dpm-weekday');
        wd.innerText = jendo.lang.weekdayNarrowNames[iwd];
        pWeekday.appendChild(wd);
    }
    // дните преди стартовия ден
    for (var iwd = 0; iwd < this.dpOptions.weekdayStart; iwd++) {
        var wd = document.createElement('div');
        wd.setAttribute('class', 'dpm-weekday');
        wd.innerText = jendo.lang.weekdayNarrowNames[iwd];
        pWeekday.appendChild(wd);
    }
    // Край: Дни от седмицата
    // Начало: Дните от месеца
    var mDays = document.createElement('div');
    mDays.setAttribute('class', 'dpm-days');
    dpBody.appendChild(mDays);
    // Отместване в началото
    var shiftStart = (new Date(Date.UTC(dYear, dMonth, 1)).getDay()) - this.dpOptions.weekdayStart;
    if (shiftStart < 0) {
        shiftStart += 7;
    }
    for (var iss = 0; iss < shiftStart; iss++) {
        var elmDay = document.createElement('button');
        elmDay.setAttribute('type', 'button');
        elmDay.setAttribute('class', 'dpm-day');
        elmDay.innerText = '';
        mDays.appendChild(elmDay);
    }
    // Дни от месеца
    var days = jendo.daysInMonth(dMonth + 1, dYear);
    for (var d = 1; d <= days; d++) {
        var elmDay = document.createElement('button');
        elmDay.setAttribute('type', 'button');
        elmDay.setAttribute('class', 'dpm-day');
        elmDay.setAttribute('data-day', d);
        elmDay.innerText = d;
        elmDay.onclick = function (source) {
            var dayIndex = source.target.getAttribute('data-day');
            var selDate = new Date(Date.UTC(dYear, dMonth, dayIndex));
            // стойноста (дата) на компонента
            jdp.dElement.dateValue = selDate;
            jdp.jdElement.val(jendo.dateFormatUI(selDate, jdp.format()));
            jdp.hide();
        };
        mDays.appendChild(elmDay);
    }
    // Допълване на месеца
    var shiftEnd = ((shiftStart + days) % 7);
    if (shiftEnd > 0) {
        shiftEnd = 7 - shiftEnd;
    }
    for (var ise = 0; ise < shiftEnd; ise++) {
        var elmDay = document.createElement('button');
        elmDay.setAttribute('type', 'button');
        elmDay.setAttribute('class', 'dpm-day');
        elmDay.innerText = '';
        mDays.appendChild(elmDay);
    }
    // Край: Дните от месеца
    // Край: Основната част 
};

// Месеци
JendoDatepicker.prototype.loadDPMonths = function (content, dYear, dMonth, dDate) {
    content.innerText = '';
    var jdp = this;
    // Заглавната част
    this.addDPHead(content, dYear,
        function (source) {
            jdp.loadDPYears(content, dYear, dMonth, dDate);
        },
        function (source) {
            var hDate = new Date(dYear - 1, dMonth, dDate);
            jdp.loadDPMonths(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
        },
        function (source) {
            var hDate = new Date(dYear + 1, dMonth, dDate);
            jdp.loadDPMonths(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
        });

    // Начало: Основната част 
    var dpBody = document.createElement('div');
    dpBody.setAttribute('class', 'dp-months');
    content.appendChild(dpBody);
    // Начало: Месеци от годината
    for (var i = 0; i < jendo.lang.monthShortNames.length; i++) {
        var ym = document.createElement('button');
        ym.setAttribute('type', 'button');
        ym.setAttribute('class', 'dpm-month');
        ym.setAttribute('data-month', i);
        ym.innerText = jendo.lang.monthShortNames[i];
        ym.onclick = function (source) {
            var monthIndex = source.target.getAttribute('data-month');
            var mDate = new Date(dYear, monthIndex, dDate);
            jdp.loadDPDays(content, mDate.getFullYear(), mDate.getMonth(), mDate.getDate());
        };
        dpBody.appendChild(ym);
    }
    // Край: Месеци от годината
    // Край: Основната част 
};

// Години
JendoDatepicker.prototype.loadDPYears = function (content, dYear, dMonth, dDate) {
    content.innerText = '';
    var jdp = this;
    var strYear = dYear.toString();
    var dYearFrom = (dYear - parseInt(strYear[strYear.length - 1]));
    var dYearTo = (dYearFrom + 9);
    // Заглавната част
    this.addDPHead(content, dYearFrom + ' - ' + dYearTo,
        function (source) {
        },
        function (source) {
            var hDate = new Date(dYear - 12, dMonth, dDate);
            jdp.loadDPYears(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
        },
        function (source) {
            var hDate = new Date(dYear + 12, dMonth, dDate);
            jdp.loadDPYears(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
        });

    // Начало: Основната част 
    var dpBody = document.createElement('div');
    dpBody.setAttribute('class', 'dp-months');
    content.appendChild(dpBody);
    // Начало: Години
    for (var i = -1; i < 11; i++) {
        var iYear = dYearFrom + i;
        var ym = document.createElement('button');
        ym.setAttribute('type', 'button');
        if ((i == -1) || (i == 10)) {
            ym.setAttribute('class', 'dpm-month dpm-outside');
        }
        else {
            ym.setAttribute('class', 'dpm-month');
        }
        ym.setAttribute('data-year', iYear);
        ym.innerText = iYear;
        ym.onclick = function (source) {
            var yearIndex = source.target.getAttribute('data-year');
            var mDate = new Date(yearIndex, dMonth, dDate);
            jdp.loadDPMonths(content, mDate.getFullYear(), mDate.getMonth(), mDate.getDate());
        };
        dpBody.appendChild(ym);
    }
    // Край: Години
    // Край: Основната част 
};

// отваря календава
JendoDatepicker.prototype.show = function () {
    if (this.dpOptions.position === 'right') {
        this.jdDatepicker.left((this.jdElement.left() + this.jdElement.width()) - 228);
        // this.jdDatepicker.right(jdp.jdElement.right());
    }
    else {
        this.jdDatepicker.left(this.jdElement.left());
    }
    this.jdDatepicker.show();
    // ф-ии за затваряне
    _JendoFuncCloseElement = this.hideElement;
    _JendoLastOpenElement = this.jdDatepicker;
}

// Календар
jendo.datepicker = function (element, args) {
    if (!jendo.isNull(element)) {
        var targetElement = jendo(element);
        if (targetElement.length > 0) {
            return new JendoDatepicker(targetElement, args);
        }
    }
};

JendoSelector.prototype.datepicker = function (args) {
    if (jendo.isNull(args)) {
        args = {};
    }
    return new JendoDatepicker(this, args);
};
/* END: Datepicker
 */