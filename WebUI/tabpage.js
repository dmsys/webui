﻿/* BEGIN: TabPage
 */
function JendoTabPage(element, args) {
    this.jdTP = jendo(element);
    this.jdTabs = jendo(this.jdTP.find('.jd-tabs').first());
    if (this.jdTabs.length === 0) {
        this.jdTabs = this.jdTP.appendDiv();
        this.jdTabs.addClass('jd-tabs');
    }
    var isinit = this.jdTP.data('isinit');
    if (isinit !== '1') {
        var self = this;
        this.jdTP.data('isinit', 1);
        this.jdTP.data('tcount', this.jdTabs.children().length);
        // scroll
        var jdScroll = this.jdTP.appendDiv()
            .addClass('jd-tab-scroll');
        var scrollLeft = null;
        var scrollRight = null;

        jdScroll.appendButton()
            .attr('data-tabscroll', 'left')
            .html('&#10094;')
            .click(function (e) {
                self.jdTabs.first().scrollLeft -= 40;
            })
            .mouseDown(function (e) {
                var jdSTabs = self.jdTabs.first();
                scrollLeft = setInterval(function () {
                    jdSTabs.scrollLeft -= 40;
                }, 200);
            })
            .mouseUp(function (e) {
                if (scrollLeft !== null) {
                    clearInterval(scrollLeft);
                    scrollLeft = null;
                }
            })
            .mouseLeave(function (e) {
                if (scrollLeft !== null) {
                    clearInterval(scrollLeft);
                    scrollLeft = null;
                }
            });
        jdScroll.appendButton()
            .attr('data-tabscroll', 'right')
            .html('&#10095;')
            .click(function (e) {
                self.jdTabs.first().scrollLeft += 40;
            })
            .mouseDown(function (e) {
                var jdSTabs = self.jdTabs.first();
                scrollRight = setInterval(function () {
                    jdSTabs.scrollLeft += 40;
                }, 200);
            })
            .mouseUp(function (e) {
                if (scrollRight !== null) {
                    clearInterval(scrollRight);
                    scrollRight = null;
                }
            })
            .mouseLeave(function (e) {
                if (scrollRight !== null) {
                    clearInterval(scrollRight);
                    scrollRight = null;
                }
            });
        // this.jdTabs.on('scroll', function (e) {
        //      console.log(e, e.target.clientWidth, e.target.offsetWidth, e.target.scrollWidth);
        // });
        window.addEventListener("resize", function (e) {
            self.showScrollBtn();
        });

        this.jdTabs.find('[data-tab]').click(function (e) {
            var jdTab = jendo(e.target);
            self.selectTab(self, jdTab);
        });

        if ('selected' in args) {
            var jdTabS = this.jdTabs.find('[data-tab="' + args.selected + '"]');
            this.selectTab(this, jdTabS);
        }
        else {
            var jdTabF = jendo(this.jdTabs.find('[data-tab]').first());
            this.selectTab(this, jdTabF);
        }
    }
    return this;
}

JendoTabPage.prototype.selectTab = function (self, jdTab) {
    var tab = jdTab.attr('data-tab');
    if (tab) {
        self.jdTabs.find('[data-tab]').removeClass('jd-active');
        jdTab.addClass('jd-active');

        self.jdTP.find(':scope > [data-tabpage]').hide();
        self.jdTP.find('[data-tabpage="' + tab + '"]').show();
    }
};

JendoTabPage.prototype.select = function (tab) {
    this.selectTab(this, this.jdTP.find('[data-tab="' + tab + '"]'));
}

JendoTabPage.prototype.exist = function (tab) {
    return (this.jdTP.find('[data-tab="' + tab + '"]').length > 0);
}

JendoTabPage.prototype.append = function (args) {
    var tCount = parseInt(this.jdTP.data('tcount'));
    if (!('tab' in args)) {
        args.tab = 'tab' + tCount;
    }
    var jdTBtn = this.jdTabs.find('[data-tab="' + args.tab + '"]');
    if (jdTBtn.length === 0) {
        var closeTab = false;
        if ('closeTab' in args) {
            closeTab = args.closeTab;
        }
        this.jdTP.data('tcount', (tCount + 1));

        var jdTab = this.jdTabs.appendDiv()
            .addClass('jd-tab');
        jdTBtn = jdTab.appendButton(args.title)
            .addClass('jd-tab-btn')
            .attr('data-tab', args.tab);
        var jdTPage = this.jdTP.appendDiv()
            .addClass('jd-page')
            .attr('data-tabpage', args.tab);
        if ('page' in args) {
            jdTPage.html(args.page);
        }
        else if ('pageUrl' in args) {
            jdTPage.load(args.pageUrl);
        }
        var self = this;
        if (closeTab) {
            jdTBtn.addClass('jd-tab-rpadd');
            var jdTClose = jdTab.appendButton()
                .html('&#215;') // .html('&times;')
                .addClass('jd-tab-close');
            jdTClose.click(function (e) {
                var jdCTab = jdTBtn.parent();
                if (jdTBtn.hasClass('jd-active')) {
                    var jdTList = self.jdTabs.children();
                    var cTab = jdCTab.first();
                    var jdNTab = null;
                    for (var i = 0; i < jdTList.items.length; i++) {
                        if (cTab === jdTList.items[i]) {
                            if (i + 1 < jdTList.items.length) {
                                jdNTab = jendo(jdTList.items[i + 1]);
                            }
                            else if (i - 1 >= 0) {
                                jdNTab = jendo(jdTList.items[i - 1]);
                            }
                        }
                    }
                    if (jdNTab !== null) {
                        self.selectTab(self, jdNTab.find('[data-tab]'));
                    }
                }
                jdCTab.remove();
                jdTPage.remove();
                self.showScrollBtn();
            });
        }
        jdTBtn.click(function (e) {
            self.selectTab(self, jdTBtn);
        });
        this.showScrollBtn();
    }
    if (args.select) {
        this.selectTab(this, jdTBtn);
    }
    return jdTBtn;
};

JendoTabPage.prototype.showScrollBtn = function () {
    var tabs = this.jdTabs.first()
    if (tabs.scrollWidth > tabs.clientWidth) {
        this.jdTP.find('[data-tabscroll]').css('display', 'inline-block');
    }
    else {
        this.jdTP.find('[data-tabscroll]').hide();
    }
}

// TabPage
jendo.tabpage = function (element, args) {
    return new JendoTabPage(element, args ? args : {});
};
/* END: TabPage
 */