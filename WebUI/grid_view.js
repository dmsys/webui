﻿/* BEGIN: Jendo GridView
 */
function JendoGridView(element) {
    // Jendo елемент
    if (jendo.isNull(element)) {
        return;
    }
    if (element.length === 0) {
        return;
    }
    this._Element = element;
    // GridView
    this._GridView = this._Element.find('[role="grid"]');

    if (this._GridView.length === 0) {
        this._GridView = this._Element.appendTable().role('grid');
    }
    // GridView: Данни за грида
    this._GVData = this._GridView.prop('gvdata');
    // Ако не са инициализирани
    if (jendo.isNull(this._GVData)) {
        this._GVData = {
            // филтриране (за целия грид)
            filterable: {
                enabled: false,     /* може ли за се филтрира */
                mode: 'row'         /* стил на филтъра 'row', 'menu' */
            },
            columns: [],
            records: [],
            sortData: [],
            sumData: [],            /* суми по колонки */
            filterData: [],
            allowPaging: true,      /* разрешено ли е paging-а */
            pageSize: 10,           /* редове на страница */
            pageNo: 1,              /* номер на текущата страница */
            pager: {
                numeric: true,      /* бутони с номера на страници */
                numericCount: 5,    /* брой на видимите бутони с номера на страници */
                prevNext: true,     /* бутони: предишена, следваща страница */
                firstLast: true,    /* бутони: първа, последна страница */
                slider: true,       /* слаидер за навигация */
                goToPage: true,     /* директно позициониране на страница */
                pageSize: true,     /* вътрешно задаване на брой редове на страница */
                pageSizeList: [],   /* списък от брой редове на страница, ако е празен се въвежда на ръка */
                info: true          /* информация за позицията */
            },
            // Визуализация
            show: {
                rowNo: false         /* номер на ред */
            },
            detail: null            /* грид детайлна информация за всеки ред */
        };
        this._GridView.prop('gvdata', this._GVData);
    }
}
// зарежда грида с данни
JendoGridView.prototype.data = function (value) {
    if (jendo.isNull(value)) {
        return this;
    }
    // Филтриране
    if ('filterable' in value) {
        if ('enabled' in value.filterable) {
            this._GVData.filterable.enabled = value.filterable.enabled;
        }
        if ('mode' in value.filterable) {
            this._GVData.filterable.mode = value.filterable.mode;
        }
    }
    // Визуализация
    if ('show' in value) {
        if ('rowNo' in value.show) {
            this._GVData.show.rowNo = value.show.rowNo;
        }
    }
    // Paging
    if ('allowPaging' in value) {
        this._GVData.allowPaging = value.allowPaging;
    }
    if ('pageSize' in value) {
        this._GVData.pageSize = value.pageSize;
    }
    if ('pager' in value) {
        if ('numeric' in value.pager) {
            this._GVData.pager.numeric = value.pager.goToPage;
        }
        if ('numeric' in value.pager) {
            this._GVData.pager.numeric = value.pager.numeric;
        }
        if ('numericCount' in value.pager) {
            this._GVData.pager.numericCount = value.pager.numericCount;
        }
        if ('prevNext' in value.pager) {
            this._GVData.pager.prevNext = value.pager.prevNext;
        }
        if ('firstLast' in value.pager) {
            this._GVData.pager.firstLast = value.pager.firstLast;
        }
        if ('slider' in value.pager) {
            this._GVData.pager.slider = value.pager.slider;
        }
        if ('goToPage' in value.pager) {
            this._GVData.pager.goToPage = value.pager.goToPage;
        }
        if ('pageSize' in value.pager) {
            this._GVData.pager.pageSize = value.pager.pageSize;
        }
        if ('pageSizeList' in value.pager) {
            this._GVData.pager.pageSizeList = value.pager.pageSizeList;
        }
        if ('info' in value.pager) {
            this._GVData.pager.info = value.pager.info;
        }
    }
    // грид детайлна информация за всеки ред
    if ('detail' in value) { this._GVData.detail = value.detail; }
    
    // events
    if ('rowBinding' in value) { this._GVData.rowBindingEvent = value.rowBinding; }
    if ('rowBound' in value) { this._GVData.rowBoundEvent = value.rowBound; }
    // създаден е грид за детайлна информация - извиква се еднократно при създаване
    if ('detailCreate' in value) { this._GVData.detailCreateEvent = value.detailCreate; } 
    // зареден е грид за детайлна информация - извиква се всеки път при показване
    if ('detailLoad' in value) { this._GVData.detailLoadEvent = value.detailLoad; } 
  
    this._GridView.html('');
    this._GVData.columns = [];
    this.filterable(value.filterable);
    this.columns(value.columns);
    this.records(value.records);

    if ('sortData' in value) {
        this.sortData(value.sortData);
    }
    return this;
};
// Валидира филтрирането
JendoGridView.prototype.filterable = function (value) {
    if (jendo.isNull(value)) {
        return;
    }
    var gFilterable = this._GVData.filterable;
    gFilterable.enabled = (jendo.isNull(value.enabled) ? false : value.enabled);
    gFilterable.mode = (jendo.isNull(value.mode) ? 'row' : value.mode);
};

JendoGridView.prototype.columns = function (gCols) {
    if (!jendo.isNull(gCols)) {
        var self = this;
        // Колонки на грида
        var gColumns = this._GVData.columns;
        // Филтриране по колонки
        var gFilterable = this._GVData.filterable;
        var sortData = this._GVData.sortData;
        var filterData = this._GVData.filterData;
        var tHead = this._GridView.appendTHead();
        var tRow = tHead.appendTRow();
        this._GVData.sumData = [];
        var sumData = this._GVData.sumData
        // Ред за филтриране
        var tRowFilter = null;
        if (gFilterable.enabled && (gFilterable.mode === 'row')) {
            tRowFilter = tHead.appendTRow();
        }
        // отваря детайлна информация
        if (!jendo.isNull(this._GVData.detail)) {
            tRow.appendTH().text('');
            // детайлна информация в реда за филтриране
            if (tRowFilter !== null) {
                tRowFilter.appendTH();
            }
        }
        // Номер на ред в заглавния ред
        if (this._GVData.show.rowNo) {
            tRow.appendTH().text('#');
            // Номер на ред в реда за филтриране
            if ( tRowFilter !== null ) {
                tRowFilter.appendTH();
            }
        }

        // Добавя останалите клетки от реда
        for (var i = 0; i < gCols.length; i++) {
            var gCol = gCols[i];
            // видима ли е колоната
            if ('visible' in gCol && gCol.visible === false) { continue; }
            // сума по колона
            if (gCol.footerSum) {
                sumData.push({
                    field: gCol.field,
                    ix: gColumns.length
                });
                gCol.sum = 0;
            }
            else if ('sum' in gCol) {
                delete gCol.sum;
            }
            // Валидира данните на колонката
            if (jendo.isNullOrEmpty(gCol.caption)) {
                gCol.caption = '';
            }
            // Заглавната част на колонката
            var th = tRow.appendTH();
            if ('width' in gCol) {
                th.width(gCol.width);
            }
            th.html(gCol.caption);
            // стил ако има
            if (!jendo.isNullOrWhiteSpace(gCol.style)) {
                th.addClass(gCol.style);
            }
            // колонка с данни
            if (!jendo.isNull(gCol.field)) {
                // Сортиране
                var colSortable = 'sortable' in gCol ? gCol.sortable : true;
                if (colSortable) {
                    th.append('<span class="jd-sort-asc">&#9206;</span>');
                    th.append('<span class="jd-sort-desc">&#9207;</span>');
                }
                th.data('field', gCol.field);
                if (colSortable) {
                    th.click(function (e) {
                        var dataField = jendo(e.target).data('field');
                        // търси полето за сортиране
                        var sortField = sortData.find(function (item) {
                            return item.field === dataField;
                        });
                        // добавя поле към сортирането
                        if (e.ctrlKey) {
                            // Ако не е намерен
                            if (jendo.isNull(sortField)) {
                                sortData.push({ field: dataField, sort: 1 });
                            }
                            // Ако е намерен
                            else {
                                if (sortField.sort === 1) {
                                    sortField.sort = -1;
                                }
                                else if (sortField.sort === -1) {
                                    var sortFieldIndex = sortData.findIndex(function (item) {
                                        return item.field === dataField;
                                    });
                                    sortData.splice(sortFieldIndex, 1);
                                }
                                else {
                                    sortField.sort = 1;
                                }
                            }
                        }
                        // задава поле за сортиране
                        else {
                            // Ако не е намерен
                            if (_.isNull(sortField)) {
                                sortData.splice(0, sortData.length);
                                sortData.push({ field: dataField, sort: 1 });
                            }
                            // Ако е намерен
                            else {
                                sortField.sort = ((sortField.sort === 1) ? -1 : 1);
                            }
                        }
                        self.sortData();
                    });
                }
                gColumns.push(gCol);
                // Ред за филтриране
                if (tRowFilter !== null) {
                    var thrf = tRowFilter.appendTH();
                    thrf.appendText('input')
                        .data('field', gCol.field)
                        .keyUp(function (e) {
                            var dField = _(e.target);
                            var dFieldName = dField.data('field');
                            var dFieldValue = dField.val();
                            // Променя масива за филтриране
                            if (!filterData.find(function (value, index) {
                                if (value.field === dFieldName) {
                                    value.filter = dFieldValue;
                                    return true;
                                }
                                else {
                                    return false;
                                }
                            })) {
                                filterData.push({ field: dFieldName, filter: dFieldValue });
                            }
                            // за да се виждат филтрираните редове се премесва на първа стр.
                            self.pageNo(1);
                            // Презарежда редовете
                            self.refreshRecords();
                        });
                }
            }
            // конлонка: команди, меню
            else if (!jendo.isNull(gCol.commands) || !jendo.isNull(gCol.menu)) {
                gColumns.push(gCol);
                // Ред за филтриране
                if (tRowFilter !== null) {
                    // var thrc =
                    tRowFilter.appendTH();
                }
            }
            // колонка с данни от функция
            else if (!jendo.isNull(gCol.bind)) {
                gColumns.push(gCol);
                // Ред за филтриране
                if (tRowFilter !== null) {
                    var thrb = tRowFilter.appendTH();
                }
            }
        }
    }
    else {
        return this._GVData.columns;
    }
};

// намира колонка по име
JendoGridView.prototype.column = function (name) {
    for (var i = 0; i < this._GVData.columns.length; i++) {
        var col = this._GVData.columns[i];
        if (col.field === name) {
            return col;
        }
    }
    return null;
};

// Зарежда редовете на грида
JendoGridView.prototype.records = function (value) {
    if (!jendo.isNull(value)) {
        // Колонки на грида
        var gColumns = this._GVData.columns;
        // Данни на грида
        this._GVData.records = value;
        // Грид
        var tBody = this._GridView.find('[role="rowgroup"]');
        if (tBody.length === 0) {
            tBody = this._GridView.appendTBody().role("rowgroup");
        }
        else {
            tBody.html('');
        }
        if (!('sortData' in this._GVData)) { // при сортиране се презарежда
            // изчислява суми по колони
            this.calculateColumnSums(gColumns, value);
            // Rows
            this.appendGRows(tBody, gColumns, value, this._GVData);
            // Paging
            this.appendGPaging(tBody, gColumns, value, this._GVData);
        }
        return this;
    }
    else {
        return this._GVData.records;
    }
};

// изчислява суми по колони
JendoGridView.prototype.calculateColumnSums = function (gColumns, gRecords) {
    var sumDataLen = this._GVData.sumData.length;
    if (sumDataLen > 0) {
        var recLen = gRecords.length;
        var sumData = this._GVData.sumData;
        for (var c = 0; c < sumDataLen; c++) {
            gColumns[sumData[c].ix].sum = 0;
        }
        for (var r = 0; r < recLen; r++) {
            var rec = gRecords[r];
            for (var c = 0; c < sumDataLen; c++) {
                var val = rec[sumData[c].field];
                var fVal = jendo.parseFloat(val, 0);
                gColumns[sumData[c].ix].sum += fVal;
            }
        }
    }
}

// Презарежда редовете с данни на грида
JendoGridView.prototype.refreshRecords = function () {
    var gColumns = this._GVData.columns;
    var gRecords = this._GVData.records;
    var gSortData = this._GVData.sortData;
    var gFilterData = this._GVData.filterData;
    var tBody = this._Element.find('tbody[role="rowgroup"]');
    tBody.html('');

    // отбелязва маркираните колонки
    var tHead = this._Element.find('th[data-field]');
    tHead.removeClass('jd-field-sort');
    tHead.removeClass('jd-field-desort');
    for (var i = 0; i < gSortData.length; i++) {
        var fieldData = gSortData[i];
        if (fieldData.sort === -1) {
            this._Element.find('th[data-field="' + fieldData.field + '"]')
                .addClass('jd-field-desort');
        }
        else {
            this._Element.find('th[data-field="' + fieldData.field + '"]')
                .addClass('jd-field-sort');
        }
    }
    if (gFilterData.length > 0) {
        gRecords = gRecords.filterBy(gFilterData);
    }
    // изчислява суми по колони
    this.calculateColumnSums(gColumns, gRecords);
    // Rows
    this.appendGRows(tBody, gColumns, gRecords, this._GVData);
    // Paging
    this.appendGPaging(tBody, gColumns, gRecords, this._GVData);
};

// Презарежда целия грид
JendoGridView.prototype.refresh = function () {
    this.refreshRecords();
};

// Добавя видимите редовете с данни на грида
JendoGridView.prototype.appendGRows = function (tBody, gColumns, gRecords, gvData) {
    // индекс на първия ред
    var firstRowIndex = 0;
    // Брой на видимите редове за зареждане
    var lastRowIndex = gRecords.length;
    // Ако има paging, зарежда само редовете от страницата
    if (this._GVData.allowPaging) {
        firstRowIndex = (gvData.pageNo > 1) ? (gvData.pageNo - 1) * this._GVData.pageSize : 0;
        // Видимите редове са не повече от редовете на стр.
        var maxRowIndex = firstRowIndex + this._GVData.pageSize;
        if (lastRowIndex > maxRowIndex) {
            lastRowIndex = maxRowIndex;
        }
    }
    // обхожда редовете
    for (var i = firstRowIndex; i < lastRowIndex; i++) {
        var gRow = gRecords[i];
        // добавя ред към грида
        (typeof gRow === 'object')
            ? this.recordBound(i, tBody, gRow, gColumns)
            : this.recordBound(i, tBody, {}, gColumns);
    }
};

// Добавя ред за навигация по стр. на грида
JendoGridView.prototype.appendGPaging = function (tBody, gColumns, gRecords, gvData) {
    // добавя сумарен ред
    if (this._GVData.sumData.length > 0) {
        var tRow = tBody.appendTRow();
        tRow.addClass('jd-sum');
        // детайлна информация
        if (!jendo.isNull(this._GVData.detail)) {
            var jdDtlBtn = tRow.appendTD();
        }
        // номер на ред
        if (this._GVData.show.rowNo) {
            tRow.appendTD();
        }
        // Зарежда клетките от реда
        for (var f = 0; f < gColumns.length; f++) {
            var gCol = gColumns[f];
            // добавя колона
            var td = tRow.appendTD();
            var tdValue = this.formatRecordValue(gCol.sum, gCol);
            // подравняване на текста
            if ('align' in gCol) {
                td.style('text-align', gCol.align);
            }
            td.html(tdValue);
        }
    }
    // Добавя ред за навигация по стр. на грида
    if (gvData.allowPaging) {
        var thisJGV = this;
        // Създава реда за навигация на страниците
        var tRow = tBody.appendTRow()
            .addClass('jd-pager');

        var td = tRow.appendTD();
        // Брой колони на грида
        var countColumns = gColumns.length;
        // добавя колона за детайлна информация
        if (!jendo.isNull(gvData.detail)) { countColumns += 1; }
        // добавя колона за номер на ред
        if (gvData.show.rowNo) { countColumns += 1; }
        // Брой страници на грида
        var countPageTmp = gRecords.length / gvData.pageSize;
        gvData.countPage = Math.round(countPageTmp, 0);
        if (countPageTmp > gvData.countPage) {
            gvData.countPage += 1;
        }
        // Добавя реда за навигация
        td.attr('colspan', countColumns);
        var pager = gvData.pager;
        // бутони: първа страница
        if (pager.firstLast) {
            var btnFirst = td.appendButton()
                .addClass('jd-pager-btn')
                .click(function (e) {
                    thisJGV.pageFirst();
                });
            btnFirst.appendSpan().html('&#171;');

            if (gvData.pageNo === 1) {
                btnFirst.attr('disabled', '');
            }
        }
        // бутони: предишена страница
        if (pager.prevNext) {
            var btnPrev = td.appendButton()
                .addClass('jd-pager-btn')
                .click(function (e) {
                    thisJGV.pagePrev();
                });
            btnPrev.appendSpan().html('&#8249;');

            if (gvData.pageNo === 1) {
                btnPrev.attr('disabled', '');
            }
        }
        // бутони: номера на страници
        if (pager.numeric) {
            // var numericCount = (pager.numericCount > gvData.countPage) ? gvData.countPage : pager.numericCount;
            var pageNoFrom = gvData.pageNo - parseInt(pager.numericCount / 2);
            if (pageNoFrom < 1) {
                pageNoFrom = 1;
            }
            var pageNoTo = pageNoFrom + pager.numericCount - 1;
            if (pageNoTo > gvData.countPage) {
                pageNoTo = gvData.countPage;
                pageNoFrom = pageNoTo - pager.numericCount + 1;
                if (pageNoFrom < 1) {
                    pageNoFrom = 1;
                }
            }
            // бутони с номера на страници
            for (var i = pageNoFrom; i <= pageNoTo; i++) {
                var jdBtn = td.appendButton(i)
                    .addClass('jd-pager-btn')
                    .click(function (e) {
                        thisJGV.pageNo(e.data.pageNo);
                    }, {
                        pageNo: i
                    });
                if (gvData.pageNo === i) {
                    jdBtn.addClass('jd-pager-active');
                }
            }
        }
        // бутони: следваща страница
        if (pager.prevNext) {
            var btnNext = td.appendButton()
                .addClass('jd-pager-btn')
                .click(function (e) {
                    thisJGV.pageNext();
                });
            btnNext.appendSpan().html('&#8250;');

            if (gvData.pageNo === gvData.countPage) {
                btnNext.attr('disabled', '');
            }
        }
        // бутони: последна страница
        if (pager.firstLast) {
            var btnLast = td.appendButton()
                .addClass('jd-pager-btn')
                .click(function (e) {
                    thisJGV.pageLast();
                });
            btnLast.appendSpan().html('&#187;');

            if (gvData.pageNo === gvData.countPage) {
                btnLast.attr('disabled', '');
            }
        }
        // директно позициониране на страница
        if (pager.goToPage) {
            var pnlGTP = td.appendDiv()
                .addClass('jd-pager-panel');
            pnlGTP.appendSpan()
                .text('Page:');
            var textGTP = pnlGTP.appendText();
            pnlGTP.appendSpan()
                .text('of ' + gvData.countPage);
            pnlGTP.appendButton('Go')
                .addClass('jd-pager-btn')
                .click(function (e) {
                    var goToPage = parseInt(textGTP.val());
                    if (goToPage > 0) {
                        thisJGV.pageNo(textGTP.val());
                    }
                });
        }
        // брой редове на страница
        if (pager.pageSize) {
            var pnlPS = td.appendDiv()
                .addClass('jd-pager-panel');
            pnlPS.appendSpan()
                .text('Page size:');
            // Списък
            if (Array.isArray(pager.pageSizeList) && (pager.pageSizeList.length > 0)) {
                // Списък
            }
            // Стойност
            else {
                var textPS = pnlPS.appendText()
                    .val(gvData.pageSize);
                pnlPS.appendButton('Change')
                    .addClass('jd-pager-btn')
                    .click(function () {
                        console.log('Change', textPS.val());
                    });
            }
        }
    }
};

// Създава ред от грида
JendoGridView.prototype.recordBound = function (rowIndex, tBody, gRow, gColumns) {
    var self = this;
    // event: rowBinding
    if (!jendo.isNull(this._GVData.rowBindingEvent)) {
        this._GVData.rowBindingEvent({ ix: rowIndex, data: gRow });
    }
    var isAlt = (rowIndex % 2);
    var tRow = tBody.appendTRow();
    if (isAlt === 1) {
        tRow.addClass('jd-alt');
    }
    // детайлна информация
    if (!jendo.isNull(this._GVData.detail)) {
        var jdDtlBtn = tRow.appendTD().appendButton();
        jdDtlBtn.class('jd-grid-btn-dtl').text('\u2B9E');
        var colCount = 1 + (this._GVData.show.rowNo ? 1 : 0) + gColumns.length;
        jdDtlBtn.click(function () {
            var jdDtlRow = tBody.find('[data-dtl="' + rowIndex + '"]');
            var is_show = jdDtlBtn.prop('is_show');
            if (is_show) {
                jdDtlBtn.prop('is_show', false);
                jdDtlBtn.text('\u2B9E');
                jdDtlRow.hide();
            }
            else {
                jdDtlBtn.prop('is_show', true);
                jdDtlBtn.text('\u2B9F');
                if (jdDtlRow.length == 0) {
                    // добадя реда
                    var dtlRow = document.createElement('tr');
                    jdDtlRow = _(dtlRow);
                    jdDtlRow.class("jd-dtl");
                    jdDtlRow.attr('data-dtl', rowIndex);
                    tBody.insertAfter(dtlRow, tRow);
                    var jdDtlCell = jdDtlRow.appendTD();
                    jdDtlCell.attr('colspan', colCount);
                    // добавя грида
                    var jdGrid = jdDtlCell.appendDiv().attr('data-grid', rowIndex);
                    var grid = new JendoGridView(jdGrid);
                    grid.data(self._GVData.detail);
                    // event: detailCreate
                    if (!jendo.isNull(self._GVData.detailCreateEvent)) {
                        self._GVData.detailCreateEvent({ ix: rowIndex, data: gRow, row: tRow, grid: grid });
                    }
                    // event: detailLoad
                    if (!jendo.isNull(self._GVData.detailLoadEvent)) {
                        self._GVData.detailLoadEvent({ ix: rowIndex, data: gRow, row: tRow, grid: grid });
                    }
                }
                else {
                    jdDtlRow.attr('style', '');
                    // event: detailLoad
                    if (!jendo.isNull(self._GVData.detailLoadEvent)) {
                        var grid = _.gridView(jdDtlRow.find('[data-grid="' + rowIndex + '"]'));
                        self._GVData.detailLoadEvent({ ix: rowIndex, data: gRow, row: tRow, grid: grid });
                    }
                }
            }
        });
    }
    // Номер на ред
    if (this._GVData.show.rowNo) {
        tRow.appendTD().text((rowIndex + 1) + '.');
    }
    // Зарежда клетките от реда
    for (var f = 0; f < gColumns.length; f++) {
        var gCol = gColumns[f];
        var td = tRow.appendTD();
        // стил ако има
        if (!jendo.isNullOrWhiteSpace(gCol.style)) {
            td.addClass(gCol.style);
        }
        // колонка с данни
        if (!jendo.isNull(gCol.field)) {
            var tdValue = this.formatRecordValue(gRow[gCol.field], gCol);
            // подравняване на текста
            if ('align' in gCol) {
                td.style('text-align', gCol.align);
            }
            td.html(tdValue);
        }
        // конлонка с команди
        else if (!jendo.isNull(gCol.commands)) {
            for (var c = 0; c < gCol.commands.length; c++) {
                var cmd = gCol.commands[c];
                // данни за събитие
                var cmdValue = {
                    rowIndex: rowIndex,
                    colIndex: f,
                    isCreate: true
                };
                // добавя данните
                if (!jendo.isNull(cmd.fields)) {
                    for (var cf = 0; cf < cmd.fields.length; cf++) {
                        var fieldName = cmd.fields[cf];
                        cmdValue[fieldName] = gRow[fieldName];
                    }
                }
                // събитие: create
                if (!jendo.isNull(cmd.create)) {
                    cmd.create(td, cmdValue);
                }
                if (cmdValue.isCreate) {
                    var btn = td.appendButton(cmd.caption);
                    btn.class('jd-grid-btn-link');
                    if (!jendo.isNullOrWhiteSpace(cmd.style)) {
                        btn.addClass(cmd.style);
                    }
                    if (!jendo.isNullOrWhiteSpace(cmd.command)) {
                        btn.attr('data-command', cmd.command);
                    }
                    // събитие: click
                    if (!jendo.isNull(cmd.click)) {
                        // добавя събитието
                        btn.click(this.clickCommand(cmd.click, cmdValue));
                    }
                }
                // събитие: bound
                if (!jendo.isNull(cmd.bound)) {
                    cmd.bound(btn, cmdValue);
                }
            }
        }
        // конлонка с меню
        else if (!jendo.isNull(gCol.menu)) {
            var tdMenu = td.appendDiv().class('jd-grid-menu');
            tdMenu.appendButton().html('&#9776;').class('jd-grid-mbtn');
            var tdItems = tdMenu.appendDiv().class('jd-grid-mitems');
            if ('position' in gCol && gCol.position.toLowerCase() === 'right') {
                tdItems.addClass('jd-grid-mi-right');
            }
            else {
                tdItems.addClass('jd-grid-mi-left');
            }
            for (var c = 0; c < gCol.menu.length; c++) {
                var mi = gCol.menu[c];
                // данни за събитие
                var miValue = {
                    rowIndex: rowIndex,
                    colIndex: f,
                    isCreate: true
                };
                // добавя данните
                if (!jendo.isNull(mi.fields)) {
                    for (var cf = 0; cf < mi.fields.length; cf++) {
                        var fieldName = mi.fields[cf];
                        miValue[fieldName] = gRow[fieldName];
                    }
                }
                // събитие: create
                if (!jendo.isNull(mi.create)) {
                    mi.create(tdItems, miValue);
                }
                if (miValue.isCreate) {
                    // добавя бутона
                    var btn = tdItems.appendButton(mi.caption);
                    btn.class('jd-grid-mitem');
                    if (!jendo.isNullOrWhiteSpace(mi.style)) {
                        btn.addClass(mi.style);
                    }
                    if (!jendo.isNullOrWhiteSpace(mi.command)) {
                        btn.attr('data-command', mi.command);
                    }
                    // събитие: click
                    if (!jendo.isNull(mi.click)) {
                        // добавя събитието
                        btn.click(this.clickCommand(mi.click, miValue));
                    }
                }
                // събитие: bound
                if (!jendo.isNull(mi.bound)) {
                    mi.bound(btn, miValue);
                }
            }
        }
        // колонка с данни от функция
        else if (typeof gCol.bind === 'function') {
            var tdFuncVal = gCol.bind({
                rowIndex: rowIndex,
                colIndex: f,
                row: gRow
            });
            var jdBItem = td.html(tdFuncVal);
            // резултат от добавянето на елемента
            if (typeof gCol.binded === 'function') {
                gCol.binded({
                    rowIndex: rowIndex,
                    colIndex: f,
                    row: gRow,
                    jdItem: jdBItem
                });
            }
        }
    }
    // event: rowBound
    if (!jendo.isNull(this._GVData.rowBoundEvent)) {
        this._GVData.rowBoundEvent({ ix: rowIndex, data: gRow, row: tRow });
    }
};

JendoGridView.prototype.formatRecordValue = function (cValue, gCol) {
    // форматиране на дата
    if ('dateFormat' in gCol) {
        var tdVDate = Date.parse(cValue);
        if (!isNaN(tdVDate)) {
            return jendo.dateFormatUI(new Date(tdVDate), gCol.dateFormat);
        }
        else {
            return cValue;
        }
    }
    // форматиране на число
    else if ('numberFixed' in gCol) {
        var tdVNum = jendo.parseFloat(cValue, NaN);
        if (!isNaN(tdVNum)) {
            return tdVNum.toFixed(gCol.numberFixed)
        }
        else {
            return cValue;
        }
    }
    else {
        return cValue;
    }
};

// Добавя функция към бутона на реда за команда
JendoGridView.prototype.clickCommand = function (commandClick, e) {
    return function (event) {
        commandClick(event, e);
    };
};

JendoGridView.prototype.sortData = function (value) {
    var sortData = this._GVData.sortData;
    if (!jendo.isNull(value)) {
        sortData.splice(0, sortData.length);
        for (var i = 0; i < value.length; i++) {
            sortData.push(value[i]);
        }
    }
    this._GVData.records.sortBy(sortData);
    this.refreshRecords();
};

// Дава или задава номер страница
JendoGridView.prototype.pageNo = function (value) {
    if (jendo.isNullOrWhiteSpace(value)) {
        return this._GVData.pageNo;
    }
    else {
        var gvData = this._GVData;
        if ((value > 0) && (value <= gvData.countPage)) {
            gvData.pageNo = value;
            this.refreshRecords();
        }
    }
};
// Премества стр. на предната
JendoGridView.prototype.pagePrev = function () {
    var gvData = this._GVData;
    this.pageNo(gvData.pageNo - 1);
};
// Премества стр. на следващата
JendoGridView.prototype.pageNext = function () {
    var gvData = this._GVData;
    this.pageNo(gvData.pageNo + 1);
};
// Премества стр. на първата
JendoGridView.prototype.pageFirst = function () {
    this.pageNo(1);
};
// Премества стр. на последната
JendoGridView.prototype.pageLast = function () {
    var gvData = this._GVData;
    this.pageNo(gvData.countPage);
};

JendoGridView.prototype.wait = function (value) {
    this.html('<div class="jd-grid-wait"><div class="jd-loader-orbit jd-xl4"></div><span>' + value + '</span></div>');
};

JendoGridView.prototype.text = function (value) {
    this.html('<div class="jd-grid-text">' + value + '</div>');
};

JendoGridView.prototype.html = function (value) {
    var tBody = this._GridView.find('[role="rowgroup"]'); // Грид
    if (tBody.length === 0) {
        tBody = this._GridView.appendTBody().role("rowgroup");
    }
    // Брой колони на грида
    var countColumns = this._GVData.columns.length;
    // добавя колона за детайлна информация
    if (!jendo.isNull(this._GVData.detail)) { countColumns += 1; }
    // добавя колона за номер на ред
    if (this._GVData.show.rowNo) { countColumns += 1; }

    tBody.html('<tr><td colspan="' + countColumns + '">' + value + '</td></tr>');
};

JendoGridView.prototype.saveAsExcel = function (args) {
    if (jendo.isNull(args)) { args = {}; }
    // Specify file name
    var fName = jendo.isNullOrWhiteSpace(args.file) ? 'document.xls' : args.file + '.xls';
    // Specify title
    var fTitle = jendo.isNullOrWhiteSpace(args.title) ? 'Document' : args.title;
    var fHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>" + fTitle + "</title></head><body>" +
        this.dataToHtml(args) + "</body></html>";
    // Specify link url
    var fUrl = 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(fHtml);
    // Create download link element
    var downloadLink = document.createElement("a");
    document.body.appendChild(downloadLink);
    if (navigator.msSaveOrOpenBlob) {
        var blob = new Blob(['\ufeff', fHtml], {
            type: 'application/vnd.ms-excel'
        });
        navigator.msSaveOrOpenBlob(blob, fName);
    } else {
        // Create a link to the file
        downloadLink.href = fUrl;
        // Setting the file name
        downloadLink.download = fName;
        //triggering the function
        downloadLink.click();
    }
    document.body.removeChild(downloadLink);
}

JendoGridView.prototype.dataToHtml = function (args) {
    // добавя заглавния ред
    var hRow = '';
    // Добавя номер на ред в заглавния ред
    if (this._GVData.show.rowNo) {
        hRow += '<th>#</th>';
    }
    var gCols = this._GVData.columns;
    // Добавя останалите клетки в заглавния ред
    for (var i = 0; i < gCols.length; i++) {
        var gCol = gCols[i];
        if (!jendo.isNullOrEmpty(gCol.field)) {
            // Заглавната част на колонката            
            hRow += '<th>' + (jendo.isNullOrEmpty(gCol.caption) ? '' : gCol.caption) + '</th>';
        }
    }
    var hTHRows = '<tr>' + hRow + '</tr>';
    // добавя редовете с данни
    var hTBRows = '';
    var gRecs = this._GVData.records;
    for (var r = 0; r < gRecs.length; r++) {
        var gRec = gRecs[r];
        hRow = '';
        if (this._GVData.show.rowNo) {
            hRow += '<td>' + (r + 1) + '</td>';
        }
        for (var c = 0; c < gCols.length; c++) {
            var gRCol = gCols[c];
            if (!jendo.isNullOrEmpty(gRCol.field)) {
                var val = gRec[gRCol.field];
                if (jendo.isNull(val)) {
                    hRow += '<td></td>';
                }
                else {
                    hRow += '<td>' + this.formatRecordValue(val, gRCol) + '</td>';
                }
            }
        }
        hTBRows += '<tr>' + hRow + '</tr>';
    }
    // заглавие
    var hTitle = '';
    if (!jendo.isNullOrWhiteSpace(args.title)) {
        hTitle = '<h2>' + args.title + '</h2>';
    }
    return hTitle +
        '<table border="1"><thead>' + hTHRows + '</thead><tbody>' + hTBRows + '</tbody></table>';
}

jendo.gridView = function (element) {
    if (!jendo.isNull(element)) {
        var targetElement = jendo(element);
        if (targetElement.length > 0) {
            return new JendoGridView(targetElement);
        }
    }
};
/* END: Jendo GridView
*/
