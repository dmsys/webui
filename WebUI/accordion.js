﻿/* BEGIN: Accordion Menu
 */
// Jendo Accordion Menu
function JendoAccordion(elements) {
    // this.sectionElements = elements;
    this.jdAccMenu = jendo(elements);
    // данни за менюто
    this._AMData = this.jdAccMenu.prop('amdata');
    // aко не са инициализирани
    if (jendo.isNull(this._AMData)) {
        this._AMData = {
            expandChar: '&#709;',
            collapseChar: '&#708;'
        };
        this.jdAccMenu.prop('amdata', this._AMData);
    }
}

// зарежда менюто с данни
JendoAccordion.prototype.data = function (value) {
    if (jendo.isNull(value)) {
        return this;
    }
    if ('expandChar' in value) {
        this._AMData.expandChar = value.expandChar;
    }
    if ('collapseChar' in value) {
        this._AMData.collapseChar = value.collapseChar;
    }
    return this;
}

// Добавя елемент за навигация
JendoAccordion.prototype.append = function (jsonData, menu) {
    if (!jendo.isNull(this.jdAccMenu) && !jendo.isNull(jsonData)) {
        var jdMenu = jendo.isNull(menu) ? this.jdAccMenu : jendo(menu);
        // console.log(menu)
        // елемент от менюто
        var menuItem = jdMenu.appendLI();
        if ('id' in jsonData) {
            menuItem.attr('data-id', jsonData.id);
        }
        // Ако има подменю
        if ('items' in jsonData) {
            menuItem.class('dropdown');
            // Видим елемент
            var userItem = null;
            // Има подаден линк
            if ('link' in jsonData) {
                userItem = menuItem.appendA(jsonData.link, jsonData.text);
            }
            // Няма подаден линк, създава бутон
            else {
                userItem = menuItem.appendButton()
                    .html('<span class="menu-text">' + jsonData.text + '</span><span class="menu-down">' + this._AMData.expandChar +
                    '</span><span class="menu-up">' + this._AMData.collapseChar + '</span>');
            }
            // var thisNB = this;
            userItem.attr('role', 'button')
                // събитие: отваря подменю
                .click(function (source) {
                    // избрания елемент
                    var jdItem = jendo(source.target);
                    // менюто на избрания елемент
                    var jdItemDD = jdItem.closest('.dropdown');
                    // други отворени на същото ниво
                    var jdOpenDD = jdItemDD.parent().find('.open');
                    // затваря другите менюта
                    jdOpenDD.removeClass('open');
                    // отваря менюто на избрания елемент, ако не е оворен
                    if (!jdOpenDD.is(jdItemDD)) {
                        jdItemDD.addClass('open');
                    }
                });
            // зарежда елементите на подменюто
            var jdSubItem = menuItem.appendUL()
                .class('dropdown-menu')
                .attr('role', 'menu');
            // (new JendoAccordion(jdSubItem)).load(jsonData.items);
            this._loadMenu(jdSubItem, jsonData.items);
        }
        // Ако няма подменю
        else {
            // линк
            if (!jendo.isNullOrWhiteSpace(jsonData.link)) {
                menuItem.appendA(jsonData.link)
                    .html('<span class="menu-text">' + jsonData.text + '</span>');
            }
            // бутон
            else {
                var self = this;
                var btnItem = menuItem.appendButton()
                    .html('<span class="menu-text">' + jsonData.text + '</span>');
                btnItem.click(function () {
                    // Dispatch the event.
                    document.dispatchEvent(new CustomEvent('accordion.navigate', {
                        detail: { src: self.jdAccMenu, args: jsonData.args }
                    }));
                });
            }
        }
    }
};

// Затваря отворения елемент, ако източника е извън него
JendoAccordion.prototype.closeElement = function (source) {
    var jdSrcDD = _(source).closest('.dropdown');
    if (jdSrcDD.length > 0) {
        var jdOpenDD = jdSrcDD.parent().find('.open');
        jdOpenDD.removeClass('open');
    }
};

// Зарежда елементите за навигация
JendoAccordion.prototype.load = function (jsonData, mItem) {
    return this._loadMenu(this.jdAccMenu, jsonData, mItem);
};
// Зарежда елементите за навигация
JendoAccordion.prototype._loadMenu = function (jdMenu, jsonData, mItem) {
    this.jdAccMenu = jdMenu;
    if (!jendo.isNull(jdMenu) && !jendo.isNull(jsonData)) {
        var jdMItem = jendo.isNull(mItem) ? jdMenu
            : jendo(jdMenu.find('[data-id=' + mItem + ']').find('[role="menu"]').first());
        jdMItem.html('');
        for (var i = 0; i < jsonData.length; i++) {
            this.append(jsonData[i], jdMItem);
        }
    }
    return this;
};

// Event: Навигация
JendoAccordion.prototype.navigate = function (func) {
    var self = this;
    document.addEventListener('accordion.navigate', function (e) {
        if (self.jdAccMenu.isContains(e.detail.src)) {
            func(e, e.detail.args);
        }
    }, false);
    return this;
};

// Jendo Accordion Menu
jendo.accordion = function (element) {
    // jendo._IsInitAccordion = true;
    if (jendo.isNull(element)) {
        return new JendoAccordion(null);
    }
    else {
        var jdElement = jendo(element);
        if (jdElement.length > 0) {
            var jdElUl = jdElement.find('ul');
            if (jdElUl.length > 0) {
                return new JendoAccordion(jdElUl);
            }
            else {
                return new JendoAccordion(jdElement.appendUL());
            }
        }
        else {
            return new JendoAccordion(null);
        }
    }
};
/* END: Accordion Menu
*/