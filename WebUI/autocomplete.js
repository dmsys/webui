﻿/* BEGIN: Autocomplete
 * Options
 *      caseSensitive   : false - Ако е true, различава главни от малки букви
 *      autoFocus       : false - Ако е true, първият елемент автоматично ще бъде фокусиран, когато се покаже менюто.
 * Events
 *      create  : когато е създаден
 *      change  : когато напусне полето, ако стойността е променена
 *      select  : когато е избран елемент от менюто
 *      open    : когато менюто бъде отворено или актуализирано
 *      close   : когато менюто е скрито
 */
function JendoAutocomplete(args) {
    var self = this;
    this.jdElement = args.element;
    this.isInit = this.jdElement.prop('jd_init') === '1';
    if (this.isInit) {
        if ('handle' in args) {
            this.handle = args.handle;
        }
        if ('caseSensitive' in args) {
            this.caseSensitive = args.caseSensitive === true;
        }
        if ('autoFocus' in args) {
            this.autoFocus = args.autoFocus === true;
        }
        if ('select' in args) {
            this.eventSelect = args.select;
        }
        if ('open' in args) {
            this.eventOpen = args.open;
        }
        if ('close' in args) {
            this.eventClose = args.close;
        }
        if ('change' in args) {
            this.eventChange = args.change;
        }
    }
    else {
        this.handle = args.handle;
        this.jdList = null;
        // различава главни от малки букви
        this.caseSensitive = args.caseSensitive === true;
        // първият елемент автоматично ще бъде фокусиран
        this.autoFocus = args.autoFocus === true;
        this.selItem = null;
        this.selIndex = -1;
        this.selData = '';
        // events
        this.eventSelect = args.select;
        this.eventOpen = args.open;
        this.eventClose = args.close;
        this.eventChange = args.change;

        this.jdElement.attr('autocomplete', 'off');
        this.jdElement.focus(function (e) {
            self.createList(e.target);
        });
        this.jdElement.keyDown(function (e) {
            // 9: Tab
            if (e.keyCode === 9) {
                self.close();
            }
        });
        this.jdElement.keyUp(function (e) {
            // 37: ArrowLeft, 38: ArrowUp
            if (e.keyCode === 38 || e.keyCode === 37) {
                if (self.jdList) {
                    var jdPChlds = self.jdList.children();
                    var prvIndex = self.selIndex - 1;
                    self.itemActive(jdPChlds, prvIndex);
                }
            }
            // 39: ArrowRight, 40: ArrowDownm
            else if (e.keyCode === 40 || e.keyCode === 39) {
                if (self.jdList) {
                    var jdNChlds = self.jdList.children();
                    var nxtIndex = self.selIndex + 1;
                    self.itemActive(jdNChlds, nxtIndex);
                }
            }
            // 13: Enter
            else if (e.keyCode === 13) {
                if (self.jdList && self.selItem) {
                    self.item(self.selItem.prop('jd-item'));
                    self.close();
                    e.stopImmediatePropagation();
                }
            }
            // 27: Escape
            else if (e.keyCode === 27) {
                self.close();
            }
            // 9: Tab
            else if (e.keyCode === 9) {
                self.close();
            }
            else {
                self.createList(e.target);
                var term = self.jdElement.val();
                var srcType = typeof args.source;
                if (srcType === 'function') {
                    args.source({
                        term: term
                    }, function (data) {
                        self.loadList(data, term);
                    });
                }
                else if (Array.isArray(args.source)) {
                    self.loadList(args.source, term);
                }
            }
        });
        this.jdElement.blur(function (e) {
            var dItem = self.jdElement.prop('jd-item');
            if (jendo.isNull(dItem)) {
                dItem = '';
            }
            self.invokeChangeEvent(dItem);
        });
        // Затваря отворения елемент
        document.addEventListener('click', function (e) {
            if (!self.jdElement.isContains(e.target)) {
                self.close();
            }
        }, true);
        // събитие: create
        if (!jendo.isNull(args.create)) {
            args.create({
                type: "autocompletecreate",
                target: this.jdElement,
                timeStamp: new Date().getTime()
            });
        }
        // зарежда данни, ако има въведен текст
        var term = this.jdElement.val();
        if (!jendo.isNullOrEmpty(term)) {
            var srcType = typeof args.source;
            if (srcType === 'function') {
                args.source({
                    term: term
                }, function (data) {
                    var si = self.getFirstItem(data, term);
                    self.item(si);
                    self.selData = si;
                });
            }
            else if (Array.isArray(args.source)) {
                var si = self.getFirstItem(args.source, term);
                self.item(si);
                self.selData = si;
            }
        }
        // инициализирано
        this.jdElement.prop('jd_init', '1');
    }
}
/* Създава списъка с елементи
 */
JendoAutocomplete.prototype.createList = function (target) {
    if (jendo.isNull(this.jdList)) {
        var jdTarget = jendo(target);
        var trTop = 0; // jdTarget.top();
        var trLeft = 0; // jdTarget.left();
        var trWidth = jdTarget.offsetWidth();
        var jdHandle = null;
        if (!jendo.isNull(this.handle)) {
            jdHandle = jendo(this.handle);
            var rect = target.getBoundingClientRect();
            trTop = rect.bottom - jdHandle.top() + window.scrollY;
            trLeft = rect.left - jdHandle.left() - window.scrollX;
            trWidth = rect.width;
        }
        else {
            jdHandle = jendo(document.body);
            // trTop += jdTarget.offsetHeight();
            var rectT = target.getBoundingClientRect();
            trTop = rectT.top + rectT.height + window.scrollY;
            trLeft = rectT.left - window.scrollX;
        }
        this.jdList = jdHandle.appendUL();
        this.jdList.class('jd-autocomplete');
        this.jdList.attr('tabindex', 0);
        this.jdList.top(trTop);
        this.jdList.left(trLeft);
        this.jdList.style('width', trWidth + 'px');
    }
}
/* Зарежда списъка с елементи
 */
JendoAutocomplete.prototype.loadList = function (data, term) {
    if (this.jdList) {
        this.jdList.html('');
        this.valueClear();
        this.jdList.show();
        // събитие: open
        if (!jendo.isNull(this.eventOpen)) {
            this.eventOpen({
                type: "autocompleteopen",
                target: this.jdElement,
                timeStamp: new Date().getTime()
            });
        }
        // филтриране
        var termVl = this.caseSensitive ? term : term.toLowerCase();
        for (var i = 0; i < data.length; i++) {
            var row = data[i];
            var rowType = typeof row;
            if (rowType === 'string') {
                var rowVl = this.caseSensitive ? row : row.toLowerCase();
                if (rowVl.indexOf(termVl) > -1) {
                    this.appendLItem(row, row, {
                        label: row,
                        value: row
                    });
                }
            }
            else if (rowType === 'object') {
                var labelVl = this.caseSensitive ? row.label : row.label.toLowerCase();
                if (labelVl.indexOf(termVl) > -1) {
                    this.appendLItem(row.label, row.value, row);
                }
            }
            else {
                console.log(rowType);
            }
        }
        // първият елемент автоматично ще бъде фокусиран
        if (this.autoFocus) {
            this.itemActive(this.jdList.children(), 0);
        }
    }
};

JendoAutocomplete.prototype.getFirstItem = function (data, term) {
    var termVl = this.caseSensitive ? term : term.toLowerCase();
    for (var i = 0; i < data.length; i++) {
        var row = data[i];
        var rowType = typeof row;
        if (rowType === 'string') {
            var rowVl = this.caseSensitive ? row : row.toLowerCase();
            if (rowVl.indexOf(termVl) > -1) {
                return {
                    label: row,
                    value: row
                };
            }
        }
        else if (rowType === 'object') {
            var labelVl = this.caseSensitive ? row.label : row.label.toLowerCase();
            if (labelVl.indexOf(termVl) > -1) {
                return row;
            }
        }
        else {
            console.log(rowType);
        }
    }
    return null;
};

/* Добавя елемент от списъка
 */
JendoAutocomplete.prototype.appendLItem = function (label, value, data) {
    var self = this;
    var li = this.jdList.appendLI();
    li.class('jd-ac-item');

    var liText = li.appendDiv()
        .class('jd-ac-text')
        .mouseEnter(function (e) {
            jendo(e.target).addClass('jd-ac-active');
        })
        .mouseLeave(function (e) {
            jendo(e.target).removeClass('jd-ac-active');
        })
        // избра елемент
        .mouseDown(function (e) {
            var jdTarget = jendo(e.target);
            var dItem = jdTarget.prop('jd-item');
            if (jendo.isNull(dItem)) {
                dItem = '';
            }
            self.item(dItem);
            self.close();
            self.invokeChangeEvent(dItem);
        });

    liText.text(label);
    liText.prop('jd-item', data);
};
/* Избран е елемент от менюто
 * label, value, data
 */
JendoAutocomplete.prototype.item = function (data) {
    // запазва избрания елемент
    if (data) {
        this.jdElement.val(data.label);
        this.jdElement.attr('jd-value', data.value);
        this.jdElement.prop('jd-item', data);
        // събитие: select
        if (!jendo.isNull(this.eventSelect)) {
            this.eventSelect({
                type: "autocompleteselect",
                target: this.jdElement,
                timeStamp: new Date().getTime()
            }, data);
        }
    }
    // дава избрания елемент
    else {
        return this.jdElement.prop('jd-item');
    }
};
/* Премахва селектирания елемент
 */
JendoAutocomplete.prototype.itemClear = function () {
    this.jdElement.val('');
    this.jdElement.attr('jd-value', '');
    this.jdElement.prop('jd-item', '');
    this.selItem = null;
    this.selIndex = -1;
};
/* Премахва данните на селектирания елемент
 */
JendoAutocomplete.prototype.valueClear = function () {
    this.jdElement.attr('jd-value', '');
    this.jdElement.prop('jd-item', '');
    this.selItem = null;
    this.selIndex = -1;
};
/* Селектира елемент от менюто
 */
JendoAutocomplete.prototype.itemActive = function (jdChlds, idx) {
    if (idx >= 0 && idx < jdChlds.length) {
        if (this.selItem) {
            this.selItem.removeClass('jd-ac-active');
            this.selItem = null;
        }
        var jdLI = jendo(jdChlds.item(idx));
        this.selIndex = idx;
        this.selItem = jendo(jdLI.children().first());
        this.selItem.addClass('jd-ac-active');
    }
};
/* Затваря списъка
 */
JendoAutocomplete.prototype.close = function () {
    if (this.jdList) {
        this.jdList.hide();
        // събитие: close
        if (!jendo.isNull(this.eventClose)) {
            this.eventClose({
                type: "autocompleteclose",
                target: this.jdElement,
                timeStamp: new Date().getTime()
            });
        }
        this.jdList = null;
    }
};

JendoAutocomplete.prototype.invokeChangeEvent = function (dItem) {
    var evItem = null;
    if (dItem === '') {
        var elVal = this.jdElement.val();
        if (this.selData !== elVal) {
            this.selData = elVal;
            evItem = { value: -1, label: elVal };
        }
    }
    else if (this.selData !== dItem) {
        this.selData = dItem;
        evItem = dItem;
    }
    // променен е елемента
    if (!jendo.isNull(this.eventChange) && !jendo.isNull(evItem)) {
        this.eventChange({
            type: "autocompletechange",
            target: this.jdElement,
            timeStamp: new Date().getTime()
        }, evItem);
    }
};

JendoSelector.prototype.autocomplete = function (args) {
    if (jendo.isNull(args)) {
        args = {
            element: this
        };
    }
    else {
        args.element = this;
    }
    return new JendoAutocomplete(args);
};
/* END: Autocomplete
 */