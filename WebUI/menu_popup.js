﻿/* BEGIN: Popup Menu
 */
function JendoPopupMenu(jdBtn, args) {
    this.jdMenuUL = null;
    var jdMenu = null;
    if (typeof args === 'string') {
        jdMenu = jendo(args);
        this.jdMenuUL = jdMenu.find('ul');
        if (this.jdMenuUL.length === 0) {
            this.jdMenuUL = jdMenu.appendUL();
        }
    }
    else if (typeof args === 'object') {
        jdMenu = jendo(args.menu);
        if (jdMenu.length === 0) {
            jdMenu = jendo(document.body).appendDiv();
        }
        this.jdMenuUL = jdMenu.find('ul');
        if (this.jdMenuUL.length === 0) {
            this.jdMenuUL = jdMenu.appendUL();
        }
        this.load(args.data);
    }
    if (!jdMenu.hasClass('jd-popup-menu')) {
        jdMenu.addClass('jd-popup-menu');
        jdMenu.addClass('jd-popup-menu-default');
    }
    this.jdMBtn = jdBtn;
    var self = this;
    jendo(document.body).click(function (e) {
        if (jdMenu) {
            var jdMenuBtn = jdMenu.first().btn;
            if (jdMenuBtn && jdMenu.isVisible()) {
                if ((jdBtn === jdMenuBtn) && !jdMenuBtn.isContains(e.target)) {
                    if (self._dispatchHideEvent) {
                        self._dispatchHideEvent();
                    }
                    jdMenu.hide();
                }
            }
        }
    });
    jdBtn.click(function () {
        if (jdMenu) {
            if (jdMenu.isVisible()) {
                if (jdBtn === jdMenu.first().btn) {
                    if (self._dispatchHideEvent) {
                        self._dispatchHideEvent();
                    }
                    jdMenu.hide();
                }
            }
            else {
                if (self._dispatchShowEvent) {
                    self._dispatchShowEvent();
                }
                if ((typeof args === 'object') && ('init' in args) && !jdMenu.isInit) {
                    args.init(self, self.jdMenuUL);
                    jdMenu.isInit = true;
                }
                jdMenu.first().btn = jdBtn;
                jdMenu.top(jdBtn.top() + jdBtn.offsetHeight());
                jdMenu.left(jdBtn.left());
                jdMenu.show();
            }
        }
    });
}

JendoPopupMenu.prototype.load = function (data) {
    if (Array.isArray(data) && this.jdMenuUL) {
        for (var i = 0; i < data.length; i++) {
            this.append(data[i]);
        }
    }
}

JendoPopupMenu.prototype.append = function (data) {
    if (this.jdMenuUL) {
        var jdLI = this.jdMenuUL.appendLI();
        if ('link' in data) {
            return jdLI.appendA(data.link, data.text);
        }
        else {
            var self = this;
            return jdLI.appendButton().html(data.text)
                .click(function () {
                    // Dispatch the event.
                    document.dispatchEvent(new CustomEvent('popupmenu.navigate', {
                        detail: { src: self.jdMBtn, args: data.args }
                    }));
                });
        }
    }
}

// Event: Навигация
JendoPopupMenu.prototype.navigate = function (func) {
    var self = this;
    document.addEventListener('popupmenu.navigate', function (e) {
        if (e.detail.src === self.jdMBtn) {
            func(e, e.detail.args);
        }
    }, false);
    return this;
};

// Event: show
JendoPopupMenu.prototype._dispatchShowEvent = null;
JendoPopupMenu.prototype.showEvent = function (func) {
    this._dispatchShowEvent = function () {
        document.dispatchEvent(new CustomEvent('popup.menu.show', null));
    };
    document.addEventListener('popup.menu.show', func, false);
    return this;
};

// Event: hide
JendoPopupMenu.prototype._dispatchHideEvent = null;
JendoPopupMenu.prototype.hideEvent = function (func) {
    this._dispatchHideEvent = function () {
        document.dispatchEvent(new CustomEvent('popup.menu.hide', null));
    };
    document.addEventListener('popup.menu.hide', func, false);
    return this;
};

JendoSelector.prototype.popupmenu =
    JendoSelector.prototype.popupMenu = function (args) {
        return new JendoPopupMenu(this, args);
    };

jendo.popupmenu =
    jendo.popupMenu = function (el, args) {
        return new JendoPopupMenu(jendo(el), args);
    };
/* END: Popup Menu
*/
