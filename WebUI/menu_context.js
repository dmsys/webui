﻿/* BEGIN: Context Menu
 */
function JendoContextMenu(jdBtn, args) {
    if (jdBtn.length === 0) {
        return this;
    }
    var menuAlign = 'left';
    this.jdMBtn = jdBtn;
    var self = this;
    var jdMenuPanel = jdBtn.parent();
    this.jdMenuUL = null;
    var jdMenu = null;
    if (typeof args === 'string') {
        jdMenu = jendo(args);
        if (jdMenu.length === 0) {
            jdMenu = jdMenuPanel.appendDiv();
        }
        this.jdMenuUL = jdMenu.find('ul');
        if (this.jdMenuUL.length === 0) {
            this.jdMenuUL = jdMenu.appendUL();
        }
    }
    else if (typeof args === 'object') {
        jdMenu = jendo(args.menu);
        if (jdMenu.length === 0) {
            jdMenu = jdMenuPanel.appendDiv();
        }
        this.jdMenuUL = jdMenu.find('ul');
        if (this.jdMenuUL.length === 0) {
            this.jdMenuUL = jdMenu.appendUL();
        }
        if ('align' in args) {
            if (args.align === 'left' || args.align === 'right') {
                menuAlign = args.align;
            }
        }
        this.load(args.data);
    }
    if (!jdMenuPanel.hasClass('jd-context-menu')) {
        jdMenuPanel.addClass('jd-context-menu');
        jdMenuPanel.addClass('jd-context-menu-default');
    }
    if (!jdBtn.hasClass('jd-menu-btn')) {
        jdBtn.addClass('jd-menu-btn');
    }
    if (!jdMenu.hasClass('jd-menu-items')) {
        jdMenu.addClass('jd-menu-items');
    }
    if (menuAlign === 'right') {
        jdMenu.addClass('jd-menu-right');
    }
    jendo(document.body).click(function (e) {
        if (jdMenu) {
            var jdMenuBtn = jdMenu.first().btn;
            if (jdMenuBtn && jdMenu.isVisible()) {
                if ((jdBtn === jdMenuBtn) && !jdMenuBtn.isContains(e.target)) {
                    if (self._dispatchHideEvent) {
                        self._dispatchHideEvent();
                    }
                    jdMenu.hide();
                }
            }
        }
    });
    jdBtn.click(function () {
        if (jdMenu) {
            if (jdMenu.isVisible()) {
                if (jdBtn === jdMenu.first().btn) {
                    if (self._dispatchHideEvent) {
                        self._dispatchHideEvent();
                    }
                    jdMenu.hide();
                }
            }
            else {
                if (self._dispatchShowEvent) {
                    self._dispatchShowEvent();
                }
                if ((typeof args === 'object') && ('init' in args) && !jdMenu.isInit) {
                    args.init(self, self.jdMenuUL);
                    jdMenu.isInit = true;
                }
                jdMenu.first().btn = jdBtn;
                jdMenu.show();
            }
        }
    });
}

JendoContextMenu.prototype.load = function (data) {
    if (Array.isArray(data) && this.jdMenuUL) {
        for (var i = 0; i < data.length; i++) {
            this.append(data[i]);
        }
    }
}

JendoContextMenu.prototype.append = function (data) {
    if (this.jdMenuUL) {
        var jdLI = this.jdMenuUL.appendLI();
        if ('link' in data) {
            return jdLI.appendA(data.link, data.text);
        }
        else {
            var self = this;
            var jdBtn = jdLI.appendButton().html(data.text);
            jdBtn.click(function () {
                // Dispatch the event.
                document.dispatchEvent(new CustomEvent('contextmenu.navigate', {
                    detail: { src: self.jdMBtn, args: data.args }
                }));
            });
            if ('click' in data) {
                jdBtn.click(function () {
                    data.click(jdBtn, data.args);
                });
            }
            return jdBtn;
        }
    }
}

// Event: Навигация
JendoContextMenu.prototype.navigate = function (func) {
    var self = this;
    document.addEventListener('contextmenu.navigate',
        function (e) {
            if (e.detail.src === self.jdMBtn) {
                func(e, e.detail.args);
            }
        }, false);
    return this;
};

// Event: show
JendoContextMenu.prototype._dispatchShowEvent = null;
JendoContextMenu.prototype.showEvent = function (func) {
    this._dispatchShowEvent = function () {
        document.dispatchEvent(new CustomEvent('context.menu.show', null));
    };
    document.addEventListener('context.menu.show', func, false);
    return this;
};

// Event: hide
JendoContextMenu.prototype._dispatchHideEvent = null;
JendoContextMenu.prototype.hideEvent = function (func) {
    this._dispatchHideEvent = function () {
        document.dispatchEvent(new CustomEvent('context.menu.hide', null));
    };
    document.addEventListener('context.menu.hide', func, false);
    return this;
};

JendoSelector.prototype.contextmenu =
    JendoSelector.prototype.contextMenu = function (args) {
        return new JendoContextMenu(this, args);
    };

jendo.contextmenu =
    jendo.contextMenu = function (el, args) {
        return new JendoContextMenu(jendo(el), args);
    };
/* END: Context Menu
*/
