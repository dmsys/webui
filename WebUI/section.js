﻿/* BEGIN: Section
*/
function JendoSection(elements) {
    this.sectionElements = elements;
}
/*
*/
JendoSection.prototype.append = function (jsonData) {
    if (!jendo.isNull(this.sectionElements) && !jendo.isNull(jsonData)) {
        var section = document.createElement('div');
        section.className = 'jd-section jd-section-info';

        var header = document.createElement('div')
        header.className = 'jd-header';
        section.appendChild(header);

        var headerH3 = document.createElement('h3');
        header.appendChild(headerH3);

        var headerA = document.createElement('A');
        headerA.href = jsonData.link;
        headerA.appendChild(document.createTextNode(jsonData.title));
        headerH3.appendChild(headerA);

        var summary = document.createElement('div');
        summary.className = 'jd-summary';
        summary.appendChild(document.createTextNode(jsonData.text));
        section.appendChild(summary);

        this.sectionElements.append(section);
    }
}
/*
*/
JendoSection.prototype.load = function (jsonData) {
    if (!jendo.isNull(this.sectionElements) && !jendo.isNull(jsonData)) {

        for (var i = 0; i < jsonData.length; i++) {
            this.append(jsonData[i]);
        }
    }
}

jendo.section = function (element) {
    if (jendo.isNull(element)) {
        return new JendoSection(null);
    }
    else {
        var targetElement = jendo(element);
        if (targetElement.length > 0) {
            return new JendoSection(targetElement);
        }
        else {
            return new JendoSection(null);
        }
    }
}
/* END: Section
*/
