﻿/* BEGIN: Combobox
 */
function JendoCombobox(args) {
    var self = this;
    this.jdElement = args.element;
    if (args.disabled) {
        this.jdElement.prop('disabled', args.disabled);
    }
    // събитие: change
    if (!jendo.isNull(args.change)) {
        this.jdElement.change(function (e) {            
            if (e.target.options && e.target.selectedIndex) {
                var selOpt = e.target.options[e.target.selectedIndex];            
                var item = {
                    label: selOpt.innerText,
                    value: selOpt.value
                };
                args.change(e, item);
            }
            else {
                args.change(e);
            }
        });
    }
    // данни
    if (args.source) {
        // изчиства старите елементи
        this.clear();
        var srcType = typeof args.source;
        // зарежда елементите от ф-я
        if (srcType === 'function') {
            args.source(function (data) {
                // зарежда елементите от масив
                if (Array.isArray(data)) {
                    var dLen = data.length;
                    for (var di = 0; di < dLen; di++) {
                        self.add(data[di]);
                    }
                    if (args.value) {
                        self.jdElement.val(args.value);
                    }
                }
            });
        }
        // зарежда елементите от масив
        else if (Array.isArray(args.source)) {
            var aLen = args.source.length;
            for (var ai = 0; ai < aLen; ai++) {
                self.add(args.source[ai]);
            }
            if (args.value) {
                self.jdElement.val(args.value);
            }
        }
    }
    // събитие: create
    if (!jendo.isNull(args.create)) {
        args.create({
            type: "comboboxcreate",
            target: this.jdElement,
            timeStamp: new Date().getTime()
        });
    }
}

JendoCombobox.prototype.clear = function () {
    this.jdElement.html('');
};

JendoCombobox.prototype.add = function (item) {
    var iType = typeof item;
    if (iType === 'string') {
        this.jdElement.appendElement('option').text(item);
    }
    else {
        var idEl = this.jdElement.appendElement('option');
        idEl.text(item.label);
        idEl.val(item.value);
    }
};

JendoCombobox.prototype.disable = function () {
    this.jdElement.prop('disabled', true);
};

JendoCombobox.prototype.enable = function () {
    this.jdElement.prop('disabled', false);
};

JendoSelector.prototype.combobox = function (args) {
    if (jendo.isNull(args)) {
        args = {
            element: this
        };
    }
    else {
        args.element = this;
    }
    return new JendoCombobox(args);
};
/* END: Combobox
 */