﻿/* BEGIN: Navigation Bar
 */
// Jendo Navigation Bar
function JendoNavBar(elements) {
    this.sectionElements = elements;
}

// Добавя елемент за навигация
JendoNavBar.prototype.append = function (jsonData) {
    if (!jendo.isNull(this.sectionElements) && !jendo.isNull(jsonData)) {
        // елемент от менюто
        var menuItem = this.sectionElements.appendLI();
        // Ако има подменю
        if ('items' in jsonData) {
            menuItem.class('dropdown');
            // Видим елемент
            var userItem = null;
            // Има подаден линк
            if ('link' in jsonData) {
                userItem = menuItem.appendA(jsonData.link, jsonData.text);
            }
            // Няма подаден линк, създава бутон
            else {
                userItem = menuItem.appendButton()
                    .html(jsonData.text + '<i class="fas fa-angle-down menu-down"></i><i class="fas fa-angle-up menu-up"></i>');
            }
            var thisNB = this;
            userItem.attr('role', 'button')
                // събитие: отваря подменю
                .click(function (source) {
                    var itemClk = jendo(source.target);
                    var liElement = itemClk.closest('.dropdown');
                    var canOpened = true;
                    if (_JendoLastOpenElement !== null) {
                        if (_JendoLastOpenElement.is(liElement)) {
                            canOpened = false;
                            _JendoLastOpenElement = null;
                        }
                    }
                    // Отваря елементa
                    if (canOpened) {
                        // функция за затваряне на елемент
                        _JendoFuncCloseElement = thisNB.closeElement;
                        _JendoLastOpenElement = liElement;
                        // Отваря елемента
                        liElement.addClass('open');
                    }
                });
            // зарежда елементите на подменюто
            (new JendoNavBar(menuItem.appendUL().class('dropdown-menu')))
                .load(jsonData.items);
        }
        // Ако няма подменю
        else {
            // линк
            if (!jendo.isNullOrWhiteSpace(jsonData.link)) {
                return menuItem.appendA(jsonData.link, jsonData.text);
            }
            // бутон
            else {
                var self = this;
                return menuItem.appendButton(jsonData.text)
                    .click(function () {
                        // Dispatch the event.
                        document.dispatchEvent(new CustomEvent('navbar.navigate', {
                            detail: { src: self.sectionElements, args: jsonData.args }
                        }));
                    });
            }
        }
    }
};

// Затваря отворения елемент, ако източника е извън него
JendoNavBar.prototype.closeElement = function (source) {
    var parentSrc = _(source).closest('.dropdown');
    // елемента с който е отворен
    if (_JendoLastOpenElement.is(parentSrc)) {
        // Затваря елемента
        _JendoLastOpenElement.removeClass('open');
        // Премахва функцията за затваряне
        _JendoFuncCloseElement = null;
    }
    // ако не е в отворения елемент ли е?
    else if (!_JendoLastOpenElement.isContains(source.parentElement)) {
        // Затваря елемента
        _JendoLastOpenElement.removeClass('open');
        // Премахва функцията за затваряне
        _JendoFuncCloseElement = null;
        // Премахва елемента
        _JendoLastOpenElement = null;
    }
};

// Зарежда елементите за навигация
JendoNavBar.prototype.load = function (jsonData) {
    if (!jendo.isNull(this.sectionElements) && !jendo.isNull(jsonData)) {
        for (var i = 0; i < jsonData.length; i++) {
            this.append(jsonData[i]);
        }
    }
};

// Event: Навигация
JendoNavBar.prototype.navigate = function (func) {
    var self = this;
    document.addEventListener('navbar.navigate', function (e) {
        if (e.detail.src === self.sectionElements) {
            func(e, e.detail.args);
        }
    }, false);
    return this;
};

// Jendo Navigation Bar
jendo.navbar = function (element) {
    jendo._IsInitNavbar = true;
    if (jendo.isNull(element)) {
        return new JendoNavBar(null);
    }
    else {
        var jdElement = jendo(element);
        if (jdElement.length > 0) {
            return new JendoNavBar(jdElement.appendUL());
        }
        else {
            return new JendoNavBar(null);
        }
    }
};
/* END: NavBar
*/