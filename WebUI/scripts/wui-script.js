﻿/* Взема кода от codeId (xmp element) и го зарежда в codeId
 */
function runXMPExample(codeId, htmlId) {
    var result = _(htmlId);
    result.html('');

    var codeText = _(codeId).html();
    result.loadHTML(codeText);
}
/* Зарежда кода (codeText) в iframe
 */
function runIFrameExample(iframeId, codeText) {
    var elm = _(iframeId).first();
    if (elm != null) {
        if (elm.contentWindow != null && elm.contentWindow.document != null) {
            var doc = elm.contentWindow.document;
            doc.open();
            doc.write(codeText);
            doc.close();
        }
    }
}

/* {
    controller
    action
    code
    view
   }
 */
function openIFrameExample(args) {
    if (_.isNull(args)) {
        return;
    }
    else if (_.isNullOrWhiteSpace(args.controller)) {
        return;
    }
    else {
        if (_.isNullOrWhiteSpace(args.action)) {
            args.action = '#default';
        }
        var srcFile = 'demo/' + args.controller + '/' + args.action.replace('#', '') + '.html';
        _.get(srcFile)
            .done(function (responseText) {
                _(args.code).text(responseText);
                runIFrameExample(args.view, responseText);
            }).send();
    }
}