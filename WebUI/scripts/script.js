﻿// Разширява scope
var controls = controls || {};
(function(scope){

    var Calc = (function () {
        var lastId = 0; // pivete

        function Calculator() {
            this._id = ++lastId;
            console.log('id', this._id);
            this._result = 0;
        }

        Calculator.prototype = {
            add: function (value) {
                this._result += value;
                return this;
            }
        }
        return Calculator;
    })();

    var calculator;
    scope.Calculator = function () {
        if (!calculator) {
            calculator = new Calc();
        }
        return calculator;
    } 
    /*
    // exposed
    return {
        Calculator: function () {
            if (!calculator) {
                calculator = new Calc();
            }
            return calculator;
        } 
    } */
}(controls));

// mod.Calculator().add(111).add(111);


function Test1() {
}
Test1.prototype.add = function () { }