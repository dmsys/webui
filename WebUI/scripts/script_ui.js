﻿/*!
 * Jendo UI v1.1.5
 * https://approxys.com/
 *
 * Copyright Approxys Ltd
 * Released under the MIT License
 */
 /* форматира дата
    "d" 	ден от месеца, от 1 до 31.
    "dd" 	ден от месеца, от 01 до 31.
    "ddd" 	съкратеното име на деня от седмицата
    "dddd" 	пълно име на ден от седмицата
    "f" 	десети от секундата 
    "ff" 	стотни от секундата
    "fff" 	милисекунди
    "M" 	месец, от 1 до 12
    "MM" 	месец, от 01 до 12
    "MMM" 	съкратеното име на месеца
    "MMMM" 	пълното име на месеца
    "h" 	час, от 1 до 12
    "hh" 	час, от 01 до 12
    "H" 	час, от 1 до 23
    "HH" 	час, от 01 до 23
    "m" 	минути, от 0 до 59
    "mm" 	минути, от 00 до 59
    "s" 	секунди, от 0 до 59
    "ss" 	секунди, от 00 до 59
    "tt" 	Обозначението AM/PM
    "yy" 	последните две цифри от стойноста на годината
    "yyyy" 	цялата стойност на годината
    "zzz" 	часовата зона (UTC)
*/
jendo.dateFormatUI = function (date, pattern) {
    if (jendo.constructorName(date).toLowerCase() === "date") {
        var nMM = date.getMonth();
        var wk = date.getDay();

        return jendo.dateFormat(date, pattern
            .replace(/MMMM/g, 'p1')
            .replace(/MMM/g, 'p2')
            .replace(/dddd/g, 'p3')
            .replace(/ddd/g, 'p4')
        )
            .replace(/p1/g, jendo.lang.monthNames[nMM])
            .replace(/p2/g, jendo.lang.monthShortNames[nMM])
            .replace(/p3/g, jendo.lang.weekdayNames[wk])
            .replace(/p4/g, jendo.lang.weekdayShortNames[wk]);
    } else {
        return "";
    }
};

/* Зарежда 'font-awesome'
*/
// jendo.includeStyle('/content/font-awesome/css/all.min.css');

/* BEGIN: Jendo UI Language
*/
jendo.lang.load({
    // Дни от седмицата
    weekdayNames: [
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    weekdayShortNames: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    weekdayNarrowNames: ["S", "M", "T", "W", "T", "F", "S"],
    // Месеци
    monthNames: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ],
    monthShortNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ]
});
/* END: Jendo UI Language
*/

// последно отворен елемент
var _JendoLastOpenElement = null;
// функция за затваряне на последно отворения елемент
var _JendoFuncCloseElement = null;
// Събитието click преди извикване на целта
document.addEventListener('click', function (e) {
    // Ако има функция за затваряне и отворен елемент
    if ((_JendoFuncCloseElement != null) && (_JendoLastOpenElement != null)) {
        _JendoFuncCloseElement(e.target);
    }
}, true);
