/*
 Jendo v1.1.6
 https://approxys.com/

 Copyright Approxys
 Released under the MIT license
 https://tldrlegal.com/license/mit-license
*/
function jendo(value) {
  return new JendoSelector(document, value);
}
jendo.findId = function(elementId) {
  if (jendo.isNull(elementId)) {
    return null;
  } else {
    var elm = new JendoSelector(document, document.getElementById(elementId));
    elm.selector = "#" + elementId;
    return elm;
  }
};
jendo.findClass = function(className) {
  if (jendo.isNull(className)) {
    return null;
  } else {
    var elm = new JendoSelector(document, document.getElementsByClassName(className));
    elm.selector = "." + className;
    return elm;
  }
};
jendo.findTag = function(tagName) {
  if (jendo.isNull(tagName)) {
    return null;
  } else {
    var elm = new JendoSelector(document, document.getElementsByTagName(tagName));
    elm.selector = "." + className;
    return elm;
  }
};
jendo.get = function(url) {
  return new JendoHttpRequest("GET", url);
};
jendo.sendGet = function(url, data, success, dataType) {
  var jHRq = new JendoHttpRequest("GET", url);
  if (success) {
    jHRq.done(success);
  }
  if (dataType) {
    jHRq.contentType(dataType);
  }
  jHRq.send(data);
};
jendo.post = function(url) {
  return new JendoHttpRequest("POST", url);
};
jendo.sendPost = function(url, data, success, dataType) {
  var jHRq = new JendoHttpRequest("POST", url);
  if (success) {
    jHRq.done(success);
  }
  if (dataType) {
    jHRq.contentType(dataType);
  }
  jHRq.send(data);
};
jendo.put = function(url) {
  return new JendoHttpRequest("PUT", url);
};
jendo.sendPut = function(url, data, success, dataType) {
  var jHRq = new JendoHttpRequest("PUT", url);
  if (success) {
    jHRq.done(success);
  }
  if (dataType) {
    jHRq.contentType(dataType);
  }
  jHRq.send(data);
};
jendo.delete = function(url) {
  return new JendoHttpRequest("DELETE", url);
};
jendo.sendDelete = function(url, data, success, dataType) {
  var jHRq = new JendoHttpRequest("DELETE", url);
  if (success) {
    jHRq.done(success);
  }
  if (dataType) {
    jHRq.contentType(dataType);
  }
  jHRq.send(data);
};
jendo.getJSONP = function(url) {
  return new JendoJSONPRequest(url);
};
jendo.parseFloat = function(val, def) {
  if (typeof val === "number") {
    return val;
  } else {
    if (jendo.isNullOrEmpty(val)) {
      return parseFloat(jendo.isNull(def) ? 0 : def);
    } else {
      var fVal = parseFloat(val.replace(/,/g, "."));
      return isNaN(fVal) ? parseFloat(jendo.isNull(def) ? 0 : def) : fVal;
    }
  }
};
jendo.roundFloat = function(val, dec) {
  if (jendo.isNullOrEmpty(dec)) {
    dec = 0;
  }
  return parseFloat(jendo.parseFloat(val, 0).toFixed(dec));
};
jendo.createElement = function(tagName) {
  if (jendo.isNullOrWhiteSpace(tagName)) {
    return null;
  } else {
    var element = new JendoSelector(document, document.createElement(tagName));
    element.selector = tagName;
    return element;
  }
};
jendo.createDiv = function() {
  return jendo.createElement("div");
};
jendo.createP = function() {
  return jendo.createElement("p");
};
jendo.createSamp = function() {
  return jendo.createElement("samp");
};
jendo.createSpan = function() {
  return jendo.createElement("span");
};
jendo.createA = function(href, text) {
  var elm = jendo.createElement("a").attr("href", href);
  if (!jendo.isNullOrEmpty(text)) {
    elm.text(text);
  }
  return elm;
};
jendo.createImg = function(src) {
  return jendo.createElement("img").attr("src", src);
};
jendo.createButton = function(text) {
  var elm = jendo.createElement("button").attr("type", "button");
  if (!jendo.isNullOrEmpty(text)) {
    elm.text(text);
  }
  return elm;
};
jendo.guid = function(delimiter) {
  var guidPattern = typeof delimiter === "undefined" ? "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx" : "xxxxxxxx" + delimiter + "xxxx" + delimiter + "4xxx" + delimiter + "yxxx" + delimiter + "xxxxxxxxxxxx";
  var d = (new Date).getTime();
  var uuid = guidPattern.replace(/[xy]/g, function(c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == "x" ? r : r & 3 | 8).toString(16);
  });
  return uuid;
};
jendo.md5 = function(string) {
  function RotateLeft(lValue, iShiftBits) {
    return lValue << iShiftBits | lValue >>> 32 - iShiftBits;
  }
  function AddUnsigned(lX, lY) {
    var lX4, lY4, lX8, lY8, lResult;
    lX8 = lX & 2147483648;
    lY8 = lY & 2147483648;
    lX4 = lX & 1073741824;
    lY4 = lY & 1073741824;
    lResult = (lX & 1073741823) + (lY & 1073741823);
    if (lX4 & lY4) {
      return lResult ^ 2147483648 ^ lX8 ^ lY8;
    }
    if (lX4 | lY4) {
      if (lResult & 1073741824) {
        return lResult ^ 3221225472 ^ lX8 ^ lY8;
      } else {
        return lResult ^ 1073741824 ^ lX8 ^ lY8;
      }
    } else {
      return lResult ^ lX8 ^ lY8;
    }
  }
  function F(x, y, z) {
    return x & y | ~x & z;
  }
  function G(x, y, z) {
    return x & z | y & ~z;
  }
  function H(x, y, z) {
    return x ^ y ^ z;
  }
  function I(x, y, z) {
    return y ^ (x | ~z);
  }
  function FF(a, b, c, d, x, s, ac) {
    a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
    return AddUnsigned(RotateLeft(a, s), b);
  }
  function GG(a, b, c, d, x, s, ac) {
    a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
    return AddUnsigned(RotateLeft(a, s), b);
  }
  function HH(a, b, c, d, x, s, ac) {
    a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
    return AddUnsigned(RotateLeft(a, s), b);
  }
  function II(a, b, c, d, x, s, ac) {
    a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
    return AddUnsigned(RotateLeft(a, s), b);
  }
  function ConvertToWordArray(string) {
    var lWordCount;
    var lMessageLength = string.length;
    var lNumberOfWords_temp1 = lMessageLength + 8;
    var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - lNumberOfWords_temp1 % 64) / 64;
    var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
    var lWordArray = Array(lNumberOfWords - 1);
    var lBytePosition = 0;
    var lByteCount = 0;
    while (lByteCount < lMessageLength) {
      lWordCount = (lByteCount - lByteCount % 4) / 4;
      lBytePosition = lByteCount % 4 * 8;
      lWordArray[lWordCount] = lWordArray[lWordCount] | string.charCodeAt(lByteCount) << lBytePosition;
      lByteCount++;
    }
    lWordCount = (lByteCount - lByteCount % 4) / 4;
    lBytePosition = lByteCount % 4 * 8;
    lWordArray[lWordCount] = lWordArray[lWordCount] | 128 << lBytePosition;
    lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
    lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
    return lWordArray;
  }
  function WordToHex(lValue) {
    var WordToHexValue = "", WordToHexValue_temp = "", lByte, lCount;
    for (lCount = 0; lCount <= 3; lCount++) {
      lByte = lValue >>> lCount * 8 & 255;
      WordToHexValue_temp = "0" + lByte.toString(16);
      WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
    }
    return WordToHexValue;
  }
  function Utf8Encode(string) {
    string = string.replace(/\r\n/g, "\n");
    var utftext = "";
    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);
      if (c < 128) {
        utftext += String.fromCharCode(c);
      } else {
        if (c > 127 && c < 2048) {
          utftext += String.fromCharCode(c >> 6 | 192);
          utftext += String.fromCharCode(c & 63 | 128);
        } else {
          utftext += String.fromCharCode(c >> 12 | 224);
          utftext += String.fromCharCode(c >> 6 & 63 | 128);
          utftext += String.fromCharCode(c & 63 | 128);
        }
      }
    }
    return utftext;
  }
  var x = Array();
  var k, AA, BB, CC, DD, a, b, c, d;
  var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
  var S21 = 5, S22 = 9, S23 = 14, S24 = 20;
  var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
  var S41 = 6, S42 = 10, S43 = 15, S44 = 21;
  string = Utf8Encode(string);
  x = ConvertToWordArray(string);
  a = 1732584193;
  b = 4023233417;
  c = 2562383102;
  d = 271733878;
  for (k = 0; k < x.length; k += 16) {
    AA = a;
    BB = b;
    CC = c;
    DD = d;
    a = FF(a, b, c, d, x[k + 0], S11, 3614090360);
    d = FF(d, a, b, c, x[k + 1], S12, 3905402710);
    c = FF(c, d, a, b, x[k + 2], S13, 606105819);
    b = FF(b, c, d, a, x[k + 3], S14, 3250441966);
    a = FF(a, b, c, d, x[k + 4], S11, 4118548399);
    d = FF(d, a, b, c, x[k + 5], S12, 1200080426);
    c = FF(c, d, a, b, x[k + 6], S13, 2821735955);
    b = FF(b, c, d, a, x[k + 7], S14, 4249261313);
    a = FF(a, b, c, d, x[k + 8], S11, 1770035416);
    d = FF(d, a, b, c, x[k + 9], S12, 2336552879);
    c = FF(c, d, a, b, x[k + 10], S13, 4294925233);
    b = FF(b, c, d, a, x[k + 11], S14, 2304563134);
    a = FF(a, b, c, d, x[k + 12], S11, 1804603682);
    d = FF(d, a, b, c, x[k + 13], S12, 4254626195);
    c = FF(c, d, a, b, x[k + 14], S13, 2792965006);
    b = FF(b, c, d, a, x[k + 15], S14, 1236535329);
    a = GG(a, b, c, d, x[k + 1], S21, 4129170786);
    d = GG(d, a, b, c, x[k + 6], S22, 3225465664);
    c = GG(c, d, a, b, x[k + 11], S23, 643717713);
    b = GG(b, c, d, a, x[k + 0], S24, 3921069994);
    a = GG(a, b, c, d, x[k + 5], S21, 3593408605);
    d = GG(d, a, b, c, x[k + 10], S22, 38016083);
    c = GG(c, d, a, b, x[k + 15], S23, 3634488961);
    b = GG(b, c, d, a, x[k + 4], S24, 3889429448);
    a = GG(a, b, c, d, x[k + 9], S21, 568446438);
    d = GG(d, a, b, c, x[k + 14], S22, 3275163606);
    c = GG(c, d, a, b, x[k + 3], S23, 4107603335);
    b = GG(b, c, d, a, x[k + 8], S24, 1163531501);
    a = GG(a, b, c, d, x[k + 13], S21, 2850285829);
    d = GG(d, a, b, c, x[k + 2], S22, 4243563512);
    c = GG(c, d, a, b, x[k + 7], S23, 1735328473);
    b = GG(b, c, d, a, x[k + 12], S24, 2368359562);
    a = HH(a, b, c, d, x[k + 5], S31, 4294588738);
    d = HH(d, a, b, c, x[k + 8], S32, 2272392833);
    c = HH(c, d, a, b, x[k + 11], S33, 1839030562);
    b = HH(b, c, d, a, x[k + 14], S34, 4259657740);
    a = HH(a, b, c, d, x[k + 1], S31, 2763975236);
    d = HH(d, a, b, c, x[k + 4], S32, 1272893353);
    c = HH(c, d, a, b, x[k + 7], S33, 4139469664);
    b = HH(b, c, d, a, x[k + 10], S34, 3200236656);
    a = HH(a, b, c, d, x[k + 13], S31, 681279174);
    d = HH(d, a, b, c, x[k + 0], S32, 3936430074);
    c = HH(c, d, a, b, x[k + 3], S33, 3572445317);
    b = HH(b, c, d, a, x[k + 6], S34, 76029189);
    a = HH(a, b, c, d, x[k + 9], S31, 3654602809);
    d = HH(d, a, b, c, x[k + 12], S32, 3873151461);
    c = HH(c, d, a, b, x[k + 15], S33, 530742520);
    b = HH(b, c, d, a, x[k + 2], S34, 3299628645);
    a = II(a, b, c, d, x[k + 0], S41, 4096336452);
    d = II(d, a, b, c, x[k + 7], S42, 1126891415);
    c = II(c, d, a, b, x[k + 14], S43, 2878612391);
    b = II(b, c, d, a, x[k + 5], S44, 4237533241);
    a = II(a, b, c, d, x[k + 12], S41, 1700485571);
    d = II(d, a, b, c, x[k + 3], S42, 2399980690);
    c = II(c, d, a, b, x[k + 10], S43, 4293915773);
    b = II(b, c, d, a, x[k + 1], S44, 2240044497);
    a = II(a, b, c, d, x[k + 8], S41, 1873313359);
    d = II(d, a, b, c, x[k + 15], S42, 4264355552);
    c = II(c, d, a, b, x[k + 6], S43, 2734768916);
    b = II(b, c, d, a, x[k + 13], S44, 1309151649);
    a = II(a, b, c, d, x[k + 4], S41, 4149444226);
    d = II(d, a, b, c, x[k + 11], S42, 3174756917);
    c = II(c, d, a, b, x[k + 2], S43, 718787259);
    b = II(b, c, d, a, x[k + 9], S44, 3951481745);
    a = AddUnsigned(a, AA);
    b = AddUnsigned(b, BB);
    c = AddUnsigned(c, CC);
    d = AddUnsigned(d, DD);
  }
  return WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);
};
Date.prototype.addYears = function(years) {
  var dat = new Date(this.valueOf());
  dat.setFullYear(dat.getFullYear() + years);
  return dat;
};
Date.prototype.addMonths = function(months) {
  var dat = new Date(this.valueOf());
  dat.setMonth(dat.getMonth() + months);
  return dat;
};
Date.prototype.addDays = function(days) {
  var dat = new Date(this.valueOf());
  dat.setDate(dat.getDate() + days);
  return dat;
};
Date.prototype.addHours = function(hours) {
  var dat = new Date(this.valueOf());
  dat.setHours(dat.getHours() + hours);
  return dat;
};
Date.prototype.addMinutes = function(minutes) {
  var dat = new Date(this.valueOf());
  dat.setMinutes(dat.getMinutes() + minutes);
  return dat;
};
Date.prototype.addSeconds = function(seconds) {
  var dat = new Date(this.valueOf());
  dat.setSeconds(dat.getSeconds() + seconds);
  return dat;
};
Date.prototype.addMilliseconds = function(ms) {
  var dat = new Date(this.valueOf());
  dat.setMilliseconds(dat.getMilliseconds() + ms);
  return dat;
};
jendo.now = function() {
  return new Date;
};
jendo.daysInMonth = function(month, year) {
  return (new Date(year, month, 0)).getDate();
};
jendo.formatDate = jendo.dateFormat = function(date, pattern) {
  if (jendo.constructorName(date).toLowerCase() == "date") {
    var yyyy = date.getFullYear().toString();
    var yy = yyyy.length > 2 ? yyyy.substr(yyyy.length - 2, 2) : yyyy;
    var MM = (date.getMonth() + 1).toString();
    var dd = date.getDate().toString();
    var nHH = date.getHours();
    var HH = nHH.toString();
    var hh = (nHH > 12 ? nHH - 12 : nHH).toString();
    var tt = nHH >= 12 ? "PM" : "AM";
    var mm = date.getMinutes().toString();
    var ss = date.getSeconds().toString();
    var fff = (1000 + date.getMilliseconds()).toString();
    var tz = date.getTimezoneOffset() / 0.6;
    var zzz = (tz > 0 ? "GMT-" : "GMT+") + (10000 + Math.abs(tz)).toString().substr(1);
    return pattern.replace(/yyyy/g, yyyy).replace(/yy/g, yy).replace(/MM/g, MM[1] ? MM : "0" + MM[0]).replace(/M/g, MM).replace(/dd/g, dd[1] ? dd : "0" + dd[0]).replace(/d/g, dd).replace(/HH/g, HH[1] ? HH : "0" + HH[0]).replace(/H/g, HH).replace(/hh/g, hh[1] ? hh : "0" + hh[0]).replace(/h/g, hh).replace(/mm/g, mm[1] ? mm : "0" + mm[0]).replace(/m/g, mm).replace(/ss/g, ss[1] ? ss : "0" + ss[0]).replace(/s/g, ss).replace(/fff/g, fff.substr(1, 3)).replace(/ff/g, fff.substr(1, 2)).replace(/f/g, fff.substr(1, 
    1)).replace(/zzz/g, zzz).replace(/tt/g, tt);
  } else {
    return "";
  }
};
jendo.formatXml = function(xml) {
  var formatted = "";
  var reg = /(>)(<)(\/*)/g;
  xml = xml.replace(reg, "$1\r\n$2$3");
  var pad = 0;
  var nodes = xml.split("\r\n");
  for (var n = 0; n < nodes.length; n++) {
    var node = nodes[n];
    var indent = 0;
    if (node.match(/.+<\/\w[^>]*>$/)) {
      indent = 0;
    } else {
      if (node.match(/^<\/\w/)) {
        if (pad !== 0) {
          pad -= 1;
        }
      } else {
        if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
          indent = 1;
        } else {
          indent = 0;
        }
      }
    }
    var padding = "";
    for (var i = 0; i < pad; i++) {
      padding += "  ";
    }
    formatted += padding + node + "\r\n";
    pad += indent;
  }
  return formatted;
};
jendo.ready = function(functionValue) {
  if (typeof this.readyFunctions === "undefined") {
    this.readyFunctions = [];
  }
  this.readyFunctions.push(functionValue);
};
jendo.includeStyle = function(url, callback) {
  var styleHref = jendo.validateUrl(url);
  var link = document.querySelector('link[href="' + styleHref + '"]');
  if (link == null) {
    link = document.createElement("link");
    link.rel = "stylesheet";
    link.href = styleHref;
    if (!jendo.isNullOrEmpty(callback)) {
      link.onload = callback;
    }
    document.head.appendChild(link);
  }
};
jendo.includeScript = function(url, callback) {
  var script = document.querySelector('script[src="' + url + '"]');
  if (script == null) {
    script = document.createElement("script");
    script.src = url;
    if (typeof callback != "undefined") {
      script.onload = callback;
    }
    document.head.appendChild(script);
  }
};
jendo.loadDOM = function(data, element, keepScripts, callback) {
  if (typeof element == "undefined") {
    element = document.createElement("div");
  }
  if (typeof keepScripts == "undefined") {
    keepScripts = true;
  }
  var loadWaiting = 0;
  function loadReady() {
    if (loadWaiting == 0 && !jendo.isNull(callback)) {
      callback();
    }
  }
  function loadNodes(docNodes, element, keepScripts) {
    for (var i = 0; i < docNodes.length; i++) {
      var docNode = docNodes[i];
      if (docNode.nodeName == "SCRIPT") {
        if (keepScripts) {
          var oScript = document.createElement(docNode.localName);
          for (var attrIndex = 0; attrIndex < docNode.attributes.length; attrIndex++) {
            oScript.setAttribute(docNode.attributes[attrIndex].name, docNode.attributes[attrIndex].value);
          }
          oScript.textContent = docNode.textContent;
          if (!jendo.isNull(docNode.attributes.src)) {
            loadWaiting += 1;
            oScript.addEventListener("load", function(handler) {
              loadWaiting -= 1;
              loadReady();
            });
          }
          element.appendChild(oScript);
        }
      } else {
        if (docNode.nodeName == "#text") {
          if (!jendo.isNullOrWhiteSpace(docNode.nodeValue) && docNode.nodeValue != "\n") {
            var oText = document.createTextNode(docNode.nodeValue);
            element.appendChild(oText);
          }
        } else {
          if (docNode.nodeName == "#comment") {
          } else {
            var oElement = document.createElement(docNode.localName);
            for (var attrIndex = 0; attrIndex < docNode.attributes.length; attrIndex++) {
              oElement.setAttribute(docNode.attributes[attrIndex].name, docNode.attributes[attrIndex].value);
            }
            element.appendChild(oElement);
            loadNodes(docNode.childNodes, oElement);
          }
        }
      }
    }
  }
  loadNodes(data, element, keepScripts);
  loadReady();
  return element;
};
jendo.parseHTML = function(data) {
  if (typeof data === "string") {
    var dataHtml = data.trim();
    if (!jendo.startsWith(dataHtml, "<!DOCTYPE") || !jendo.startsWith(dataHtml, "<html")) {
      dataHtml = "<!DOCTYPE html><html><body>" + dataHtml + "</body></html>";
    }
    var parser = new DOMParser;
    return parser.parseFromString(dataHtml, "text/html");
  } else {
    return null;
  }
};
jendo.parseXML = function(data) {
  if (typeof data === "string") {
    return (new DOMParser).parseFromString(data, "text/xml");
  } else {
    return null;
  }
};
jendo.toJson = function(value) {
  if (jendo.isNullOrEmpty(value)) {
    return "{}";
  } else {
    switch(typeof value) {
      case "string":
        return '"' + value.replace(/"/g, '\\"').replace(/\n/g, "\\n") + '"';
      case "function":
        var funValue = (value.name || value.toString()).replace(/"/g, '\\"').replace(/\n/g, "\\n");
        return '"' + funValue + '"';
      case "object":
        var isArray = Array.isArray(value);
        return "{["[+isArray] + Object.keys(value).map(function(key) {
          return '"' + key + '":' + jendo.toJson(value[key]);
        }).join(",") + "}]"[+isArray];
      default:
        return value.toString();
    }
  }
};
jendo.startsWith = function(valueString, searchString, position) {
  if (typeof valueString === "string" && typeof searchString === "string") {
    position = position || 0;
    return valueString.substr(position, searchString.length).toLowerCase() === searchString.toLowerCase();
  } else {
    return false;
  }
};
jendo.trimRight = function(valueString, trimString) {
  for (var i = valueString.length - 1; i >= 0; i--) {
    if (trimString != valueString.charAt(i)) {
      return valueString.substring(0, i + 1);
    }
  }
  return "";
};
jendo.isNull = function(value) {
  if (typeof value == "undefined") {
    return true;
  } else {
    if (value == null) {
      return true;
    } else {
      return false;
    }
  }
};
jendo.isNullOrEmpty = function(value) {
  if (jendo.isNull(value)) {
    return true;
  } else {
    if (typeof value === "string") {
      return value === "";
    } else {
      if (Array.isArray(value)) {
        return value.length === 0;
      } else {
        return false;
      }
    }
  }
};
jendo.isNullOrWhiteSpace = function(value) {
  if (jendo.isNull(value)) {
    return true;
  } else {
    if (typeof value === "string") {
      return value.replace(/ /g, "").replace(/\r/g, "").replace(/\n/g, "") === "";
    } else {
      if (Array.isArray(value)) {
        return value.length === 0;
      } else {
        return false;
      }
    }
  }
};
jendo.isVisible = function(element) {
  if (jendo.isNull(element)) {
    return false;
  } else {
    return !((element.style == null ? true : element.style.display === "none" || element.style.visibility === "hidden") || element.offsetParent === null);
  }
};
jendo.inArray = function(value, array, fromIndex) {
  for (var i = 0; i < array.length; i++) {
    if (value == array[i]) {
      return i;
    }
  }
  return -1;
};
jendo.constructorName = function(element) {
  if (element == null) {
    return "";
  } else {
    if ("name" in element.constructor) {
      return element.constructor.name;
    } else {
      var con = element.constructor.toString().trim();
      var indexFS = con.indexOf("function");
      if (indexFS == 0) {
        var indexFE = con.indexOf("(");
        if (indexFE == -1) {
          return con.substring(indexFS + 8, con.length);
        } else {
          return con.substring(indexFS + 8, indexFE).trim();
        }
      } else {
        var conMatch = con.match(/^\[object\s(.+)\]/);
        if (conMatch != null) {
          return conMatch[1];
        } else {
          conMatch = con.match(/^function\s(.+)\(/);
          if (conMatch != null) {
            return conMatch[1];
          } else {
            return con;
          }
        }
      }
    }
  }
};
jendo.screenWidth = function() {
  return screen.width;
};
jendo.screenHeight = function() {
  return screen.height;
};
jendo.windowWidth = function() {
  return window.innerWidth;
};
jendo.windowHeight = function() {
  return window.innerHeight;
};
jendo.isFileProtocol = function() {
  return window.location.protocol == "file:";
};
jendo.combineUrl = function(url1, url2) {
  if (jendo.startsWith(url2, "/")) {
    return jendo.trimRight(url1, "/") + url2;
  } else {
    return jendo.trimRight(url1, "/") + "/" + url2;
  }
};
jendo.validateUrl = function(url) {
  if (jendo.isNullOrWhiteSpace(url)) {
    return "";
  } else {
    if (jendo.isFileProtocol()) {
      if (jendo.startsWith(url, "file:///")) {
        return url;
      } else {
        if (url[0] == "/") {
          return "." + url;
        } else {
          return url;
        }
      }
    } else {
      return url;
    }
  }
};
function JendoSelector(container, item) {
  this.selector = "";
  this.container = jendo.isNull(container) ? document : container;
  this.items = [];
  var typeItem = typeof item;
  if (typeItem === "object") {
    if (item != null) {
      this.selector = jendo.constructorName(item);
      if (this.selector === "HTMLCollection" || this.selector === "NodeList" || this.selector === "Array") {
        for (var i = 0; i < item.length; i++) {
          this.items.push(item[i]);
        }
      } else {
        if (this.selector === "JendoSelector") {
          for (var ji = 0; ji < item.items.length; ji++) {
            this.items.push(item.items[ji]);
          }
        } else {
          this.items.push(item);
        }
      }
    }
  } else {
    if (typeItem === "string" && item.length > 0) {
      this.selector = item;
      var elements = this.container.querySelectorAll(this.selector);
      for (var si = 0; si < elements.length; si++) {
        this.items.push(elements[si]);
      }
    }
  }
  this.length = this.items.length;
}
JendoSelector.prototype.find = function(item) {
  if (this.items.length > 0) {
    var jTag = new JendoSelector;
    jTag.selector = item;
    for (var i = 0; i < this.items.length; i++) {
      var findElements = this.items[i].querySelectorAll(item);
      for (var f = 0; f < findElements.length; f++) {
        jTag.items.push(findElements[f]);
      }
    }
    jTag.length = jTag.items.length;
    return jTag;
  } else {
    return new JendoSelector(this.container, item);
  }
};
JendoSelector.prototype.tag = function(name) {
  if (this.items.length > 0) {
    var jTag = new JendoSelector;
    jTag.selector = name;
    for (var i = 0; i < this.items.length; i++) {
      var findElements = this.items[i].getElementsByTagName(name);
      for (var f = 0; f < findElements.length; f++) {
        jTag.items.push(findElements[f]);
      }
    }
    jTag.length = jTag.items.length;
    return jTag;
  } else {
    return new JendoSelector;
  }
};
JendoSelector.prototype.eq = JendoSelector.prototype.item = function(index) {
  var itemIndex = 0;
  if (!jendo.isNull(index)) {
    itemIndex = index;
  }
  if (this.items.length > itemIndex) {
    return new JendoSelector(this.container, this.items[itemIndex]);
  } else {
    return null;
  }
};
JendoSelector.prototype.isVisible = function() {
  return jendo.isVisible(this.first());
};
JendoSelector.prototype.html = function(value) {
  var valueType = typeof value;
  if (valueType === "undefined") {
    if (this.items.length > 0) {
      var git = this.items[0];
      return git.innerHTML;
    } else {
      return "";
    }
  } else {
    if (valueType === "function") {
      for (var f = 0; f < this.items.length; f++) {
        var fit = this.items[f];
        fit.innerHTML = value(f, fit.innerHTML);
      }
      return this;
    } else {
      for (var s = 0; s < this.items.length; s++) {
        this.items[s].innerHTML = value;
      }
      return this;
    }
  }
};
JendoSelector.prototype.toHtml = function() {
  return new JendoSelector(jendo.parseHTML(this.text()), "body");
};
JendoSelector.prototype.val = function(value) {
  var valueType = typeof value;
  if (valueType === "undefined") {
    if (this.items.length > 0) {
      var git = this.items[0];
      var gitType = git.type;
      if (gitType === "checkbox") {
        return git.checked ? 1 : 0;
      } else {
        if (git.multiple && "options" in git) {
          var giOpts = [];
          for (var gi = 0; gi < git.options.length; gi++) {
            var gito = git.options[gi];
            if (gito.selected) {
              giOpts.push(gito.value);
            }
          }
          return giOpts;
        } else {
          return git.value;
        }
      }
    } else {
      return "";
    }
  } else {
    if (valueType === "object") {
      if (Array.isArray(value)) {
        var vls = [];
        for (var v = 0; v < value.length; v++) {
          vls.push(value[v].toString().toLowerCase());
        }
        for (var a = 0; a < this.items.length; a++) {
          var ait = this.items[a];
          var aitType = ait.type;
          if (aitType === "select-multiple") {
            for (var ai = 0; ai < ait.options.length; ai++) {
              var aito = ait.options[ai];
              var aix = vls.indexOf(aito.value.toLowerCase());
              if (aito.selected && aix === -1) {
                aito.selected = false;
              } else {
                if (!aito.selected && aix !== -1) {
                  aito.selected = true;
                }
              }
            }
          } else {
            if (aitType === "checkbox") {
              ait.checked = value.indexOf(ait.value.toLowerCase()) > -1;
            } else {
              if (aitType === "radio") {
                ait.checked = value.indexOf(ait.value.toLowerCase()) > -1;
              } else {
                ait.value = vls[vls.length - 1];
              }
            }
          }
        }
      }
      return this;
    } else {
      if (valueType === "function") {
        for (var f = 0; f < this.items.length; f++) {
          var fit = this.items[f];
          if (fit.multiple && "options" in fit) {
            var fiOpts = [];
            for (var go = 0; go < fit.options.length; go++) {
              var figo = fit.options[go];
              if (figo.selected) {
                fiOpts.push(figo.value);
              }
            }
            var fiNOpts = value(f, fiOpts);
            for (var so = 0; so < fit.options.length; so++) {
              var fiso = fit.options[so];
              var fix = fiNOpts.indexOf(fiso.value);
              if (fiso.selected && fix === -1) {
                fiso.selected = false;
              } else {
                if (!fiso.selected && fix !== -1) {
                  fiso.selected = true;
                }
              }
            }
          } else {
            fit.value = value(f, fit.value);
          }
        }
      } else {
        for (var s = 0; s < this.items.length; s++) {
          var ait = this.items[s];
          var aitType = ait.type;
          if (aitType === "checkbox") {
            if (valueType === "number") {
              ait.checked = value === 1;
            } else {
              if (valueType === "boolean") {
                ait.checked = value;
              } else {
                if (valueType === "string") {
                  ait.checked = value === "1" || value.toLowerCase() === "true";
                } else {
                  ait.value = value;
                }
              }
            }
          } else {
            ait.value = value;
          }
        }
        return this;
      }
    }
  }
};
JendoSelector.prototype.text = function(value) {
  var valueType = typeof value;
  if (typeof value === "undefined") {
    if (this.items.length > 0) {
      var git = this.items[0];
      if ("options" in git) {
        var giOpts = [];
        for (var gi = 0; gi < git.options.length; gi++) {
          var gito = git.options[gi];
          if (gito.selected) {
            giOpts.push(gito.innerText);
          }
        }
        return giOpts;
      } else {
        if ("value" in git) {
          return git.value;
        } else {
          return git.textContent;
        }
      }
    } else {
      return "";
    }
  } else {
    if (valueType === "function") {
      for (var f = 0; f < this.items.length; f++) {
        var fit = this.items[f];
        fit.textContent = value(f, fit.textContent);
      }
      return this;
    } else {
      for (var s = 0; s < this.items.length; s++) {
        this.items[s].textContent = value;
      }
      return this;
    }
  }
};
JendoSelector.prototype.data = function(key, value) {
  if (jendo.isNull(key)) {
    return this;
  } else {
    return this.attr("data-" + key, value);
  }
};
JendoSelector.prototype.removeData = function(key) {
  if (jendo.isNull(key)) {
    return this;
  } else {
    return this.removeAttr("data-" + key);
  }
};
JendoSelector.prototype.attr = function(key, value) {
  if (jendo.isNull(key)) {
    return this;
  } else {
    if (jendo.isNull(value)) {
      var tKey = typeof key;
      if (tKey == "object") {
        this.forEach(function(i, item) {
          for (var propName in key) {
            item.setAttribute(propName, key[propName]);
          }
        });
        return this;
      } else {
        var item = this.first();
        if (item == null) {
          return null;
        } else {
          var attrItem = item.attributes[key];
          return attrItem == null ? null : attrItem.value;
        }
      }
    } else {
      var tValue = typeof value;
      if (tValue == "function") {
        this.forEach(function(i, item) {
          var iValue = item.attributes[key];
          var fValue = jendo.isNullOrEmpty(iValue) ? eval("value(" + i + ", '')") : eval("value(" + i + ", '" + iValue.value.replace(/'/g, "\\'") + "')");
          item.setAttribute(key, fValue);
        });
      } else {
        this.forEach(function(i, item) {
          item.setAttribute(key, value);
        });
      }
      return this;
    }
  }
};
JendoSelector.prototype.removeAttr = function(key) {
  if (!jendo.isNull(key)) {
    this.forEach(function(i, item) {
      item.removeAttribute(key);
    });
  }
  return this;
};
JendoSelector.prototype.attrId = function(value) {
  return this.attr("id", value);
};
JendoSelector.prototype.role = JendoSelector.prototype.attrRole = function(value) {
  return this.attr("role", value);
};
JendoSelector.prototype.class = function(value) {
  if (jendo.isNull(value)) {
    return this.attr("class");
  } else {
    this.attr("class", value);
    return this;
  }
};
JendoSelector.prototype.addClass = function(value) {
  if (!jendo.isNull(value)) {
    if (typeof value === "function") {
      this.forEach(function(i, item) {
        jendo(item).addClass(value(i, item.className));
      });
    } else {
      var vList = value.split(" ");
      var vLng = vList.length;
      this.forEach(function(i, item) {
        for (var c = 0; c < vLng; c++) {
          item.classList.add(vList[c]);
        }
      });
    }
  }
  return this;
};
JendoSelector.prototype.removeClass = function(value) {
  if (!jendo.isNull(value)) {
    var vList = value.split(" ");
    var vLng = vList.length;
    this.forEach(function(i, item) {
      for (var c = 0; c < vLng; c++) {
        item.classList.remove(vList[c]);
      }
    });
  } else {
    this.forEach(function(i, item) {
      item.className = "";
    });
  }
  return this;
};
JendoSelector.prototype.hasClass = function(value) {
  var iLng = this.items.length;
  if (jendo.isNull(value) || iLng === 0) {
    return false;
  } else {
    var has = false;
    for (var i = 0; i < iLng && !has; i++) {
      has = this.items[i].classList.contains(value);
    }
    return has;
  }
};
JendoSelector.prototype.toggle = function(value) {
  if (!jendo.isNull(value)) {
    this.forEach(function(i, item) {
      item.classList.toggle(value);
    });
  }
  return this;
};
JendoSelector.prototype.prop = function(key, value) {
  if (jendo.isNull(key)) {
    return this;
  } else {
    if (jendo.isNull(value)) {
      var tKey = typeof key;
      if (tKey == "object") {
        this.forEach(function(i, item) {
          for (var propName in key) {
            item[propName] = key[propName];
          }
        });
        return this;
      } else {
        var item = this.first();
        return item == null ? null : item[key];
      }
    } else {
      var tValue = typeof value;
      if (tValue == "function") {
        this.forEach(function(i, item) {
          var iValue = item[key];
          var fValue = jendo.isNullOrEmpty(iValue) ? eval("value(" + i + ", '')") : eval("value(" + i + ", '" + iValue.replace(/'/g, "\\'") + "')");
          item[key] = fValue;
        });
      } else {
        this.forEach(function(i, item) {
          item[key] = value;
        });
      }
      return this;
    }
  }
};
JendoSelector.prototype.removeProp = function(key) {
  if (jendo.isNull(key)) {
    return this;
  } else {
    this.forEach(function(i, item) {
      delete item[key];
    });
    return this;
  }
};
JendoSelector.prototype.parent = function(selector) {
  var jParent = new JendoSelector;
  if (jendo.isNullOrWhiteSpace(selector)) {
    jParent.selector = "";
  } else {
    jParent.selector = selector;
  }
  for (var i = 0; i < this.items.length; i++) {
    var eParent = this.items[i].parentElement;
    if (jParent.selector === "") {
      jParent.items.push(eParent);
    } else {
      if (eParent.matches(selector)) {
        jParent.items.push(eParent);
      }
    }
  }
  return jParent;
};
JendoSelector.prototype.closest = function(selector) {
  var j = new JendoSelector;
  for (var i = 0; i < this.items.length; i++) {
    var items = this.items[i].closest(selector);
    if (items) {
      j.items.push(items);
    }
  }
  return j;
};
JendoSelector.prototype.is = function(value) {
  if (jendo.isNull(value)) {
    return false;
  } else {
    var vlType = typeof value;
    if (vlType === "function") {
      var isResultF = false;
      for (var f = 0; f < this.items.length && !isResultF; f++) {
        var thisItemF = this.items[f];
        var fnResult = value(f, thisItemF);
        if (typeof fnResult === "boolean") {
          isResultF = fnResult;
        }
      }
      return isResultF;
    } else {
      var isResult = false;
      var elements = jendo(value).items;
      for (var t = 0; t < this.items.length && !isResult; t++) {
        var thisItem = this.items[t];
        for (var e = 0; e < elements.length && !isResult; e++) {
          isResult = thisItem.isSameNode(elements[e]);
        }
      }
      return isResult;
    }
  }
};
JendoSelector.prototype.isContains = function(value) {
  if (jendo.isNull(value)) {
    return false;
  } else {
    var vlType = typeof value;
    if (vlType === "function") {
      var isResult = false;
      for (var t = 0; t < this.items.length && !isResult; t++) {
        var thisItem = this.items[t];
        var fnResult = eval("value(t, thisItem)");
        if (typeof fnResult === "boolean") {
          isResult = fnResult;
        }
      }
      return isResult;
    } else {
      var isResult = false;
      var elements = jendo(value).items;
      for (var t = 0; t < this.items.length && !isResult; t++) {
        var thisItem = this.items[t];
        for (var e = 0; e < elements.length && !isResult; e++) {
          isResult = thisItem.contains(elements[e]);
        }
      }
      return isResult;
    }
  }
};
JendoSelector.prototype.isEqual = function(value) {
  if (jendo.isNull(value)) {
    return false;
  } else {
    var vlType = typeof value;
    if (vlType == "function") {
      var isResult = false;
      for (var t = 0; t < this.items.length && !isResult; t++) {
        var thisItem = this.items[t];
        var fnResult = eval("value(t, thisItem)");
        if (typeof fnResult === "boolean") {
          isResult = fnResult;
        }
      }
      return isResult;
    } else {
      var isResult = false;
      var elements = jendo(value).items;
      for (var t = 0; t < this.items.length && !isResult; t++) {
        var thisItem = this.items[t];
        for (var e = 0; e < elements.length && !isResult; e++) {
          isResult = thisItem.isEqualNode(elements[e]);
        }
      }
      return isResult;
    }
  }
};
JendoSelector.prototype.append = function(value) {
  var childNodes = [];
  var valueType = typeof value;
  if (valueType === "string") {
    for (var i = 0; i < this.items.length; i++) {
      var tItem = this.items[i];
      var cnIndex = tItem.childNodes.length;
      tItem.insertAdjacentHTML("beforeend", value);
      for (; cnIndex < tItem.childNodes.length; cnIndex++) {
        childNodes.push(tItem.childNodes[cnIndex]);
      }
    }
  } else {
    if (valueType === "object") {
      var valueCnstr = jendo.constructorName(value);
      if (valueCnstr === "HTMLCollection" || valueCnstr === "NodeList") {
        for (var vci = 0; vci < value.length; vci++) {
          this._appendItemChild(this.items, value[vci], childNodes);
        }
      } else {
        if (valueCnstr === "JendoSelector") {
          var lastChildNodes = [];
          for (var vsi = value.items.length - 1; vsi >= 0; vsi--) {
            var vItem = value.items[vsi];
            var vItemCnstr = jendo.constructorName(vItem);
            if (vItemCnstr === "HTMLCollection" || vItemCnstr === "NodeList") {
              for (var iItem = vItem.length - 1; iItem >= 0; iItem--) {
                this._insertItemChild(this.items, vItem[iItem], lastChildNodes, childNodes);
              }
            } else {
              this._insertItemChild(this.items, value.items[vsi], lastChildNodes, childNodes);
            }
          }
        } else {
          if (valueCnstr === "HTMLDocument") {
            var nodes = value.body.childNodes;
            for (var vdi = 0; vdi < nodes.length; vdi++) {
              this._appendItemChild(this.items, nodes[vdi], childNodes);
            }
          } else {
            this._appendItemChild(this.items, value, childNodes);
          }
        }
      }
    }
  }
  return jendo(childNodes);
};
JendoSelector.prototype._insertItemChild = function(nItems, nValue, nLastChildNodes, childNodes) {
  if (!jendo.isNull(nItems) && !jendo.isNull(nValue)) {
    for (var i = 0; i < nItems.length; i++) {
      var lastChildNode = nLastChildNodes[i];
      if (i === 0) {
        childNodes.push(nValue);
        nItems[i].insertBefore(nValue, lastChildNode);
        nLastChildNodes[i] = nValue;
      } else {
        var nValueClone = nValue.cloneNode(true);
        childNodes.push(nValueClone);
        nItems[i].insertBefore(nValueClone, lastChildNode);
        nLastChildNodes[i] = nValueClone;
      }
    }
  }
};
JendoSelector.prototype._appendItemChild = function(nItems, nValue, childNodes) {
  if (!jendo.isNull(nItems) && !jendo.isNull(nValue)) {
    for (var i = 0; i < nItems.length; i++) {
      if (i === 0) {
        childNodes.push(nValue);
        nItems[i].appendChild(nValue);
      } else {
        var nValueClone = nValue.cloneNode(true);
        childNodes.push(nValueClone);
        nItems[i].appendChild(nValueClone);
      }
    }
  }
};
JendoSelector.prototype._insertFirstChild = function(nItems, nValue, childNodes) {
  if (!jendo.isNull(nItems) && !jendo.isNull(nValue)) {
    for (var i = 0; i < nItems.length; i++) {
      if (i === 0) {
        childNodes.push(nValue);
        nItems[i].insertBefore(nValue, nItems[i].childNodes[0]);
      } else {
        var nValueClone = nValue.cloneNode(true);
        childNodes.push(nValueClone);
        nItems[i].insertBefore(nValueClone, nItems[i].childNodes[0]);
      }
    }
  }
};
JendoSelector.prototype.appendElement = function(tagName) {
  if (jendo.isNullOrWhiteSpace(tagName)) {
    return jendo([]);
  } else {
    return this.append(document.createElement(tagName));
  }
};
JendoSelector.prototype.appendDiv = function() {
  return this.append(document.createElement("div"));
};
JendoSelector.prototype.appendP = function() {
  return this.append(document.createElement("p"));
};
JendoSelector.prototype.appendSamp = function() {
  return this.append(document.createElement("samp"));
};
JendoSelector.prototype.appendI = function() {
  return this.append(document.createElement("i"));
};
JendoSelector.prototype.appendSpan = function(text) {
  var elm = this.append(document.createElement("span"));
  if (!jendo.isNullOrEmpty(text)) {
    elm.text(text);
  }
  return elm;
};
JendoSelector.prototype.appendA = function(href, text) {
  var elm = this.append(document.createElement("a")).attr("href", href);
  if (!jendo.isNullOrEmpty(text)) {
    elm.text(text);
  }
  return elm;
};
JendoSelector.prototype.appendButton = function(text) {
  var elm = this.append(document.createElement("button")).attr("type", "button");
  if (!jendo.isNullOrEmpty(text)) {
    elm.text(text);
  }
  return elm;
};
JendoSelector.prototype.appendText = function() {
  return this.append(document.createElement("input")).attr("type", "text");
};
JendoSelector.prototype.appendHidden = function(value) {
  var el = this.append(document.createElement("input")).attr("type", "hidden");
  el.val(value ? value : "");
  return el;
};
JendoSelector.prototype.appendImg = function(src) {
  return this.append(document.createElement("img")).attr("src", src);
};
JendoSelector.prototype.appendUL = function() {
  return this.append(document.createElement("ul"));
};
JendoSelector.prototype.appendLI = function() {
  return this.append(document.createElement("li"));
};
JendoSelector.prototype.appendTable = function() {
  return this.append(document.createElement("table"));
};
JendoSelector.prototype.appendTHead = function() {
  return this.append(document.createElement("thead"));
};
JendoSelector.prototype.appendTBody = function() {
  return this.append(document.createElement("tbody"));
};
JendoSelector.prototype.appendTR = JendoSelector.prototype.appendTRow = function() {
  return this.append(document.createElement("tr"));
};
JendoSelector.prototype.appendTH = function() {
  return this.append(document.createElement("th"));
};
JendoSelector.prototype.appendTD = function() {
  return this.append(document.createElement("td"));
};
JendoSelector.prototype.prepend = function(value) {
  var childNodes = [];
  var valueType = typeof value;
  if (valueType === "string") {
    for (var i = 0; i < this.items.length; i++) {
      var tItem = this.items[i];
      tItem.insertAdjacentHTML("afterbegin", value);
      childNodes.push(tItem.childNodes[0]);
    }
  } else {
    if (valueType === "object") {
      var valueCnstr = jendo.constructorName(value);
      if (valueCnstr === "HTMLCollection" || valueCnstr === "NodeList") {
        for (var vci = 0; vci < value.length; vci++) {
          this._insertFirstChild(this.items, value[vci], childNodes);
        }
      } else {
        if (valueCnstr === "JendoSelector") {
          var lastChildNodes = [];
          for (var vsi = value.items.length - 1; vsi >= 0; vsi--) {
            var vItem = value.items[vsi];
            var vItemCnstr = jendo.constructorName(vItem);
            if (vItemCnstr === "HTMLCollection" || vItemCnstr === "NodeList") {
              for (var iItem = vItem.length - 1; iItem >= 0; iItem--) {
                this._insertItemChild(this.items, vItem[iItem], lastChildNodes, childNodes);
              }
            } else {
              this._insertFirstChild(this.items, value.items[vsi], childNodes);
            }
          }
        } else {
          if (valueCnstr === "HTMLDocument") {
            var nodes = value.body.childNodes;
            for (var vdi = 0; vdi < nodes.length; vdi++) {
              this._insertFirstChild(this.items, nodes[vdi], childNodes);
            }
          } else {
            this._insertFirstChild(this.items, value, childNodes);
          }
        }
      }
    }
  }
  return jendo(childNodes);
};
JendoSelector.prototype.insert = function(newNode, referenceNode) {
  if (!jendo.isNull(newNode)) {
    if (jendo.isNull(referenceNode)) {
      for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        item.insertBefore(newNode, item.childNodes[0]);
      }
    } else {
      var refElement = this.find(referenceNode).first();
      for (var ri = 0; ri < this.items.length; ri++) {
        var itemR = this.items[ri];
        itemR.insertBefore(newNode, refElement);
      }
    }
  }
  return this;
};
JendoSelector.prototype.insertElement = function(tagName, referenceNode) {
  if (jendo.isNullOrWhiteSpace(tagName)) {
    return null;
  } else {
    var element = document.createElement(tagName);
    this.insert(element, referenceNode);
    return jendo(element);
  }
};
JendoSelector.prototype.insertDiv = function() {
  return this.insertElement("div");
};
JendoSelector.prototype.insertP = function() {
  return this.insertElement("p");
};
JendoSelector.prototype.insertSamp = function() {
  return this.insertElement("samp");
};
JendoSelector.prototype.insertAfter = function(newNode, referenceNode) {
  if (!jendo.isNull(newNode)) {
    if (jendo.isNull(referenceNode)) {
      for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        item.appendChild(newNode);
      }
    } else {
      var valueConstructor = jendo.constructorName(referenceNode);
      var refElement;
      if (valueConstructor === "JendoSelector") {
        refElement = referenceNode.first().nextSibling;
      } else {
        refElement = this.find(referenceNode).first().nextSibling;
      }
      for (var ri = 0; ri < this.items.length; ri++) {
        var itemR = this.items[ri];
        itemR.insertBefore(newNode, refElement);
      }
    }
  }
  return this;
};
JendoSelector.prototype.load = function(url, callback) {
  if (this.items.length > 0) {
    var elements = this.items;
    jendo.get(url).done(function(responseText) {
      var doc = jendo.parseHTML(responseText);
      if (doc !== null) {
        var docNodes = doc.body.childNodes;
        var loadWaiting = 0;
        for (var i = 0; i < elements.length; i++) {
          loadWaiting += 1;
          jendo.loadDOM(docNodes, elements[i], true, function() {
            loadWaiting -= 1;
            if (loadWaiting === 0 && !jendo.isNull(callback)) {
              callback();
            }
          });
        }
      } else {
        if (!jendo.isNull(callback)) {
          callback();
        }
      }
    }).send();
  }
  return this;
};
JendoSelector.prototype.loadHTML = function(value, callback) {
  var doc = jendo.parseHTML(value);
  if (doc !== null) {
    var docNodes = doc.body.childNodes;
    var loadWaiting = 0;
    for (var i = 0; i < this.items.length; i++) {
      loadWaiting += 1;
      jendo.loadDOM(docNodes, this.items[i], true, function() {
        loadWaiting -= 1;
        if (loadWaiting === 0 && !jendo.isNull(callback)) {
          callback();
        }
      });
    }
  } else {
    if (!jendo.isNull(callback)) {
      callback();
    }
  }
  return this;
};
JendoSelector.prototype.remove = function(value) {
  if (jendo.isNull(value)) {
    for (var i = this.items.length - 1; i >= 0; i--) {
      this.items[i].remove();
    }
    this.items.pop();
    this.length = this.items.length;
    return this;
  } else {
    for (var i = this.items.length - 1; i >= 0; i--) {
      var rItem = this.items[i];
      if (jendo(rItem).is(value)) {
        rItem.remove();
        this.items.splice(i, 1);
      }
      this.length = this.items.length;
    }
    return this;
  }
};
JendoSelector.prototype.removeChild = function(value) {
  if (jendo.isNull(value)) {
    return this;
  } else {
    var jdNode = jendo(value);
    this.forEach(function(i, item) {
      jdNode.forEach(function(n, nItem) {
        item.removeChild(nItem);
      });
    });
    return this;
  }
};
JendoSelector.prototype.empty = function() {
  this.forEach(function(i, item) {
    while (item.firstChild) {
      item.removeChild(item.firstChild);
    }
  });
};
JendoSelector.prototype.css = JendoSelector.prototype.style = function(property, value) {
  if (jendo.isNull(property)) {
    return this;
  } else {
    if (jendo.isNull(value)) {
      var prType = typeof property;
      if (prType === "string") {
        if (property.indexOf(":") === -1) {
          var thisItem = this.first();
          var styleItem = thisItem.ownerDocument.defaultView ? thisItem.ownerDocument.defaultView.getComputedStyle(thisItem, null) : thisItem.currentStyle;
          return thisItem.style[property];
        } else {
          var propList = property.split(";");
          for (var i = 0; i < propList.length; i++) {
            var propVal = propList[i].split(":");
            if (propVal.length === 2) {
              this.css(propVal[0], propVal[1]);
            }
          }
          return this;
        }
      } else {
        if (prType === "object") {
          if (Array.isArray(property)) {
            var fItem = this.first();
            var fIStyle = fItem.ownerDocument.defaultView ? fItem.ownerDocument.defaultView.getComputedStyle(fItem, null) : fItem.currentStyle;
            var props = {};
            for (var fi = 0; fi < property.length; fi++) {
              var prName = property[fi];
              props[prName] = fIStyle[prName];
            }
            return props;
          } else {
            var prNms = Object.getOwnPropertyNames(property);
            for (var ni = 0; ni < prNms.length; ni++) {
              var nm = prNms[ni];
              this.css(nm, property[nm]);
            }
            return this;
          }
        } else {
          return this;
        }
      }
    } else {
      var vlType = typeof value;
      if (vlType === "string") {
        this.forEach(function(i, item) {
          item.style[property] = value;
        });
        return this;
      } else {
        if (vlType === "function") {
          this.forEach(function(i, item) {
            var styleItem = item.ownerDocument.defaultView ? item.ownerDocument.defaultView.getComputedStyle(item, null) : item.currentStyle;
            var itemValue = styleItem[property];
            var fnValue = eval("value(i, itemValue)");
            item.style[property] = fnValue;
          });
          return this;
        } else {
          return this;
        }
      }
    }
  }
};
JendoSelector.prototype.hide = function() {
  this.style("display", "none");
  return this;
};
JendoSelector.prototype.show = function() {
  this.style("display", "block");
  return this;
};
JendoSelector.prototype.first = function() {
  if (this.items.length > 0) {
    return this.items[0];
  } else {
    return null;
  }
};
JendoSelector.prototype.last = function() {
  if (this.items.length > 0) {
    return this.items[this.items.length - 1];
  } else {
    return null;
  }
};
JendoSelector.prototype.children = function() {
  var childNodes = [];
  this.forEach(function(i, item) {
    var icNodes = item.childNodes;
    for (var i = 0; i < icNodes.length; i++) {
      childNodes.push(icNodes[i]);
    }
  });
  return jendo(childNodes);
};
JendoSelector.prototype.lastChild = function() {
  var childNodes = [];
  this.forEach(function(i, item) {
    var icNodes = item.childNodes;
    if (icNodes.length > 0) {
      childNodes.push(icNodes[icNodes.length - 1]);
    }
  });
  return jendo(childNodes);
};
JendoSelector.prototype.top = function(value) {
  if (jendo.isNull(value)) {
    var element = this.first();
    if (element != null) {
      return element.offsetTop;
    } else {
      return 0;
    }
  } else {
    var element = this.first();
    if (element != null) {
      return element.style.top = value + "px";
    }
    return this;
  }
};
JendoSelector.prototype.bottom = function(value) {
  if (jendo.isNull(value)) {
    var element = this.first();
    if (element != null) {
      return element.offsetTop;
    } else {
      return 0;
    }
  } else {
    var element = this.first();
    if (element != null) {
      return element.style.bottom = value + "px";
    }
    return this;
  }
};
JendoSelector.prototype.left = function(value) {
  if (jendo.isNull(value)) {
    var element = this.first();
    if (element != null) {
      return element.offsetLeft;
    } else {
      return 0;
    }
  } else {
    var element = this.first();
    if (element != null) {
      return element.style.left = value + "px";
    }
    return this;
  }
};
JendoSelector.prototype.right = function(value) {
  if (jendo.isNull(value)) {
    var element = this.first();
    if (element != null) {
      return jendo(element).parent().width() - (element.offsetLeft + element.offsetWidth);
    } else {
      return 0;
    }
  } else {
    var element = this.first();
    if (element != null) {
      return element.style.right = value + "px";
    }
    return this;
  }
};
JendoSelector.prototype.position = function(value) {
  if (jendo.isNull(value)) {
    var element = this.first();
    if (!jendo.isNull(element)) {
      return {top:element.offsetTop, left:element.offsetLeft};
    } else {
      return {top:0, left:0};
    }
  } else {
    var element = this.first();
    if (!jendo.isNull(element) && "style" in element) {
      if (!jendo.isNull(value.top)) {
        element.style.top = value.top + "px";
      }
      if (!jendo.isNull(value.left)) {
        element.style.left = value.left + "px";
      }
    }
    return this;
  }
};
JendoSelector.prototype.width = function(value) {
  if (jendo.isNull(value)) {
    var el = this.first();
    if (el) {
      var cnstrName = el.constructor.name;
      if (cnstrName === "HTMLDocument") {
        return el.body.clientWidth;
      } else {
        return el.clientWidth;
      }
    } else {
      return 0;
    }
  } else {
    if (typeof value === "number") {
      if (value === 0) {
        this.removeAttr("width");
      } else {
        this.attr("width", value + "px");
      }
    } else {
      if (value === "") {
        this.removeAttr("width");
      } else {
        this.attr("width", value);
      }
    }
    return this;
  }
};
JendoSelector.prototype.height = function(value) {
  if (jendo.isNull(value)) {
    var el = this.first();
    if (el) {
      var cnstrName = el.constructor.name;
      if (cnstrName === "HTMLDocument") {
        return el.body.clientHeight;
      } else {
        return el.clientHeight;
      }
    } else {
      return 0;
    }
  } else {
    if (typeof value === "number") {
      if (value === 0) {
        this.removeAttr("height");
      } else {
        this.attr("height", value + "px");
      }
    } else {
      if (value === "") {
        this.removeAttr("height");
      } else {
        this.attr("height", value);
      }
    }
    return this;
  }
};
JendoSelector.prototype.offsetWidth = function(value) {
  if (jendo.isNull(value)) {
    var el = this.first();
    if (el) {
      var cnstrName = el.constructor.name;
      if (cnstrName === "HTMLDocument") {
        return el.body.offsetWidth;
      } else {
        return el.offsetWidth;
      }
    } else {
      return 0;
    }
  } else {
    if (typeof value === "number") {
      if (value === 0) {
        this.removeAttr("width");
      } else {
        this.attr("width", value + "px");
      }
    } else {
      if (value === "") {
        this.removeAttr("width");
      } else {
        this.attr("width", value);
      }
    }
    return this;
  }
};
JendoSelector.prototype.offsetHeight = function(value) {
  if (jendo.isNull(value)) {
    var el = this.first();
    if (el) {
      var cnstrName = el.constructor.name;
      if (cnstrName === "HTMLDocument") {
        return el.body.offsetHeight;
      } else {
        return el.offsetHeight;
      }
    } else {
      return 0;
    }
  } else {
    if (typeof value === "number") {
      if (value === 0) {
        this.removeAttr("height");
      } else {
        this.attr("height", value + "px");
      }
    } else {
      if (value === "") {
        this.removeAttr("height");
      } else {
        this.attr("height", value);
      }
    }
    return this;
  }
};
JendoSelector.prototype.each = JendoSelector.prototype.forEach = function(callback) {
  if (callback) {
    for (var i = 0; i < this.items.length; i++) {
      callback(i, this.items[i]);
    }
  }
};
JendoSelector.prototype.dispatchJEvent = function(type, detail) {
  for (var i = 0; i < this.items.length; i++) {
    var item = this.items[i];
    var jdListener = item.jdListener;
    if (!jendo.isNull(jdListener)) {
      var typeListener = jdListener[type];
      if (!jendo.isNull(typeListener)) {
        for (var l = 0; l < typeListener.length; l++) {
          var listenerFunc = typeListener[l].listener;
          if (!jendo.isNull(listenerFunc)) {
            item.detail = detail;
            listenerFunc(item);
          }
        }
      }
    }
  }
  return this;
};
JendoSelector.prototype.dispatchEvent = function(type, detail) {
  var evt = new CustomEvent(type, {detail:detail, bubbles:true, cancelable:true});
  for (var i = 0; i < this.items.length; i++) {
    this.items[i].dispatchEvent(evt);
  }
  return this;
};
JendoSelector.prototype.addJListener = function(type, listener, data) {
  for (var i = 0; i < this.items.length; i++) {
    var item = this.items[i];
    if (jendo.isNull(item.jdListener)) {
      item.jdListener = {};
      item.jdListener[type] = [];
    } else {
      if (jendo.isNull(item.jdListener[type])) {
        item.jdListener[type] = [];
      }
    }
    item.jdListener[type].push({listener:listener, data:data});
  }
};
JendoSelector.prototype.on = JendoSelector.prototype.addEventListener = function(type, listener, data) {
  if (jendo.isNull(data)) {
    for (var i = 0; i < this.items.length; i++) {
      this.items[i].addEventListener(type, listener, false);
    }
  } else {
    for (var x = 0; x < this.items.length; x++) {
      this.items[x].addEventListener(type, function(e) {
        e.data = data;
        listener(e);
      }, false);
    }
  }
  return this;
};
JendoSelector.prototype.keyDown = function(listener, data) {
  return this.addEventListener("keydown", listener, data);
};
JendoSelector.prototype.keyPress = function(listener, data) {
  return this.addEventListener("keypress", listener, data);
};
JendoSelector.prototype.keyUp = function(listener, data) {
  return this.addEventListener("keyup", listener, data);
};
JendoSelector.prototype.mouseEnter = function(listener, data) {
  return this.addEventListener("mouseenter", listener, data);
};
JendoSelector.prototype.mouseOver = function(listener, data) {
  return this.addEventListener("mouseover", listener, data);
};
JendoSelector.prototype.mouseMove = function(listener, data) {
  return this.addEventListener("mousemove", listener, data);
};
JendoSelector.prototype.mouseDown = function(listener, data) {
  return this.addEventListener("mousedown", listener, data);
};
JendoSelector.prototype.mouseUp = function(listener, data) {
  return this.addEventListener("mouseup", listener, data);
};
JendoSelector.prototype.click = function(listener, data) {
  return this.addEventListener("click", listener, data);
};
JendoSelector.prototype.dblClick = function(listener, data) {
  return this.addEventListener("dblclick", listener, data);
};
JendoSelector.prototype.contextMenu = function(listener, data) {
  return this.addEventListener("contextmenu", listener, data);
};
JendoSelector.prototype.wheel = function(listener, data) {
  return this.addEventListener("wheel", listener, data);
};
JendoSelector.prototype.mouseLeave = function(listener, data) {
  return this.addEventListener("mouseleave", listener, data);
};
JendoSelector.prototype.mouseOut = function(listener, data) {
  return this.addEventListener("mouseout", listener, data);
};
JendoSelector.prototype.select = function(listener, data) {
  return this.addEventListener("select", listener, data);
};
JendoSelector.prototype.pointerLockChange = function(listener, data) {
  return this.addEventListener("pointerlockchange", listener, data);
};
JendoSelector.prototype.pointerLockError = function(listener, data) {
  return this.addEventListener("pointerlockerror", listener, data);
};
JendoSelector.prototype.focus = function(listener, data) {
  if (jendo.isNullOrEmpty(listener)) {
    this.forEach(function(i, item) {
      item.focus();
    });
    return this;
  } else {
    return this.addEventListener("focus", listener, data);
  }
};
JendoSelector.prototype.blur = function(listener, data) {
  if (jendo.isNullOrEmpty(listener)) {
    this.forEach(function(i, item) {
      item.blur();
    });
    return this;
  } else {
    return this.addEventListener("blur", listener, data);
  }
};
JendoSelector.prototype.change = function(listener, data) {
  return this.addEventListener("change", listener, data);
};
JendoSelector.prototype.onClick = function(handler) {
  for (var i = 0; i < this.items.length; i++) {
    this.items[i].onclick = handler;
  }
  return this;
};
JendoSelector.prototype.onMouseDown = function(handler) {
  for (var i = 0; i < this.items.length; i++) {
    this.items[i].onmousedown = handler;
  }
  return this;
};
JendoSelector.prototype.onMouseUp = function(handler) {
  for (var i = 0; i < this.items.length; i++) {
    this.items[i].onmouseup = handler;
  }
  return this;
};
JendoSelector.prototype.onMouseMove = function(handler) {
  for (var i = 0; i < this.items.length; i++) {
    this.items[i].onmousemove = handler;
  }
  return this;
};
function JendoHttpRequest(method, url) {
  var jndreq = this;
  this._eventsDone = null;
  this._raisesDone = function(result, statusText) {
    if (jndreq._eventsDone !== null) {
      for (var rd = 0; rd < jndreq._eventsDone.length; rd++) {
        jndreq._eventsDone[rd](result, statusText);
      }
    }
    jndreq._raisesAlways(result, statusText);
  };
  this._eventsFail = null;
  this._raisesFail = function(result, statusText) {
    if (jndreq._eventsFail !== null) {
      for (var rf = 0; rf < jndreq._eventsFail.length; rf++) {
        jndreq._eventsFail[rf](result, statusText);
      }
    }
    jndreq._raisesAlways(result, statusText);
  };
  this._eventsAlways = null;
  this._raisesAlways = function(result, statusText) {
    if (jndreq._eventsAlways !== null) {
      for (var ra = 0; ra < jndreq._eventsAlways.length; ra++) {
        jndreq._eventsAlways[ra](result, statusText);
      }
    }
  };
  this._eventsProcessing = null;
  this._raisesProcessing = function(result, statusText) {
    if (jndreq._eventsProcessing !== null) {
      for (var rp = 0; rp < jndreq._eventsProcessing.length; rp++) {
        jndreq._eventsProcessing[rp](result, statusText);
      }
    }
  };
  this._contentType = "application/x-www-form-urlencoded; charset=UTF-8";
  if ("XMLHttpRequest" in window) {
    if ("ActiveXObject" in window && jendo.isFileProtocol()) {
      try {
        this._request = new window.ActiveXObject("MSXML2.XMLHTTP");
      } catch (ex1) {
        try {
          this._request = new window.ActiveXObject("Microsoft.XMLHTTP");
        } catch (ex2) {
          console.error(ex1, ex2);
        }
      }
    } else {
      this._request = new XMLHttpRequest;
      this._request.responseType = "text";
    }
  } else {
    if ("ActiveXObject" in window) {
      try {
        this._request = new window.ActiveXObject("MSXML2.XMLHTTP");
      } catch (ex3) {
        try {
          this._request = new window.ActiveXObject("Microsoft.XMLHTTP");
        } catch (ex4) {
          console.error(ex3, ex4);
        }
      }
    }
  }
  this._method = method;
  this._url = jendo.validateUrl(url);
  this._async = true;
  var xmlhttp = this._request;
  xmlhttp.onreadystatechange = function(e) {
    switch(xmlhttp.readyState) {
      case 0:
        jndreq._raisesProcessing(xmlhttp, e);
        break;
      case 1:
        jndreq._raisesProcessing(xmlhttp, e);
        break;
      case 2:
        jndreq._raisesProcessing(xmlhttp, e);
        break;
      case 3:
        jndreq._raisesProcessing(xmlhttp, e);
        break;
      case 4:
        jndreq._raisesProcessing(xmlhttp, e);
        if (xmlhttp.status === 0 || xmlhttp.status === 200) {
          var contentType = xmlhttp.getResponseHeader("Content-Type");
          var rsData = null;
          if (!jendo.isNullOrWhiteSpace(contentType)) {
            if (contentType.startsWith("application/json")) {
              try {
                rsData = JSON.parse(xmlhttp.responseText);
              } catch (ex) {
                console.log(ex);
                rsData = xmlhttp.responseText;
              }
            } else {
              rsData = xmlhttp.responseText;
            }
          } else {
            rsData = xmlhttp.responseText;
          }
          jndreq._raisesDone(rsData, "success");
        } else {
          jndreq._raisesFail(xmlhttp, "error");
        }
        break;
      default:
        break;
    }
  };
}
JendoHttpRequest.prototype.contentType = function(value) {
  if (jendo.isNull(value)) {
    return this._contentType;
  } else {
    this._contentType = value;
    return this;
  }
};
JendoHttpRequest.prototype.done = function(handler) {
  if (this._eventsDone === null) {
    this._eventsDone = [];
  }
  this._eventsDone.push(handler);
  return this;
};
JendoHttpRequest.prototype.fail = function(handler) {
  if (this._eventsFail === null) {
    this._eventsFail = [];
  }
  this._eventsFail.push(handler);
  return this;
};
JendoHttpRequest.prototype.always = function(handler) {
  if (this._eventsAlways === null) {
    this._eventsAlways = [];
  }
  this._eventsAlways.push(handler);
  return this;
};
JendoHttpRequest.prototype.processing = function(handler) {
  if (this._eventsProcessing === null) {
    this._eventsProcessing = [];
  }
  this._eventsProcessing.push(handler);
  return this;
};
JendoHttpRequest.prototype.send = function(data) {
  if (data === undefined) {
    data = null;
  }
  this._request.open(this._method, this._url, this._async);
  if (data === null) {
    this._request.setRequestHeader("Content-Type", this._contentType);
    this._request.send();
  } else {
    var typeData = typeof data;
    if (typeData === "string") {
      this._request.setRequestHeader("Content-Type", this._contentType);
      this._request.send(data);
    } else {
      if (typeData === "object") {
        if (data instanceof FormData) {
          this._request.send(data);
        } else {
          var dataRq = "";
          var isFirstKey = true;
          for (var key in data) {
            if (isFirstKey) {
              dataRq = key + "=" + encodeURIComponent(data[key]);
              isFirstKey = false;
            } else {
              dataRq += "&" + key + "=" + encodeURIComponent(data[key]);
            }
          }
          this._request.setRequestHeader("Content-Type", this._contentType);
          this._request.send(dataRq);
        }
      } else {
        this._request.setRequestHeader("Content-Type", this._contentType);
        this._request.send();
      }
    }
  }
};
JendoHttpRequest.prototype.sendJson = function(data) {
  this.contentType("application/json");
  this.send(JSON.stringify(data));
};
function JendoJSONPRequest(url) {
  this._url = url;
}
JendoJSONPRequest.prototype.done = function(doneFunction) {
  this._onDone = doneFunction;
  return this;
};
JendoJSONPRequest.prototype.fail = function(failFunction) {
  this._onFail = failFunction;
  return this;
};
JendoJSONPRequest.prototype.always = function(alwaysFunction) {
  this._onAlways = alwaysFunction;
  return this;
};
JendoJSONPRequest.prototype.send = function(data) {
  var callbackFun = "callback" + jendo.guid("");
  var on_done = this._onDone;
  var on_always = this._onAlways;
  var on_fail = this._onFail;
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.async = true;
  if (this._url.indexOf("?") > -1) {
    script.src = this._url + "&callback=" + callbackFun;
  } else {
    script.src = this._url + "?callback=" + callbackFun;
  }
  script.onerror = function(dataRs) {
    if (on_fail) {
      on_fail(dataRs, dataRs.type);
    }
    if (on_always) {
      on_always(dataRs, dataRs.type);
    }
  };
  window[callbackFun] = function(dataRs) {
    if (on_done) {
      on_done(dataRs, "success");
    }
    if (on_always) {
      on_always(dataRs, "success");
    }
  };
  var head = document.getElementsByTagName("head");
  if (head[0] !== null) {
    head[0].appendChild(script);
  }
};
function JendoLanguage() {
}
JendoLanguage.prototype.load = function(value) {
  if (typeof value == "object") {
    for (var x in value) {
      this[x] = value[x];
    }
  }
};
jendo.lang = new JendoLanguage;
function JendoCookies() {
}
JendoCookies.prototype.set = function setCookie(cname, cvalue, exdays) {
  var d = new Date;
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};
JendoCookies.prototype.get = function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};
jendo.cookies = jendo.cookies ? jendo.cookies : new JendoCookies;
function JendoGeolocation(options) {
  this.MSG_NOT_SUPPORTED = "Geolocation is not supported by this browser.";
  this.glOptions = {enableHighAccuracy:false, timeout:5000, maximumAge:0};
  if (!jendo.isNull(options)) {
    if ("enableHighAccuracy" in options) {
      this.glOptions.enableHighAccuracy = options.enableHighAccuracy;
    }
    if ("timeout" in options) {
      this.glOptions.timeout = options.timeout;
    }
    if ("maximumAge" in options) {
      this.glOptions.maximumAge = options.maximumAge;
    }
  }
}
JendoGeolocation.prototype.enableHighAccuracy = function(value) {
  if (jendo.isNull(value)) {
    return this.glOptions.enableHighAccuracy;
  } else {
    this.glOptions.enableHighAccuracy = value;
  }
};
JendoGeolocation.prototype.timeout = function(value) {
  if (jendo.isNull(value)) {
    return this.glOptions.timeout;
  } else {
    this.glOptions.timeout = value;
  }
};
JendoGeolocation.prototype.maximumAge = function(value) {
  if (jendo.isNull(value)) {
    return this.glOptions.maximumAge;
  } else {
    this.glOptions.maximumAge = value;
  }
};
JendoGeolocation.prototype.get = function(successCallback, errorCallback, options) {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successCallback, errorCallback, jendo.isNull(options) ? this.glOptions : options);
  } else {
    console.log(this.MSG_NOT_SUPPORTED);
  }
};
JendoGeolocation.prototype.watch = function(successCallback, errorCallback, options) {
  if (navigator.geolocation) {
    return navigator.geolocation.watchPosition(successCallback, errorCallback, jendo.isNull(options) ? this.glOptions : options);
  } else {
    console.log(this.MSG_NOT_SUPPORTED);
  }
};
JendoGeolocation.prototype.clear = function(id) {
  if (navigator.geolocation) {
    return navigator.geolocation.clearWatch(id);
  } else {
    console.log(this.MSG_NOT_SUPPORTED);
  }
};
jendo.geolocation = function(options) {
  return new JendoGeolocation(options);
};
function JendoNavigator() {
}
JendoNavigator.prototype.getUserMedia = function(constraints, successCallback, errorCallback) {
  if (navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia(constraints).then(successCallback).catch(errorCallback);
  } else {
    if (navigator.getUserMedia) {
      navigator.getUserMedia(constraints, successCallback, errorCallback);
    } else {
      if (navigator.webkitGetUserMedia) {
        navigator.webkitGetUserMedia(constraints, successCallback, errorCallback);
      } else {
        if (navigator.mozGetUserMedia) {
          navigator.mozGetUserMedia(constraints, successCallback, errorCallback);
        } else {
          if (navigator.msGetUserMedia) {
            navigator.msGetUserMedia(constraints, successCallback, errorCallback);
          } else {
            var error = "Native web camera streaming (getUserMedia) not supported in this browser.";
            if (!jendo.isNull(errorCallback)) {
              qrcodeErrorEvent(error);
            } else {
              console.log(error);
            }
          }
        }
      }
    }
  }
};
jendo.navigator = jendo.navigator ? jendo.navigator : new JendoNavigator;
if (jendo.isNull(jendo.rootUrl)) {
  jendo.rootUrl = "";
}
if (jendo.isNull(jendo.pageLinkType)) {
  jendo.pageLinkType = 0;
}
var _ = jendo;
function _jendoSortMultiple() {
  var props = _jendoSortArguments(arguments);
  return function(obj1, obj2) {
    var i = 0, result = 0, numberOfProperties = props.length;
    while (result === 0 && i < numberOfProperties) {
      var prop = props[i];
      result = _jendoSort(prop.field, prop.sort)(obj1, obj2);
      i++;
    }
    return result;
  };
}
function _jendoReverseMultiple() {
  var props = _jendoSortArguments(arguments);
  return function(obj1, obj2) {
    var i = 0, result = 0, numberOfProperties = props.length;
    while (result === 0 && i < numberOfProperties) {
      var prop = props[i];
      result = _jendoSort(prop.field, prop.sort)(obj1, obj2) * -1;
      i++;
    }
    return result;
  };
}
function _jendoSort(property, sortOrder) {
  return function(a, b) {
    var ap = a[property];
    if (typeof ap == "string") {
      ap = ap.toLowerCase();
    }
    var bp = b[property];
    if (typeof bp == "string") {
      bp = bp.toLowerCase();
    }
    var result = ap < bp ? -1 : ap > bp ? 1 : 0;
    return result * sortOrder;
  };
}
function _jendoSortArguments(args) {
  var props = [];
  for (arg in args) {
    var propValue = args[arg];
    var propType = typeof propValue;
    if (propType == "string") {
      var sortOrder = 1;
      if (propValue[0] === "-") {
        sortOrder = -1;
        propValue = propValue.substr(1, propValue.length - 1);
      }
      props.push({field:propValue, sort:sortOrder});
    } else {
      if (propType == "object" && Array.isArray(propValue)) {
        for (var ic = 0; ic < propValue.length; ic++) {
          props.push(propValue[ic]);
        }
      }
    }
  }
  return props;
}
Array.prototype.sortBy = function() {
  return this.sort(_jendoSortMultiple.apply(null, arguments));
};
Array.prototype.reverseBy = function() {
  return this.sort(_jendoReverseMultiple.apply(null, arguments));
};
Array.prototype.filterBy = function(value) {
  if (Array.isArray(value)) {
    return this.filter(function(item, i, arr) {
      for (var f = 0; f < value.length; f++) {
        var vField = value[f].field;
        var vFilter = value[f].filter;
        if (typeof item == "object" && vFilter != "") {
          var iValue = item[vField];
          var iType = typeof iValue;
          if (iType == "undefined") {
            return false;
          } else {
            if (iType == "string") {
              iValue = iValue.toLowerCase();
              if (iValue.indexOf(vFilter.toLowerCase()) == -1) {
                return false;
              }
            } else {
              if (iValue != vFilter) {
                return false;
              }
            }
          }
        }
      }
      return true;
    });
  } else {
    return this.filter(value);
  }
};
window.onload = function() {
  if (typeof jendo.readyFunctions != "undefined") {
    if (typeof jendo.readyFunctionIndex == "undefined") {
      jendo.readyFunctionIndex = 0;
    }
    while (jendo.readyFunctionIndex < jendo.readyFunctions.length) {
      var readyFunction = jendo.readyFunctions[jendo.readyFunctionIndex];
      jendo.readyFunctionIndex += 1;
      readyFunction();
    }
  }
};

