/*
 Jendo UI v1.1.5
 https://approxys.com/

 Copyright Approxys Ltd
 Released under the MIT License
*/
jendo.dateFormatUI = function(date, pattern) {
  if (jendo.constructorName(date).toLowerCase() === "date") {
    var nMM = date.getMonth();
    var wk = date.getDay();
    return jendo.dateFormat(date, pattern.replace(/MMMM/g, "p1").replace(/MMM/g, "p2").replace(/dddd/g, "p3").replace(/ddd/g, "p4")).replace(/p1/g, jendo.lang.monthNames[nMM]).replace(/p2/g, jendo.lang.monthShortNames[nMM]).replace(/p3/g, jendo.lang.weekdayNames[wk]).replace(/p4/g, jendo.lang.weekdayShortNames[wk]);
  } else {
    return "";
  }
};
jendo.lang.load({weekdayNames:["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], weekdayShortNames:["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], weekdayNarrowNames:["S", "M", "T", "W", "T", "F", "S"], monthNames:["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], monthShortNames:["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]});
var _JendoLastOpenElement = null;
var _JendoFuncCloseElement = null;
document.addEventListener("click", function(e) {
  if (_JendoFuncCloseElement != null && _JendoLastOpenElement != null) {
    _JendoFuncCloseElement(e.target);
  }
}, true);
function JendoSection(elements) {
  this.sectionElements = elements;
}
JendoSection.prototype.append = function(jsonData) {
  if (!jendo.isNull(this.sectionElements) && !jendo.isNull(jsonData)) {
    var section = document.createElement("div");
    section.className = "jd-section jd-section-info";
    var header = document.createElement("div");
    header.className = "jd-header";
    section.appendChild(header);
    var headerH3 = document.createElement("h3");
    header.appendChild(headerH3);
    var headerA = document.createElement("A");
    headerA.href = jsonData.link;
    headerA.appendChild(document.createTextNode(jsonData.title));
    headerH3.appendChild(headerA);
    var summary = document.createElement("div");
    summary.className = "jd-summary";
    summary.appendChild(document.createTextNode(jsonData.text));
    section.appendChild(summary);
    this.sectionElements.append(section);
  }
};
JendoSection.prototype.load = function(jsonData) {
  if (!jendo.isNull(this.sectionElements) && !jendo.isNull(jsonData)) {
    for (var i = 0; i < jsonData.length; i++) {
      this.append(jsonData[i]);
    }
  }
};
jendo.section = function(element) {
  if (jendo.isNull(element)) {
    return new JendoSection(null);
  } else {
    var targetElement = jendo(element);
    if (targetElement.length > 0) {
      return new JendoSection(targetElement);
    } else {
      return new JendoSection(null);
    }
  }
};
function JendoNavBar(elements) {
  this.sectionElements = elements;
}
JendoNavBar.prototype.append = function(jsonData) {
  if (!jendo.isNull(this.sectionElements) && !jendo.isNull(jsonData)) {
    var menuItem = this.sectionElements.appendLI();
    if ("items" in jsonData) {
      menuItem.class("dropdown");
      var userItem = null;
      if ("link" in jsonData) {
        userItem = menuItem.appendA(jsonData.link, jsonData.text);
      } else {
        userItem = menuItem.appendButton().html(jsonData.text + '<i class="fas fa-angle-down menu-down"></i><i class="fas fa-angle-up menu-up"></i>');
      }
      var thisNB = this;
      userItem.attr("role", "button").click(function(source) {
        var itemClk = jendo(source.target);
        var liElement = itemClk.closest(".dropdown");
        var canOpened = true;
        if (_JendoLastOpenElement !== null) {
          if (_JendoLastOpenElement.is(liElement)) {
            canOpened = false;
            _JendoLastOpenElement = null;
          }
        }
        if (canOpened) {
          _JendoFuncCloseElement = thisNB.closeElement;
          _JendoLastOpenElement = liElement;
          liElement.addClass("open");
        }
      });
      (new JendoNavBar(menuItem.appendUL().class("dropdown-menu"))).load(jsonData.items);
    } else {
      if (!jendo.isNullOrWhiteSpace(jsonData.link)) {
        return menuItem.appendA(jsonData.link, jsonData.text);
      } else {
        var self = this;
        return menuItem.appendButton(jsonData.text).click(function() {
          document.dispatchEvent(new CustomEvent("navbar.navigate", {detail:{src:self.sectionElements, args:jsonData.args}}));
        });
      }
    }
  }
};
JendoNavBar.prototype.closeElement = function(source) {
  var parentSrc = _(source).closest(".dropdown");
  if (_JendoLastOpenElement.is(parentSrc)) {
    _JendoLastOpenElement.removeClass("open");
    _JendoFuncCloseElement = null;
  } else {
    if (!_JendoLastOpenElement.isContains(source.parentElement)) {
      _JendoLastOpenElement.removeClass("open");
      _JendoFuncCloseElement = null;
      _JendoLastOpenElement = null;
    }
  }
};
JendoNavBar.prototype.load = function(jsonData) {
  if (!jendo.isNull(this.sectionElements) && !jendo.isNull(jsonData)) {
    for (var i = 0; i < jsonData.length; i++) {
      this.append(jsonData[i]);
    }
  }
};
JendoNavBar.prototype.navigate = function(func) {
  var self = this;
  document.addEventListener("navbar.navigate", function(e) {
    if (e.detail.src === self.sectionElements) {
      func(e, e.detail.args);
    }
  }, false);
  return this;
};
jendo.navbar = function(element) {
  jendo._IsInitNavbar = true;
  if (jendo.isNull(element)) {
    return new JendoNavBar(null);
  } else {
    var jdElement = jendo(element);
    if (jdElement.length > 0) {
      return new JendoNavBar(jdElement.appendUL());
    } else {
      return new JendoNavBar(null);
    }
  }
};
function JendoAccordion(elements) {
  this.jdAccMenu = jendo(elements);
  this._AMData = this.jdAccMenu.prop("amdata");
  if (jendo.isNull(this._AMData)) {
    this._AMData = {expandChar:"&#709;", collapseChar:"&#708;"};
    this.jdAccMenu.prop("amdata", this._AMData);
  }
}
JendoAccordion.prototype.data = function(value) {
  if (jendo.isNull(value)) {
    return this;
  }
  if ("expandChar" in value) {
    this._AMData.expandChar = value.expandChar;
  }
  if ("collapseChar" in value) {
    this._AMData.collapseChar = value.collapseChar;
  }
  return this;
};
JendoAccordion.prototype.append = function(jsonData, menu) {
  if (!jendo.isNull(this.jdAccMenu) && !jendo.isNull(jsonData)) {
    var jdMenu = jendo.isNull(menu) ? this.jdAccMenu : jendo(menu);
    var menuItem = jdMenu.appendLI();
    if ("id" in jsonData) {
      menuItem.attr("data-id", jsonData.id);
    }
    if ("items" in jsonData) {
      menuItem.class("dropdown");
      var userItem = null;
      if ("link" in jsonData) {
        userItem = menuItem.appendA(jsonData.link, jsonData.text);
      } else {
        userItem = menuItem.appendButton().html('<span class="menu-text">' + jsonData.text + '</span><span class="menu-down">' + this._AMData.expandChar + '</span><span class="menu-up">' + this._AMData.collapseChar + "</span>");
      }
      userItem.attr("role", "button").click(function(source) {
        var jdItem = jendo(source.target);
        var jdItemDD = jdItem.closest(".dropdown");
        var jdOpenDD = jdItemDD.parent().find(".open");
        jdOpenDD.removeClass("open");
        if (!jdOpenDD.is(jdItemDD)) {
          jdItemDD.addClass("open");
        }
      });
      var jdSubItem = menuItem.appendUL().class("dropdown-menu").attr("role", "menu");
      this._loadMenu(jdSubItem, jsonData.items);
    } else {
      if (!jendo.isNullOrWhiteSpace(jsonData.link)) {
        menuItem.appendA(jsonData.link).html('<span class="menu-text">' + jsonData.text + "</span>");
      } else {
        var self = this;
        var btnItem = menuItem.appendButton().html('<span class="menu-text">' + jsonData.text + "</span>");
        btnItem.click(function() {
          document.dispatchEvent(new CustomEvent("accordion.navigate", {detail:{src:self.jdAccMenu, args:jsonData.args}}));
        });
      }
    }
  }
};
JendoAccordion.prototype.closeElement = function(source) {
  var jdSrcDD = _(source).closest(".dropdown");
  if (jdSrcDD.length > 0) {
    var jdOpenDD = jdSrcDD.parent().find(".open");
    jdOpenDD.removeClass("open");
  }
};
JendoAccordion.prototype.load = function(jsonData, mItem) {
  return this._loadMenu(this.jdAccMenu, jsonData, mItem);
};
JendoAccordion.prototype._loadMenu = function(jdMenu, jsonData, mItem) {
  this.jdAccMenu = jdMenu;
  if (!jendo.isNull(jdMenu) && !jendo.isNull(jsonData)) {
    var jdMItem = jendo.isNull(mItem) ? jdMenu : jendo(jdMenu.find("[data-id=" + mItem + "]").find('[role="menu"]').first());
    jdMItem.html("");
    for (var i = 0; i < jsonData.length; i++) {
      this.append(jsonData[i], jdMItem);
    }
  }
  return this;
};
JendoAccordion.prototype.navigate = function(func) {
  var self = this;
  document.addEventListener("accordion.navigate", function(e) {
    if (self.jdAccMenu.isContains(e.detail.src)) {
      func(e, e.detail.args);
    }
  }, false);
  return this;
};
jendo.accordion = function(element) {
  if (jendo.isNull(element)) {
    return new JendoAccordion(null);
  } else {
    var jdElement = jendo(element);
    if (jdElement.length > 0) {
      var jdElUl = jdElement.find("ul");
      if (jdElUl.length > 0) {
        return new JendoAccordion(jdElUl);
      } else {
        return new JendoAccordion(jdElement.appendUL());
      }
    } else {
      return new JendoAccordion(null);
    }
  }
};
function JendoDialog(element) {
  this.jDialog = jendo(element);
  if (this.jDialog.length === 0) {
    return;
  }
  if (jendo.isNullOrEmpty(this.jDialog.attr("role"))) {
    this.jDialog.attr("role", "dialog");
  }
  if (jendo.isNullOrEmpty(this.jDialog.attr("class"))) {
    this.jDialog.attr("class", "jd-dialog");
  }
  this.dialogTitle = "Jendo Dialog";
  this.dialogModel = this.jDialog.prop("dialogModel");
  this.withTitlebar = true;
  this.withCloseBtnLeft = false;
  this.withCloseBtnRight = true;
  this.isModalForm = false;
}
JendoDialog.moveElement = null;
JendoDialog.moveTop = 0;
JendoDialog.moveLeft = 0;
JendoDialog.showPositionTop = 50;
JendoDialog.prototype.clear = function() {
  this.jDialog.html("");
  this.jDialog.prop("srcUrl", "");
  this.dialogModel = null;
  var el = this.jDialog.first();
  elClone = el.cloneNode(true);
  el.parentNode.replaceChild(elClone, el);
  this.jDialog = jendo(elClone);
  return this;
};
JendoDialog.prototype.load = function(args, callback) {
  var dialog = this.jDialog;
  var self = this;
  var url = typeof args === "object" ? args.url : args;
  var loadUrl = dialog.prop("srcUrl");
  if (loadUrl !== url) {
    jendo.get(url).done(function(responseText) {
      dialog.prop("srcUrl", url);
      dialog.loadHTML(responseText);
      if (typeof args === "object" && typeof args.model === "string") {
        var objModel = window[args.model];
        if (!jendo.isNull(objModel)) {
          self.dialogModel = new objModel({dialog:self});
          if (self.dialogModel) {
            if (self.dialogModel.onShow) {
              self.onShow(self.dialogModel.onShow);
            }
            if (self.dialogModel.onShown) {
              self.onShown(self.dialogModel.onShown);
            }
            if (self.dialogModel.onHide) {
              self.onHide(self.dialogModel.onHide);
            }
            if (self.dialogModel.onHidden) {
              self.onHidden(self.dialogModel.onHidden);
            }
          }
          dialog.prop("dialogModel", self.dialogModel);
        }
      }
      if (callback) {
        callback(self);
      }
    }).send();
  } else {
    if (callback) {
      callback(self);
    }
  }
};
JendoDialog.prototype.show = function(args) {
  if (this.jDialog.length === 0) {
    return;
  }
  if (jendo.isNull(args)) {
    args = {};
  } else {
    if (!jendo.isNull(args.style)) {
      this.jDialog.class("jd-dialog");
      this.jDialog.addClass(args.style);
    }
  }
  if (!jendo.isNull(this.dialogModel)) {
    if ("refresh" in args) {
      this.dialogModel.refresh = args.refresh;
    }
    args.model = this.dialogModel;
  }
  this.jDialog.dispatchJEvent("jendo.dialog.show", args);
  if (this.isModalForm) {
    this.jDialog.addClass("jd-dialog-modal");
  }
  var jdDocument = this.jDialog.find('[role="document"]');
  if (jdDocument.length === 0) {
    jdDocument = jendo.createDiv().class("jd-dialog-document").attr("role", "document").attr("tabindex", 0);
    jdDocument.append(this.jDialog.children());
    jdDocument.addClass(this.jDialog.data("class"));
    this.jDialog.append(jdDocument);
    this.jDialog.keyPress(function(e) {
      if (e.keyCode === 27) {
        jendo.dialog(this).hide();
      }
    });
  }
  var jdContent = jdDocument.find('[data-dialog="content"]');
  if (jdContent.length === 0) {
    jdContent = jdDocument.find(".jd-dialog-content");
    if (jdContent.length === 0) {
      jdContent = jendo.createDiv().class("jd-dialog-content").attr("data-dialog", "content");
      jdContent.append(jdDocument.children());
      jdDocument.append(jdContent);
    } else {
      jdContent.attr("data-dialog", "content");
    }
  }
  if (this.withTitlebar) {
    var jdTitlebar = jdDocument.find('[data-dialog="titlebar"]');
    if (jdTitlebar.length === 0) {
      jdTitlebar = jdDocument.find(".jd-dialog-titlebar");
      if (jdTitlebar.length === 0) {
        jdTitlebar = jdDocument.insertDiv().class("jd-dialog-titlebar").attr("data-dialog", "titlebar");
        if (this.withCloseBtnLeft) {
          jdTitlebar.appendElement().class("jd-dt-button jd-dt-button-left").appendButton().attr("data-dialog", "close").appendI().class("fas fa-chevron-left").attr("aria-hidden", "true");
        }
        jdTitlebar.appendDiv().class("jd-dt-title").text(jendo.isNullOrWhiteSpace(args.title) ? this.dialogTitle : args.title);
        if (this.withCloseBtnRight) {
          jdTitlebar.appendDiv().class("jd-dt-button jd-dt-button-right").appendButton().attr("data-dialog", "close").class("jd-btn").appendI().class("fas fa-times").attr("aria-hidden", "true");
        }
      } else {
        jdTitlebar.attr("data-dialog", "titlebar");
      }
    } else {
      if (jendo.isNullOrEmpty(jdDocument.attr("class"))) {
        jdDocument.attr("class", "jd-dialog-titlebar");
      }
      if (!jendo.isNullOrWhiteSpace(args.title)) {
        jdTitlebar.find(".jd-dt-title").text(args.title);
      }
    }
    var htmlDocument = jendo(document);
    jdTitlebar.mouseDown(function(e) {
      var targetEl = jendo(e.target);
      if (targetEl.attr("data-dialog") !== "close") {
        var docEl = JendoDialog.findDocument(e.target);
        if (!jendo.isNull(docEl)) {
          JendoDialog.moveElement = jendo(docEl);
          JendoDialog.moveTop = e.offsetY + e.target.offsetTop;
          JendoDialog.moveLeft = e.offsetX + e.target.offsetLeft;
        }
      }
    });
    htmlDocument.mouseUp(function(e) {
      JendoDialog.moveElement = null;
      JendoDialog.moveTop = 0;
      JendoDialog.moveLeft = 0;
    });
    htmlDocument.mouseMove(function(e) {
      if (JendoDialog.moveElement != null) {
        var pos = {top:0, left:0};
        pos.top = e.clientY - JendoDialog.moveTop;
        if (pos.top < 0) {
          pos.top = 0;
        }
        pos.left = e.clientX - JendoDialog.moveLeft;
        if (pos.left < 0) {
          pos.left = 0;
        }
        JendoDialog.moveElement.position(pos);
        JendoDialog.moveElement.prop("position", pos);
      }
    });
  }
  jdDocument.find('[data-dialog="close"]').click(function(e) {
    JendoDialog.findDialog(e.target).hide();
  });
  this.jDialog.show();
  var pos = jdDocument.prop("position");
  if (jendo.isNull(pos)) {
    if (jendo.screenWidth() == jdDocument.offsetWidth()) {
      pos = {top:0, left:0};
    } else {
      pos = {top:jdDocument.height() < jendo.windowHeight() ? JendoDialog.showPositionTop : 0, left:jendo.windowWidth() / 2 - jdDocument.width() / 2};
      if (!this.isModalForm) {
        JendoDialog.showPositionTop += 30;
      }
      jdDocument.prop("position", pos);
    }
  }
  jdDocument.position(pos);
  jdDocument.focus();
  this.jDialog.dispatchJEvent("jendo.dialog.shown", args);
};
JendoDialog.prototype.showModal = function(args) {
  this.isModalForm = true;
  this.show(args);
};
JendoDialog.prototype.hide = function(args) {
  if (this.jDialog.length === 0) {
    return;
  }
  if (jendo.isNull(args)) {
    args = {};
  }
  this.jDialog.dispatchJEvent("jendo.dialog.hide", args);
  this.jDialog.hide();
  this.jDialog.dispatchJEvent("jendo.dialog.hidden", args);
};
JendoDialog.prototype.onShow = function(listener) {
  this.jDialog.addJListener("jendo.dialog.show", listener);
  return this;
};
JendoDialog.prototype.onShown = function(listener) {
  this.jDialog.addJListener("jendo.dialog.shown", listener);
  return this;
};
JendoDialog.prototype.onHide = function(listener) {
  this.jDialog.addJListener("jendo.dialog.hide", listener);
  return this;
};
JendoDialog.prototype.onHidden = function(listener) {
  this.jDialog.addJListener("jendo.dialog.hidden", listener);
  return this;
};
JendoDialog.findDialog = function(target) {
  var elm = this.findParentElement(target, "role", "dialog");
  return new JendoDialog(elm);
};
JendoDialog.findDocument = function(target) {
  if (typeof target === "undefined") {
    return this.jDialog.find('[role="document"]');
  } else {
    return this.findParentElement(target, "role", "document");
  }
};
JendoDialog.findTitlebar = function(target) {
  return this.findParentElement(target, "data-dialog", "titlebar");
};
JendoDialog.findParentElement = function(target, attrName, attrValue) {
  var element = target.parentElement;
  while (element != null) {
    var elementRole = element.attributes[attrName];
    if (!jendo.isNull(elementRole) && elementRole.value.toLowerCase() == attrValue) {
      return element;
    }
    element = element.parentElement;
  }
  return null;
};
jendo.dialog = function(element) {
  return new JendoDialog(element);
};
JendoSelector.prototype.dialog = function(args) {
  var jDlg = new JendoDialog(this);
  if (args.modal === true) {
    jDlg.showModal();
  } else {
    jDlg.show();
  }
  return jDlg;
};
jendo.dialogInit = function() {
  jendo('[data-toggle="jd-dialog"]').forEach(function(i, item) {
    var jdItem = jendo(item);
    var dTarget = jdItem.data("target");
    if (!jendo.isNullOrWhiteSpace(dTarget)) {
      jdItem.click(function(e) {
        jendo.dialog(dTarget).show();
      });
    }
  });
};
function JendoDatepicker(element, options) {
  this.jdElement = element;
  this.dElement = this.jdElement.first();
  this.dpOptions = this.dElement.options;
  if (jendo.isNull(this.dpOptions)) {
    this.dpOptions = {weekdayStart:1, format:"yyyy-MM-dd", viewMode:"days", showButton:false, position:"left"};
    this.dElement.options = this.dpOptions;
  }
  if (!this.isInit()) {
    var dpParent = this.jdElement.parent();
    if (this.dpOptions.showButton) {
      var dBtn = document.createElement("i");
      dBtn.setAttribute("class", "fas fa-calendar");
      dBtn.setAttribute("aria-hidden", "true");
      dpParent.insertAfter(dBtn, this.jdElement);
    }
    this.dDatepicker = this.createDatepicker(this.jdElement.attrId() + "_datepicker");
    dpParent.insertAfter(this.dDatepicker, this.jdElement);
    var self = this;
    this.jdDatepicker = jendo(this.dDatepicker);
    this.jdElement.click(function(source) {
      if (self.jdDatepicker.isVisible()) {
        self.hide();
      } else {
        self.show();
      }
    });
    this.jdElement.keyDown(function(e) {
      if (e.keyCode === 9) {
        self.hide();
      }
    });
    this.jdElement.keyUp(function(e) {
      if (e.keyCode === 27) {
        self.hide();
      } else {
        if (!self.jdDatepicker.isVisible()) {
          self.show();
        }
      }
    });
    this.isInit(true);
  }
}
JendoDatepicker.prototype.isInit = function(value) {
  if (jendo.isNull(value)) {
    var isInit = this.dElement.jdp_IsInit;
    return jendo.isNull(isInit) ? false : isInit;
  } else {
    this.dElement.jdp_IsInit = value;
    return this;
  }
};
JendoDatepicker.prototype.hide = function() {
  this.jdDatepicker.hide();
  _JendoFuncCloseElement = null;
  _JendoLastOpenElement = null;
};
JendoDatepicker.prototype.hideElement = function(source) {
  if (!_JendoLastOpenElement.isContains(source)) {
    _JendoLastOpenElement.hide();
    _JendoFuncCloseElement = null;
    _JendoLastOpenElement = null;
  }
};
JendoDatepicker.prototype.format = function(pattern) {
  if (jendo.isNullOrWhiteSpace(pattern)) {
    return this.dpOptions.format;
  } else {
    this.dpOptions.format = pattern;
    if (!jendo.isNull(this.dElement.dateValue)) {
      this.jdElement.val(jendo.dateFormatUI(this.dElement.dateValue, pattern));
    }
    return this;
  }
};
JendoDatepicker.prototype.positionLeft = function() {
  this.dpOptions.position = "left";
  return this;
};
JendoDatepicker.prototype.positionRight = function() {
  this.dpOptions.position = "right";
  return this;
};
JendoDatepicker.prototype.createDatepicker = function(datepickerId) {
  var dp = document.createElement("div");
  dp.setAttribute("class", "datepicker");
  dp.id = datepickerId;
  var dpDate = jendo.now();
  this.loadDPDays(dp, dpDate.getFullYear(), dpDate.getMonth(), dpDate.getDate());
  return dp;
};
JendoDatepicker.prototype.addDPHead = function(content, titleText, titleClick, leftClick, rightClick) {
  var dpHead = document.createElement("div");
  dpHead.setAttribute("class", "dp-head");
  content.appendChild(dpHead);
  var dphLeft = document.createElement("div");
  dphLeft.setAttribute("class", "dph-left");
  dpHead.appendChild(dphLeft);
  var dphlButton = document.createElement("button");
  dphlButton.setAttribute("type", "button");
  dphlButton.innerText = "<";
  dphlButton.onclick = leftClick;
  dphLeft.appendChild(dphlButton);
  var dphTitle = document.createElement("div");
  dphTitle.setAttribute("class", "dph-title");
  dpHead.appendChild(dphTitle);
  var dphtButton = document.createElement("button");
  dphtButton.setAttribute("type", "button");
  dphtButton.innerText = titleText;
  dphtButton.onclick = titleClick;
  dphTitle.appendChild(dphtButton);
  var dphRight = document.createElement("div");
  dphRight.setAttribute("class", "dph-right");
  dpHead.appendChild(dphRight);
  var dphrButton = document.createElement("button");
  dphrButton.setAttribute("type", "button");
  dphrButton.innerText = ">";
  dphrButton.onclick = rightClick;
  dphRight.appendChild(dphrButton);
};
JendoDatepicker.prototype.loadDPDays = function(content, dYear, dMonth, dDate) {
  content.innerText = "";
  var jdp = this;
  this.addDPHead(content, jendo.lang.monthNames[dMonth] + " " + dYear, function(source) {
    jdp.loadDPMonths(content, dYear, dMonth, dDate);
  }, function(source) {
    var hDate = new Date(dYear, dMonth - 1, dDate);
    jdp.loadDPDays(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
  }, function(source) {
    var hDate = new Date(dYear, dMonth + 1, dDate);
    jdp.loadDPDays(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
  });
  var dpBody = document.createElement("div");
  dpBody.setAttribute("class", "dp-month");
  content.appendChild(dpBody);
  var pWeekday = document.createElement("div");
  pWeekday.setAttribute("class", "dpm-weekdays");
  dpBody.appendChild(pWeekday);
  for (var iwd = this.dpOptions.weekdayStart; iwd < jendo.lang.weekdayNames.length; iwd++) {
    var wd = document.createElement("div");
    wd.setAttribute("class", "dpm-weekday");
    wd.innerText = jendo.lang.weekdayNarrowNames[iwd];
    pWeekday.appendChild(wd);
  }
  for (var iwd = 0; iwd < this.dpOptions.weekdayStart; iwd++) {
    var wd = document.createElement("div");
    wd.setAttribute("class", "dpm-weekday");
    wd.innerText = jendo.lang.weekdayNarrowNames[iwd];
    pWeekday.appendChild(wd);
  }
  var mDays = document.createElement("div");
  mDays.setAttribute("class", "dpm-days");
  dpBody.appendChild(mDays);
  var shiftStart = (new Date(Date.UTC(dYear, dMonth, 1))).getDay() - this.dpOptions.weekdayStart;
  if (shiftStart < 0) {
    shiftStart += 7;
  }
  for (var iss = 0; iss < shiftStart; iss++) {
    var elmDay = document.createElement("button");
    elmDay.setAttribute("type", "button");
    elmDay.setAttribute("class", "dpm-day");
    elmDay.innerText = "";
    mDays.appendChild(elmDay);
  }
  var days = jendo.daysInMonth(dMonth + 1, dYear);
  for (var d = 1; d <= days; d++) {
    var elmDay = document.createElement("button");
    elmDay.setAttribute("type", "button");
    elmDay.setAttribute("class", "dpm-day");
    elmDay.setAttribute("data-day", d);
    elmDay.innerText = d;
    elmDay.onclick = function(source) {
      var dayIndex = source.target.getAttribute("data-day");
      var selDate = new Date(Date.UTC(dYear, dMonth, dayIndex));
      jdp.dElement.dateValue = selDate;
      jdp.jdElement.val(jendo.dateFormatUI(selDate, jdp.format()));
      jdp.hide();
    };
    mDays.appendChild(elmDay);
  }
  var shiftEnd = (shiftStart + days) % 7;
  if (shiftEnd > 0) {
    shiftEnd = 7 - shiftEnd;
  }
  for (var ise = 0; ise < shiftEnd; ise++) {
    var elmDay = document.createElement("button");
    elmDay.setAttribute("type", "button");
    elmDay.setAttribute("class", "dpm-day");
    elmDay.innerText = "";
    mDays.appendChild(elmDay);
  }
};
JendoDatepicker.prototype.loadDPMonths = function(content, dYear, dMonth, dDate) {
  content.innerText = "";
  var jdp = this;
  this.addDPHead(content, dYear, function(source) {
    jdp.loadDPYears(content, dYear, dMonth, dDate);
  }, function(source) {
    var hDate = new Date(dYear - 1, dMonth, dDate);
    jdp.loadDPMonths(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
  }, function(source) {
    var hDate = new Date(dYear + 1, dMonth, dDate);
    jdp.loadDPMonths(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
  });
  var dpBody = document.createElement("div");
  dpBody.setAttribute("class", "dp-months");
  content.appendChild(dpBody);
  for (var i = 0; i < jendo.lang.monthShortNames.length; i++) {
    var ym = document.createElement("button");
    ym.setAttribute("type", "button");
    ym.setAttribute("class", "dpm-month");
    ym.setAttribute("data-month", i);
    ym.innerText = jendo.lang.monthShortNames[i];
    ym.onclick = function(source) {
      var monthIndex = source.target.getAttribute("data-month");
      var mDate = new Date(dYear, monthIndex, dDate);
      jdp.loadDPDays(content, mDate.getFullYear(), mDate.getMonth(), mDate.getDate());
    };
    dpBody.appendChild(ym);
  }
};
JendoDatepicker.prototype.loadDPYears = function(content, dYear, dMonth, dDate) {
  content.innerText = "";
  var jdp = this;
  var strYear = dYear.toString();
  var dYearFrom = dYear - parseInt(strYear[strYear.length - 1]);
  var dYearTo = dYearFrom + 9;
  this.addDPHead(content, dYearFrom + " - " + dYearTo, function(source) {
  }, function(source) {
    var hDate = new Date(dYear - 12, dMonth, dDate);
    jdp.loadDPYears(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
  }, function(source) {
    var hDate = new Date(dYear + 12, dMonth, dDate);
    jdp.loadDPYears(content, hDate.getFullYear(), hDate.getMonth(), hDate.getDate());
  });
  var dpBody = document.createElement("div");
  dpBody.setAttribute("class", "dp-months");
  content.appendChild(dpBody);
  for (var i = -1; i < 11; i++) {
    var iYear = dYearFrom + i;
    var ym = document.createElement("button");
    ym.setAttribute("type", "button");
    if (i == -1 || i == 10) {
      ym.setAttribute("class", "dpm-month dpm-outside");
    } else {
      ym.setAttribute("class", "dpm-month");
    }
    ym.setAttribute("data-year", iYear);
    ym.innerText = iYear;
    ym.onclick = function(source) {
      var yearIndex = source.target.getAttribute("data-year");
      var mDate = new Date(yearIndex, dMonth, dDate);
      jdp.loadDPMonths(content, mDate.getFullYear(), mDate.getMonth(), mDate.getDate());
    };
    dpBody.appendChild(ym);
  }
};
JendoDatepicker.prototype.show = function() {
  if (this.dpOptions.position === "right") {
    this.jdDatepicker.left(this.jdElement.left() + this.jdElement.width() - 228);
  } else {
    this.jdDatepicker.left(this.jdElement.left());
  }
  this.jdDatepicker.show();
  _JendoFuncCloseElement = this.hideElement;
  _JendoLastOpenElement = this.jdDatepicker;
};
jendo.datepicker = function(element, args) {
  if (!jendo.isNull(element)) {
    var targetElement = jendo(element);
    if (targetElement.length > 0) {
      return new JendoDatepicker(targetElement, args);
    }
  }
};
JendoSelector.prototype.datepicker = function(args) {
  if (jendo.isNull(args)) {
    args = {};
  }
  return new JendoDatepicker(this, args);
};
function JendoGridView(element) {
  if (jendo.isNull(element)) {
    return;
  }
  if (element.length === 0) {
    return;
  }
  this._Element = element;
  this._GridView = this._Element.find('[role="grid"]');
  if (this._GridView.length === 0) {
    this._GridView = this._Element.appendTable().role("grid");
  }
  this._GVData = this._GridView.prop("gvdata");
  if (jendo.isNull(this._GVData)) {
    this._GVData = {filterable:{enabled:false, mode:"row"}, columns:[], records:[], sortData:[], sumData:[], filterData:[], allowPaging:true, pageSize:10, pageNo:1, pager:{numeric:true, numericCount:5, prevNext:true, firstLast:true, slider:true, goToPage:true, pageSize:true, pageSizeList:[], info:true}, show:{rowNo:false}, detail:null};
    this._GridView.prop("gvdata", this._GVData);
  }
}
JendoGridView.prototype.data = function(value) {
  if (jendo.isNull(value)) {
    return this;
  }
  if ("filterable" in value) {
    if ("enabled" in value.filterable) {
      this._GVData.filterable.enabled = value.filterable.enabled;
    }
    if ("mode" in value.filterable) {
      this._GVData.filterable.mode = value.filterable.mode;
    }
  }
  if ("show" in value) {
    if ("rowNo" in value.show) {
      this._GVData.show.rowNo = value.show.rowNo;
    }
  }
  if ("allowPaging" in value) {
    this._GVData.allowPaging = value.allowPaging;
  }
  if ("pageSize" in value) {
    this._GVData.pageSize = value.pageSize;
  }
  if ("pager" in value) {
    if ("numeric" in value.pager) {
      this._GVData.pager.numeric = value.pager.goToPage;
    }
    if ("numeric" in value.pager) {
      this._GVData.pager.numeric = value.pager.numeric;
    }
    if ("numericCount" in value.pager) {
      this._GVData.pager.numericCount = value.pager.numericCount;
    }
    if ("prevNext" in value.pager) {
      this._GVData.pager.prevNext = value.pager.prevNext;
    }
    if ("firstLast" in value.pager) {
      this._GVData.pager.firstLast = value.pager.firstLast;
    }
    if ("slider" in value.pager) {
      this._GVData.pager.slider = value.pager.slider;
    }
    if ("goToPage" in value.pager) {
      this._GVData.pager.goToPage = value.pager.goToPage;
    }
    if ("pageSize" in value.pager) {
      this._GVData.pager.pageSize = value.pager.pageSize;
    }
    if ("pageSizeList" in value.pager) {
      this._GVData.pager.pageSizeList = value.pager.pageSizeList;
    }
    if ("info" in value.pager) {
      this._GVData.pager.info = value.pager.info;
    }
  }
  if ("detail" in value) {
    this._GVData.detail = value.detail;
  }
  if ("rowBinding" in value) {
    this._GVData.rowBindingEvent = value.rowBinding;
  }
  if ("rowBound" in value) {
    this._GVData.rowBoundEvent = value.rowBound;
  }
  if ("detailCreate" in value) {
    this._GVData.detailCreateEvent = value.detailCreate;
  }
  if ("detailLoad" in value) {
    this._GVData.detailLoadEvent = value.detailLoad;
  }
  this._GridView.html("");
  this._GVData.columns = [];
  this.filterable(value.filterable);
  this.columns(value.columns);
  this.records(value.records);
  if ("sortData" in value) {
    this.sortData(value.sortData);
  }
  return this;
};
JendoGridView.prototype.filterable = function(value) {
  if (jendo.isNull(value)) {
    return;
  }
  var gFilterable = this._GVData.filterable;
  gFilterable.enabled = jendo.isNull(value.enabled) ? false : value.enabled;
  gFilterable.mode = jendo.isNull(value.mode) ? "row" : value.mode;
};
JendoGridView.prototype.columns = function(gCols) {
  if (!jendo.isNull(gCols)) {
    var self = this;
    var gColumns = this._GVData.columns;
    var gFilterable = this._GVData.filterable;
    var sortData = this._GVData.sortData;
    var filterData = this._GVData.filterData;
    var tHead = this._GridView.appendTHead();
    var tRow = tHead.appendTRow();
    this._GVData.sumData = [];
    var sumData = this._GVData.sumData;
    var tRowFilter = null;
    if (gFilterable.enabled && gFilterable.mode === "row") {
      tRowFilter = tHead.appendTRow();
    }
    if (!jendo.isNull(this._GVData.detail)) {
      tRow.appendTH().text("");
      if (tRowFilter !== null) {
        tRowFilter.appendTH();
      }
    }
    if (this._GVData.show.rowNo) {
      tRow.appendTH().text("#");
      if (tRowFilter !== null) {
        tRowFilter.appendTH();
      }
    }
    for (var i = 0; i < gCols.length; i++) {
      var gCol = gCols[i];
      if ("visible" in gCol && gCol.visible === false) {
        continue;
      }
      if (gCol.footerSum) {
        sumData.push({field:gCol.field, ix:gColumns.length});
        gCol.sum = 0;
      } else {
        if ("sum" in gCol) {
          delete gCol.sum;
        }
      }
      if (jendo.isNullOrEmpty(gCol.caption)) {
        gCol.caption = "";
      }
      var th = tRow.appendTH();
      if ("width" in gCol) {
        th.width(gCol.width);
      }
      th.html(gCol.caption);
      if (!jendo.isNullOrWhiteSpace(gCol.style)) {
        th.addClass(gCol.style);
      }
      if (!jendo.isNull(gCol.field)) {
        var colSortable = "sortable" in gCol ? gCol.sortable : true;
        if (colSortable) {
          th.append('<i class="fas fa-sort-up jd-sort-asc" aria-hidden="true"></i>');
          th.append('<i class="fas fa-sort-down jd-sort-desc" aria-hidden="true"></i>');
        }
        th.data("field", gCol.field);
        if (colSortable) {
          th.click(function(e) {
            var dataField = jendo(e.target).data("field");
            var sortField = sortData.find(function(item) {
              return item.field === dataField;
            });
            if (e.ctrlKey) {
              if (jendo.isNull(sortField)) {
                sortData.push({field:dataField, sort:1});
              } else {
                if (sortField.sort === 1) {
                  sortField.sort = -1;
                } else {
                  if (sortField.sort === -1) {
                    var sortFieldIndex = sortData.findIndex(function(item) {
                      return item.field === dataField;
                    });
                    sortData.splice(sortFieldIndex, 1);
                  } else {
                    sortField.sort = 1;
                  }
                }
              }
            } else {
              if (_.isNull(sortField)) {
                sortData.splice(0, sortData.length);
                sortData.push({field:dataField, sort:1});
              } else {
                sortField.sort = sortField.sort === 1 ? -1 : 1;
              }
            }
            self.sortData();
          });
        }
        gColumns.push(gCol);
        if (tRowFilter !== null) {
          var thrf = tRowFilter.appendTH();
          thrf.appendText("input").data("field", gCol.field).keyUp(function(e) {
            var dField = _(e.target);
            var dFieldName = dField.data("field");
            var dFieldValue = dField.val();
            if (!filterData.find(function(value, index) {
              if (value.field === dFieldName) {
                value.filter = dFieldValue;
                return true;
              } else {
                return false;
              }
            })) {
              filterData.push({field:dFieldName, filter:dFieldValue});
            }
            self.pageNo(1);
            self.refreshRecords();
          });
        }
      } else {
        if (!jendo.isNull(gCol.commands) || !jendo.isNull(gCol.menu)) {
          gColumns.push(gCol);
          if (tRowFilter !== null) {
            tRowFilter.appendTH();
          }
        } else {
          if (!jendo.isNull(gCol.bind)) {
            gColumns.push(gCol);
            if (tRowFilter !== null) {
              var thrb = tRowFilter.appendTH();
            }
          }
        }
      }
    }
  } else {
    return this._GVData.columns;
  }
};
JendoGridView.prototype.column = function(name) {
  for (var i = 0; i < this._GVData.columns.length; i++) {
    var col = this._GVData.columns[i];
    if (col.field === name) {
      return col;
    }
  }
  return null;
};
JendoGridView.prototype.records = function(value) {
  if (!jendo.isNull(value)) {
    var gColumns = this._GVData.columns;
    this._GVData.records = value;
    var tBody = this._GridView.find('[role="rowgroup"]');
    if (tBody.length === 0) {
      tBody = this._GridView.appendTBody().role("rowgroup");
    } else {
      tBody.html("");
    }
    if (!("sortData" in this._GVData)) {
      this.calculateColumnSums(gColumns, value);
      this.appendGRows(tBody, gColumns, value, this._GVData);
      this.appendGPaging(tBody, gColumns, value, this._GVData);
    }
  } else {
    return this._GVData.records;
  }
};
JendoGridView.prototype.calculateColumnSums = function(gColumns, gRecords) {
  var sumDataLen = this._GVData.sumData.length;
  if (sumDataLen > 0) {
    var recLen = gRecords.length;
    var sumData = this._GVData.sumData;
    for (var c = 0; c < sumDataLen; c++) {
      gColumns[sumData[c].ix].sum = 0;
    }
    for (var r = 0; r < recLen; r++) {
      var rec = gRecords[r];
      for (var c = 0; c < sumDataLen; c++) {
        var val = rec[sumData[c].field];
        var fVal = jendo.parseFloat(val, 0);
        gColumns[sumData[c].ix].sum += fVal;
      }
    }
  }
};
JendoGridView.prototype.refreshRecords = function() {
  var gColumns = this._GVData.columns;
  var gRecords = this._GVData.records;
  var gSortData = this._GVData.sortData;
  var gFilterData = this._GVData.filterData;
  var tBody = this._Element.find('tbody[role="rowgroup"]');
  tBody.html("");
  var tHead = this._Element.find("th[data-field]");
  tHead.removeClass("jd-field-sort");
  tHead.removeClass("jd-field-desort");
  for (var i = 0; i < gSortData.length; i++) {
    var fieldData = gSortData[i];
    if (fieldData.sort === -1) {
      this._Element.find('th[data-field="' + fieldData.field + '"]').addClass("jd-field-desort");
    } else {
      this._Element.find('th[data-field="' + fieldData.field + '"]').addClass("jd-field-sort");
    }
  }
  if (gFilterData.length > 0) {
    gRecords = gRecords.filterBy(gFilterData);
  }
  this.calculateColumnSums(gColumns, gRecords);
  this.appendGRows(tBody, gColumns, gRecords, this._GVData);
  this.appendGPaging(tBody, gColumns, gRecords, this._GVData);
};
JendoGridView.prototype.refresh = function() {
  this.refreshRecords();
};
JendoGridView.prototype.appendGRows = function(tBody, gColumns, gRecords, gvData) {
  var firstRowIndex = 0;
  var lastRowIndex = gRecords.length;
  if (this._GVData.allowPaging) {
    firstRowIndex = gvData.pageNo > 1 ? (gvData.pageNo - 1) * this._GVData.pageSize : 0;
    var maxRowIndex = firstRowIndex + this._GVData.pageSize;
    if (lastRowIndex > maxRowIndex) {
      lastRowIndex = maxRowIndex;
    }
  }
  for (var i = firstRowIndex; i < lastRowIndex; i++) {
    var gRow = gRecords[i];
    typeof gRow === "object" ? this.recordBound(i, tBody, gRow, gColumns) : this.recordBound(i, tBody, {}, gColumns);
  }
};
JendoGridView.prototype.appendGPaging = function(tBody, gColumns, gRecords, gvData) {
  if (this._GVData.sumData.length > 0) {
    var tRow = tBody.appendTRow();
    tRow.addClass("jd-sum");
    if (!jendo.isNull(this._GVData.detail)) {
      var jdDtlBtn = tRow.appendTD();
    }
    if (this._GVData.show.rowNo) {
      tRow.appendTD();
    }
    for (var f = 0; f < gColumns.length; f++) {
      var gCol = gColumns[f];
      var td = tRow.appendTD();
      var tdValue = this.formatRecordValue(gCol.sum, gCol);
      if ("align" in gCol) {
        td.style("text-align", gCol.align);
      }
      td.html(tdValue);
    }
  }
  if (gvData.allowPaging) {
    var thisJGV = this;
    var tRow = tBody.appendTRow().addClass("jd-pager");
    var td = tRow.appendTD();
    var countColumns = gColumns.length;
    if (!jendo.isNull(gvData.detail)) {
      countColumns += 1;
    }
    if (gvData.show.rowNo) {
      countColumns += 1;
    }
    var countPageTmp = gRecords.length / gvData.pageSize;
    gvData.countPage = Math.round(countPageTmp, 0);
    if (countPageTmp > gvData.countPage) {
      gvData.countPage += 1;
    }
    td.attr("colspan", countColumns);
    var pager = gvData.pager;
    if (pager.firstLast) {
      var btnFirst = td.appendButton().addClass("jd-pager-btn").click(function(e) {
        thisJGV.pageFirst();
      });
      btnFirst.appendI().attr("class", "fas fa-angle-double-left").attr("aria-hidden", true);
      if (gvData.pageNo === 1) {
        btnFirst.attr("disabled", "");
      }
    }
    if (pager.prevNext) {
      var btnPrev = td.appendButton().addClass("jd-pager-btn").click(function(e) {
        thisJGV.pagePrev();
      });
      btnPrev.appendI().attr("class", "fas fa-angle-left").attr("aria-hidden", true);
      if (gvData.pageNo === 1) {
        btnPrev.attr("disabled", "");
      }
    }
    if (pager.numeric) {
      var pageNoFrom = gvData.pageNo - parseInt(pager.numericCount / 2);
      if (pageNoFrom < 1) {
        pageNoFrom = 1;
      }
      var pageNoTo = pageNoFrom + pager.numericCount - 1;
      if (pageNoTo > gvData.countPage) {
        pageNoTo = gvData.countPage;
        pageNoFrom = pageNoTo - pager.numericCount + 1;
        if (pageNoFrom < 1) {
          pageNoFrom = 1;
        }
      }
      for (var i = pageNoFrom; i <= pageNoTo; i++) {
        var jdBtn = td.appendButton(i).addClass("jd-pager-btn").click(function(e) {
          thisJGV.pageNo(e.data.pageNo);
        }, {pageNo:i});
        if (gvData.pageNo === i) {
          jdBtn.addClass("jd-pager-active");
        }
      }
    }
    if (pager.prevNext) {
      var btnNext = td.appendButton().addClass("jd-pager-btn").click(function(e) {
        thisJGV.pageNext();
      });
      btnNext.appendI().attr("class", "fas fa-angle-right").attr("aria-hidden", true);
      if (gvData.pageNo === gvData.countPage) {
        btnNext.attr("disabled", "");
      }
    }
    if (pager.firstLast) {
      var btnLast = td.appendButton().addClass("jd-pager-btn").click(function(e) {
        thisJGV.pageLast();
      });
      btnLast.appendI().attr("class", "fas fa-angle-double-right").attr("aria-hidden", true);
      if (gvData.pageNo === gvData.countPage) {
        btnLast.attr("disabled", "");
      }
    }
    if (pager.goToPage) {
      var pnlGTP = td.appendDiv().addClass("jd-pager-panel");
      pnlGTP.appendSpan().text("Page:");
      var textGTP = pnlGTP.appendText();
      pnlGTP.appendSpan().text("of " + gvData.countPage);
      pnlGTP.appendButton("Go").addClass("jd-pager-btn").click(function(e) {
        var goToPage = parseInt(textGTP.val());
        if (goToPage > 0) {
          thisJGV.pageNo(textGTP.val());
        }
      });
    }
    if (pager.pageSize) {
      var pnlPS = td.appendDiv().addClass("jd-pager-panel");
      pnlPS.appendSpan().text("Page size:");
      if (Array.isArray(pager.pageSizeList) && pager.pageSizeList.length > 0) {
      } else {
        var textPS = pnlPS.appendText().val(gvData.pageSize);
        pnlPS.appendButton("Change").addClass("jd-pager-btn").click(function() {
          console.log("Change", textPS.val());
        });
      }
    }
  }
};
JendoGridView.prototype.recordBound = function(rowIndex, tBody, gRow, gColumns) {
  var self = this;
  if (!jendo.isNull(this._GVData.rowBindingEvent)) {
    this._GVData.rowBindingEvent({ix:rowIndex, data:gRow});
  }
  var isAlt = rowIndex % 2;
  var tRow = tBody.appendTRow();
  if (isAlt === 1) {
    tRow.addClass("jd-alt");
  }
  if (!jendo.isNull(this._GVData.detail)) {
    var jdDtlBtn = tRow.appendTD().appendButton();
    jdDtlBtn.class("jd-grid-btn-dtl").text("\u2b9e");
    var colCount = 1 + (this._GVData.show.rowNo ? 1 : 0) + gColumns.length;
    jdDtlBtn.click(function() {
      var jdDtlRow = tBody.find('[data-dtl="' + rowIndex + '"]');
      var is_show = jdDtlBtn.prop("is_show");
      if (is_show) {
        jdDtlBtn.prop("is_show", false);
        jdDtlBtn.text("\u2b9e");
        jdDtlRow.hide();
      } else {
        jdDtlBtn.prop("is_show", true);
        jdDtlBtn.text("\u2b9f");
        if (jdDtlRow.length == 0) {
          var dtlRow = document.createElement("tr");
          jdDtlRow = _(dtlRow);
          jdDtlRow.class("jd-dtl");
          jdDtlRow.attr("data-dtl", rowIndex);
          tBody.insertAfter(dtlRow, tRow);
          var jdDtlCell = jdDtlRow.appendTD();
          jdDtlCell.attr("colspan", colCount);
          var jdGrid = jdDtlCell.appendDiv().attr("data-grid", rowIndex);
          var grid = new JendoGridView(jdGrid);
          grid.data(self._GVData.detail);
          if (!jendo.isNull(self._GVData.detailCreateEvent)) {
            self._GVData.detailCreateEvent({ix:rowIndex, data:gRow, row:tRow, grid:grid});
          }
          if (!jendo.isNull(self._GVData.detailLoadEvent)) {
            self._GVData.detailLoadEvent({ix:rowIndex, data:gRow, row:tRow, grid:grid});
          }
        } else {
          jdDtlRow.attr("style", "");
          if (!jendo.isNull(self._GVData.detailLoadEvent)) {
            var grid = _.gridView(jdDtlRow.find('[data-grid="' + rowIndex + '"]'));
            self._GVData.detailLoadEvent({ix:rowIndex, data:gRow, row:tRow, grid:grid});
          }
        }
      }
    });
  }
  if (this._GVData.show.rowNo) {
    tRow.appendTD().text(rowIndex + 1 + ".");
  }
  for (var f = 0; f < gColumns.length; f++) {
    var gCol = gColumns[f];
    var td = tRow.appendTD();
    if (!jendo.isNullOrWhiteSpace(gCol.style)) {
      td.addClass(gCol.style);
    }
    if (!jendo.isNull(gCol.field)) {
      var tdValue = this.formatRecordValue(gRow[gCol.field], gCol);
      if ("align" in gCol) {
        td.style("text-align", gCol.align);
      }
      td.html(tdValue);
    } else {
      if (!jendo.isNull(gCol.commands)) {
        for (var c = 0; c < gCol.commands.length; c++) {
          var cmd = gCol.commands[c];
          var cmdValue = {rowIndex:rowIndex, colIndex:f, isCreate:true};
          if (!jendo.isNull(cmd.fields)) {
            for (var cf = 0; cf < cmd.fields.length; cf++) {
              var fieldName = cmd.fields[cf];
              cmdValue[fieldName] = gRow[fieldName];
            }
          }
          if (!jendo.isNull(cmd.create)) {
            cmd.create(td, cmdValue);
          }
          if (cmdValue.isCreate) {
            var btn = td.appendButton(cmd.caption);
            btn.class("jd-grid-btn-link");
            if (!jendo.isNullOrWhiteSpace(cmd.style)) {
              btn.addClass(cmd.style);
            }
            if (!jendo.isNullOrWhiteSpace(cmd.command)) {
              btn.attr("data-command", cmd.command);
            }
            if (!jendo.isNull(cmd.click)) {
              btn.click(this.clickCommand(cmd.click, cmdValue));
            }
          }
          if (!jendo.isNull(cmd.bound)) {
            cmd.bound(btn, cmdValue);
          }
        }
      } else {
        if (!jendo.isNull(gCol.menu)) {
          var tdMenu = td.appendDiv().class("jd-grid-menu");
          tdMenu.appendButton().html("&#9776;").class("jd-grid-mbtn");
          var tdItems = tdMenu.appendDiv().class("jd-grid-mitems");
          if ("position" in gCol && gCol.position.toLowerCase() === "right") {
            tdItems.addClass("jd-grid-mi-right");
          } else {
            tdItems.addClass("jd-grid-mi-left");
          }
          for (var c = 0; c < gCol.menu.length; c++) {
            var mi = gCol.menu[c];
            var miValue = {rowIndex:rowIndex, colIndex:f, isCreate:true};
            if (!jendo.isNull(mi.fields)) {
              for (var cf = 0; cf < mi.fields.length; cf++) {
                var fieldName = mi.fields[cf];
                miValue[fieldName] = gRow[fieldName];
              }
            }
            if (!jendo.isNull(mi.create)) {
              mi.create(tdItems, miValue);
            }
            if (miValue.isCreate) {
              var btn = tdItems.appendButton(mi.caption);
              btn.class("jd-grid-mitem");
              if (!jendo.isNullOrWhiteSpace(mi.style)) {
                btn.addClass(mi.style);
              }
              if (!jendo.isNullOrWhiteSpace(mi.command)) {
                btn.attr("data-command", mi.command);
              }
              if (!jendo.isNull(mi.click)) {
                btn.click(this.clickCommand(mi.click, miValue));
              }
            }
            if (!jendo.isNull(mi.bound)) {
              mi.bound(btn, miValue);
            }
          }
        } else {
          if (typeof gCol.bind === "function") {
            var tdFuncVal = gCol.bind({rowIndex:rowIndex, colIndex:f, row:gRow});
            var jdBItem = td.html(tdFuncVal);
            if (typeof gCol.binded === "function") {
              gCol.binded({rowIndex:rowIndex, colIndex:f, row:gRow, jdItem:jdBItem});
            }
          }
        }
      }
    }
  }
  if (!jendo.isNull(this._GVData.rowBoundEvent)) {
    this._GVData.rowBoundEvent({ix:rowIndex, data:gRow, row:tRow});
  }
};
JendoGridView.prototype.formatRecordValue = function(cValue, gCol) {
  if ("dateFormat" in gCol) {
    var tdVDate = Date.parse(cValue);
    if (!isNaN(tdVDate)) {
      return jendo.dateFormatUI(new Date(tdVDate), gCol.dateFormat);
    } else {
      return cValue;
    }
  } else {
    if ("numberFixed" in gCol) {
      var tdVNum = jendo.parseFloat(cValue, NaN);
      if (!isNaN(tdVNum)) {
        return tdVNum.toFixed(gCol.numberFixed);
      } else {
        return cValue;
      }
    } else {
      return cValue;
    }
  }
};
JendoGridView.prototype.clickCommand = function(commandClick, e) {
  return function(event) {
    commandClick(event, e);
  };
};
JendoGridView.prototype.sortData = function(value) {
  var sortData = this._GVData.sortData;
  if (!jendo.isNull(value)) {
    sortData.splice(0, sortData.length);
    for (var i = 0; i < value.length; i++) {
      sortData.push(value[i]);
    }
  }
  this._GVData.records.sortBy(sortData);
  this.refreshRecords();
};
JendoGridView.prototype.pageNo = function(value) {
  if (jendo.isNullOrWhiteSpace(value)) {
    return this._GVData.pageNo;
  } else {
    var gvData = this._GVData;
    if (value > 0 && value <= gvData.countPage) {
      gvData.pageNo = value;
      this.refreshRecords();
    }
  }
};
JendoGridView.prototype.pagePrev = function() {
  var gvData = this._GVData;
  this.pageNo(gvData.pageNo - 1);
};
JendoGridView.prototype.pageNext = function() {
  var gvData = this._GVData;
  this.pageNo(gvData.pageNo + 1);
};
JendoGridView.prototype.pageFirst = function() {
  this.pageNo(1);
};
JendoGridView.prototype.pageLast = function() {
  var gvData = this._GVData;
  this.pageNo(gvData.countPage);
};
JendoGridView.prototype.wait = function(value) {
  this.html('<div class="jd-grid-wait"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span>' + value + "</span></div>");
};
JendoGridView.prototype.text = function(value) {
  this.html('<div class="jd-grid-text">' + value + "</div>");
};
JendoGridView.prototype.html = function(value) {
  var tBody = this._GridView.find('[role="rowgroup"]');
  if (tBody.length === 0) {
    tBody = this._GridView.appendTBody().role("rowgroup");
  }
  var countColumns = this._GVData.columns.length;
  if (!jendo.isNull(this._GVData.detail)) {
    countColumns += 1;
  }
  if (this._GVData.show.rowNo) {
    countColumns += 1;
  }
  tBody.html('<tr><td colspan="' + countColumns + '">' + value + "</td></tr>");
};
JendoGridView.prototype.saveAsExcel = function(args) {
  if (jendo.isNull(args)) {
    args = {};
  }
  var fName = jendo.isNullOrWhiteSpace(args.file) ? "document.xls" : args.file + ".xls";
  var fTitle = jendo.isNullOrWhiteSpace(args.title) ? "Document" : args.title;
  var fHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>" + fTitle + "</title></head><body>" + this.dataToHtml(args) + "</body></html>";
  var fUrl = "data:application/vnd.ms-excel;charset=utf-8," + encodeURIComponent(fHtml);
  var downloadLink = document.createElement("a");
  document.body.appendChild(downloadLink);
  if (navigator.msSaveOrOpenBlob) {
    var blob = new Blob(["\ufeff", fHtml], {type:"application/vnd.ms-excel"});
    navigator.msSaveOrOpenBlob(blob, fName);
  } else {
    downloadLink.href = fUrl;
    downloadLink.download = fName;
    downloadLink.click();
  }
  document.body.removeChild(downloadLink);
};
JendoGridView.prototype.dataToHtml = function(args) {
  var hRow = "";
  if (this._GVData.show.rowNo) {
    hRow += "<th>#</th>";
  }
  var gCols = this._GVData.columns;
  for (var i = 0; i < gCols.length; i++) {
    var gCol = gCols[i];
    if (!jendo.isNullOrEmpty(gCol.field)) {
      hRow += "<th>" + (jendo.isNullOrEmpty(gCol.caption) ? "" : gCol.caption) + "</th>";
    }
  }
  var hTHRows = "<tr>" + hRow + "</tr>";
  var hTBRows = "";
  var gRecs = this._GVData.records;
  for (var r = 0; r < gRecs.length; r++) {
    var gRec = gRecs[r];
    hRow = "";
    if (this._GVData.show.rowNo) {
      hRow += "<td>" + (r + 1) + "</td>";
    }
    for (var c = 0; c < gCols.length; c++) {
      var gRCol = gCols[c];
      if (!jendo.isNullOrEmpty(gRCol.field)) {
        var val = gRec[gRCol.field];
        if (jendo.isNull(val)) {
          hRow += "<td></td>";
        } else {
          hRow += "<td>" + this.formatRecordValue(val, gRCol) + "</td>";
        }
      }
    }
    hTBRows += "<tr>" + hRow + "</tr>";
  }
  var hTitle = "";
  if (!jendo.isNullOrWhiteSpace(args.title)) {
    hTitle = "<h2>" + args.title + "</h2>";
  }
  return hTitle + '<table border="1"><thead>' + hTHRows + "</thead><tbody>" + hTBRows + "</tbody></table>";
};
jendo.gridView = function(element) {
  if (!jendo.isNull(element)) {
    var targetElement = jendo(element);
    if (targetElement.length > 0) {
      return new JendoGridView(targetElement);
    }
  }
};
function JendoQRCodeReader(element) {
  this._QRCodeReader = jendo(element);
  this._Height = this._QRCodeReader.height();
  this._Width = this._QRCodeReader.width();
  this._OnSuccess = null;
  this._OnError = null;
  this._OnVideoError = null;
}
JendoQRCodeReader.prototype.height = function(value) {
  if (jendo.isNull(value)) {
    return this._Height;
  } else {
    this._Height = value;
    return this;
  }
};
JendoQRCodeReader.prototype.width = function(value) {
  if (jendo.isNull(value)) {
    return this._Width;
  } else {
    this._Width = value;
    return this;
  }
};
JendoQRCodeReader.prototype.onSuccess = function(handler) {
  this._OnSuccess = handler;
  return this;
};
JendoQRCodeReader.prototype.onError = function(handler) {
  this._OnError = handler;
  return this;
};
JendoQRCodeReader.prototype.onVideoError = function(handler) {
  this._OnVideoError = handler;
  return this;
};
JendoQRCodeReader.prototype.start = function() {
  var qrcReader = this._QRCodeReader;
  var height = this._Height == null ? 250 : this._Height;
  var width = this._Width == null ? 300 : this._Width;
  var qrcodeSuccessEvent = this._OnSuccess;
  var qrcodeErrorEvent = this._OnError;
  var qrcodeVideoErrorEvent = this._OnVideoError;
  var vidElem = this._QRCodeReader.find("#qr-video");
  if (vidElem.length == 0) {
    vidElem = this._QRCodeReader.appendElement("video").attr("id", "qr-video").height(height).width(width);
  } else {
    vidElem.show();
  }
  var canvasElem = this._QRCodeReader.find("#qr-canvas");
  if (canvasElem.length == 0) {
    canvasElem = this._QRCodeReader.appendElement("canvas").height(height - 2).width(width - 2).attr("id", "qr-canvas").style("display:none;");
  } else {
    canvasElem.hide();
  }
  var video = vidElem.first();
  var canvas = canvasElem.first();
  var context = canvas.getContext("2d");
  var qrcodeScan = function() {
    var mediaStream = qrcReader.data("stream");
    if (!jendo.isNull(mediaStream)) {
      context.drawImage(video, 0, 0, 307, 250);
      try {
        qrcode.decode();
      } catch (e) {
        if (!jendo.isNull(qrcodeErrorEvent)) {
          qrcodeErrorEvent(e, mediaStream);
        }
      }
      setTimeout(qrcodeScan, 500);
    }
  };
  jendo.navigator.getUserMedia({video:true}, function(stream) {
    window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
    video.src = window.URL && window.URL.createObjectURL(stream) || stream;
    qrcReader.data("stream", stream);
    video.play();
    setTimeout(qrcodeScan, 1000);
  }, function(error) {
    qrcodeVideoErrorEvent(error, qrcReader.data("stream"));
  });
  qrcode.callback = function(result) {
    if (!jendo.isNull(qrcodeSuccessEvent)) {
      qrcodeSuccessEvent(result, qrcReader.data("stream"));
    }
  };
};
JendoQRCodeReader.prototype.stop = function() {
  var stream = this._QRCodeReader.data("stream");
  if (!jendo.isNull(stream)) {
    stream.getVideoTracks().forEach(function(videoTrack) {
      videoTrack.stop();
    });
    this._QRCodeReader.dataRemove("stream");
  }
};
JendoQRCodeReader.prototype.files = function(value) {
  var qrcReader = this._QRCodeReader;
  var height = this._Height == null ? 250 : this._Height;
  var width = this._Width == null ? 300 : this._Width;
  var qrcodeSuccessEvent = this._OnSuccess;
  var qrcodeErrorEvent = this._OnError;
  var qrcodeVideoErrorEvent = this._OnVideoError;
  var vidElem = this._QRCodeReader.find("#qr-video");
  if (vidElem.length > 0) {
    vidElem.hide();
  }
  var canvasElem = this._QRCodeReader.find("#qr-canvas");
  if (canvasElem.length == 0) {
    canvasElem = this._QRCodeReader.appendElement("canvas").height(height - 2).width(width - 2).attr("id", "qr-canvas");
  } else {
    canvasElem.show();
  }
  var video = vidElem.first();
  var canvas = canvasElem.first();
  var context = canvas.getContext("2d");
  qrcode.callback = function(result) {
    if (!jendo.isNull(qrcodeSuccessEvent)) {
      qrcodeSuccessEvent(result, qrcReader.data("stream"));
    }
  };
  var img = new Image;
  img.onload = function() {
    img.width = context.canvas.width;
    img.height = context.canvas.height;
    context.drawImage(img, 0, 0, context.canvas.width, context.canvas.height);
  };
  for (var i = 0; i < value.length; i++) {
    var reader = new FileReader;
    reader.onload = function(imgFile) {
      return function(imgReader) {
        try {
          qrcode.decode(imgReader.target.result);
          img.src = imgReader.target.result;
        } catch (e) {
          if (!jendo.isNull(qrcodeErrorEvent)) {
            qrcodeErrorEvent(e, mediaStream);
          }
        }
      };
    }(value[i]);
    reader.readAsDataURL(value[i]);
  }
};
jendo.QRCodeReader = function(element) {
  return new JendoQRCodeReader(element);
};
function JendoToolbar(element, options) {
  this.jdElement = element;
  if (jendo.isNull(options)) {
    return this;
  } else {
    if (!jendo.isNull(options.menu)) {
      var jdTBMenu = this.jdElement.appendDiv().class("jd-toolbar jd-toolbar-default");
      if ("style" in options) {
        jdTBMenu.addClass(options.style);
      }
      this.loadMenu(jdTBMenu, options);
    } else {
      if (!jendo.isNull(options.tabs)) {
        var tBar = this.jdElement.appendDiv().class("jd-toolbar jd-toolbar-default");
        var tabMenu = tBar.appendUL().class("jd-tb-tabs");
        var selTab = null;
        for (var t = 0; t < options.tabs.length; t++) {
          var menuItem = options.tabs[t];
          var tabItemMenu = this.loadMenu(tBar, menuItem);
          tabItemMenu.hide();
          var menuBtn = tabMenu.appendLI().appendButton().class("jd-tb-button").text(menuItem.title).click(function(e) {
            var tiBtn = jendo(e.target);
            tiBtn.parent().parent().find("button").removeClass("jd-tb-active");
            tiBtn.addClass("jd-tb-active");
            var tiMenu = e.data.tabMenu;
            tiMenu.parent().find(".jd-tb-menu").hide();
            tiMenu.show();
          }, {tabMenu:tabItemMenu});
          menuBtn.prop("menu", tabItemMenu);
          if (menuItem.selected || selTab === null) {
            selTab = menuBtn;
          }
        }
        if (selTab !== null) {
          selTab.addClass("jd-tb-active");
          selTab.prop("menu").show();
        }
        return this;
      }
    }
  }
}
JendoToolbar.prototype.loadMenu = function(jdTab, tbMenu) {
  var jdMenu = jdTab.appendUL().class("jd-tb-menu");
  for (var i = 0; i < tbMenu.menu.length; i++) {
    var item = tbMenu.menu[i];
    var jdMBtn = jdMenu.appendLI().appendButton().class("jd-tb-button");
    if ("tooltip" in item) {
      jdMBtn.attr("title", item.tooltip);
    }
    if ("glyphs" in item) {
      jdMBtn.appendI().class(item.glyphs).attr("aria-hidden", true);
    }
    if ("title" in item) {
      jdMBtn.appendSamp().text(item.title);
    }
    if ("click" in item) {
      jdMBtn.click(item.click);
    }
  }
  return jdMenu;
};
jendo.toolbar = function(element, options) {
  return new JendoToolbar(jendo(element), options);
};
function JendoAutocomplete(args) {
  var self = this;
  this.jdElement = args.element;
  this.isInit = this.jdElement.prop("jd_init") === "1";
  if (this.isInit) {
    if ("handle" in args) {
      this.handle = args.handle;
    }
    if ("caseSensitive" in args) {
      this.caseSensitive = args.caseSensitive === true;
    }
    if ("autoFocus" in args) {
      this.autoFocus = args.autoFocus === true;
    }
    if ("select" in args) {
      this.eventSelect = args.select;
    }
    if ("open" in args) {
      this.eventOpen = args.open;
    }
    if ("close" in args) {
      this.eventClose = args.close;
    }
    if ("change" in args) {
      this.eventChange = args.change;
    }
  } else {
    this.handle = args.handle;
    this.jdList = null;
    this.caseSensitive = args.caseSensitive === true;
    this.autoFocus = args.autoFocus === true;
    this.selItem = null;
    this.selIndex = -1;
    this.selData = "";
    this.eventSelect = args.select;
    this.eventOpen = args.open;
    this.eventClose = args.close;
    this.eventChange = args.change;
    this.jdElement.attr("autocomplete", "off");
    this.jdElement.focus(function(e) {
      self.createList(e.target);
    });
    this.jdElement.keyDown(function(e) {
      if (e.keyCode === 9) {
        self.close();
      }
    });
    this.jdElement.keyUp(function(e) {
      if (e.keyCode === 38 || e.keyCode === 37) {
        if (self.jdList) {
          var jdPChlds = self.jdList.children();
          var prvIndex = self.selIndex - 1;
          self.itemActive(jdPChlds, prvIndex);
        }
      } else {
        if (e.keyCode === 40 || e.keyCode === 39) {
          if (self.jdList) {
            var jdNChlds = self.jdList.children();
            var nxtIndex = self.selIndex + 1;
            self.itemActive(jdNChlds, nxtIndex);
          }
        } else {
          if (e.keyCode === 13) {
            if (self.jdList && self.selItem) {
              self.item(self.selItem.prop("jd-item"));
              self.close();
              e.stopImmediatePropagation();
            }
          } else {
            if (e.keyCode === 27) {
              self.close();
            } else {
              if (e.keyCode === 9) {
                self.close();
              } else {
                self.createList(e.target);
                var term = self.jdElement.val();
                var srcType = typeof args.source;
                if (srcType === "function") {
                  args.source({term:term}, function(data) {
                    self.loadList(data, term);
                  });
                } else {
                  if (Array.isArray(args.source)) {
                    self.loadList(args.source, term);
                  }
                }
              }
            }
          }
        }
      }
    });
    this.jdElement.blur(function(e) {
      var dItem = self.jdElement.prop("jd-item");
      if (jendo.isNull(dItem)) {
        dItem = "";
      }
      self.invokeChangeEvent(dItem);
    });
    document.addEventListener("click", function(e) {
      if (!self.jdElement.isContains(e.target)) {
        self.close();
      }
    }, true);
    if (!jendo.isNull(args.create)) {
      args.create({type:"autocompletecreate", target:this.jdElement, timeStamp:(new Date).getTime()});
    }
    var term = this.jdElement.val();
    if (!jendo.isNullOrEmpty(term)) {
      var srcType = typeof args.source;
      if (srcType === "function") {
        args.source({term:term}, function(data) {
          var si = self.getFirstItem(data, term);
          self.item(si);
          self.selData = si;
        });
      } else {
        if (Array.isArray(args.source)) {
          var si = self.getFirstItem(args.source, term);
          self.item(si);
          self.selData = si;
        }
      }
    }
    this.jdElement.prop("jd_init", "1");
  }
}
JendoAutocomplete.prototype.createList = function(target) {
  if (jendo.isNull(this.jdList)) {
    var jdTarget = jendo(target);
    var trTop = 0;
    var trLeft = 0;
    var trWidth = jdTarget.offsetWidth();
    var jdHandle = null;
    if (!jendo.isNull(this.handle)) {
      jdHandle = jendo(this.handle);
      var rect = target.getBoundingClientRect();
      trTop = rect.bottom - jdHandle.top() + window.scrollY;
      trLeft = rect.left - jdHandle.left() - window.scrollX;
      trWidth = rect.width;
    } else {
      jdHandle = jendo(document.body);
      var rectT = target.getBoundingClientRect();
      trTop = rectT.top + rectT.height + window.scrollY;
      trLeft = rectT.left - window.scrollX;
    }
    this.jdList = jdHandle.appendUL();
    this.jdList.class("jd-autocomplete");
    this.jdList.attr("tabindex", 0);
    this.jdList.top(trTop);
    this.jdList.left(trLeft);
    this.jdList.style("width", trWidth + "px");
  }
};
JendoAutocomplete.prototype.loadList = function(data, term) {
  if (this.jdList) {
    this.jdList.html("");
    this.valueClear();
    this.jdList.show();
    if (!jendo.isNull(this.eventOpen)) {
      this.eventOpen({type:"autocompleteopen", target:this.jdElement, timeStamp:(new Date).getTime()});
    }
    var termVl = this.caseSensitive ? term : term.toLowerCase();
    for (var i = 0; i < data.length; i++) {
      var row = data[i];
      var rowType = typeof row;
      if (rowType === "string") {
        var rowVl = this.caseSensitive ? row : row.toLowerCase();
        if (rowVl.indexOf(termVl) > -1) {
          this.appendLItem(row, row, {label:row, value:row});
        }
      } else {
        if (rowType === "object") {
          var labelVl = this.caseSensitive ? row.label : row.label.toLowerCase();
          if (labelVl.indexOf(termVl) > -1) {
            this.appendLItem(row.label, row.value, row);
          }
        } else {
          console.log(rowType);
        }
      }
    }
    if (this.autoFocus) {
      this.itemActive(this.jdList.children(), 0);
    }
  }
};
JendoAutocomplete.prototype.getFirstItem = function(data, term) {
  var termVl = this.caseSensitive ? term : term.toLowerCase();
  for (var i = 0; i < data.length; i++) {
    var row = data[i];
    var rowType = typeof row;
    if (rowType === "string") {
      var rowVl = this.caseSensitive ? row : row.toLowerCase();
      if (rowVl.indexOf(termVl) > -1) {
        return {label:row, value:row};
      }
    } else {
      if (rowType === "object") {
        var labelVl = this.caseSensitive ? row.label : row.label.toLowerCase();
        if (labelVl.indexOf(termVl) > -1) {
          return row;
        }
      } else {
        console.log(rowType);
      }
    }
  }
  return null;
};
JendoAutocomplete.prototype.appendLItem = function(label, value, data) {
  var self = this;
  var li = this.jdList.appendLI();
  li.class("jd-ac-item");
  var liText = li.appendDiv().class("jd-ac-text").mouseEnter(function(e) {
    jendo(e.target).addClass("jd-ac-active");
  }).mouseLeave(function(e) {
    jendo(e.target).removeClass("jd-ac-active");
  }).mouseDown(function(e) {
    var jdTarget = jendo(e.target);
    var dItem = jdTarget.prop("jd-item");
    if (jendo.isNull(dItem)) {
      dItem = "";
    }
    self.item(dItem);
    self.close();
    self.invokeChangeEvent(dItem);
  });
  liText.text(label);
  liText.prop("jd-item", data);
};
JendoAutocomplete.prototype.item = function(data) {
  if (data) {
    this.jdElement.val(data.label);
    this.jdElement.attr("jd-value", data.value);
    this.jdElement.prop("jd-item", data);
    if (!jendo.isNull(this.eventSelect)) {
      this.eventSelect({type:"autocompleteselect", target:this.jdElement, timeStamp:(new Date).getTime()}, data);
    }
  } else {
    return this.jdElement.prop("jd-item");
  }
};
JendoAutocomplete.prototype.itemClear = function() {
  this.jdElement.val("");
  this.jdElement.attr("jd-value", "");
  this.jdElement.prop("jd-item", "");
  this.selItem = null;
  this.selIndex = -1;
};
JendoAutocomplete.prototype.valueClear = function() {
  this.jdElement.attr("jd-value", "");
  this.jdElement.prop("jd-item", "");
  this.selItem = null;
  this.selIndex = -1;
};
JendoAutocomplete.prototype.itemActive = function(jdChlds, idx) {
  if (idx >= 0 && idx < jdChlds.length) {
    if (this.selItem) {
      this.selItem.removeClass("jd-ac-active");
      this.selItem = null;
    }
    var jdLI = jendo(jdChlds.item(idx));
    this.selIndex = idx;
    this.selItem = jendo(jdLI.children().first());
    this.selItem.addClass("jd-ac-active");
  }
};
JendoAutocomplete.prototype.close = function() {
  if (this.jdList) {
    this.jdList.hide();
    if (!jendo.isNull(this.eventClose)) {
      this.eventClose({type:"autocompleteclose", target:this.jdElement, timeStamp:(new Date).getTime()});
    }
    this.jdList = null;
  }
};
JendoAutocomplete.prototype.invokeChangeEvent = function(dItem) {
  var evItem = null;
  if (dItem === "") {
    var elVal = this.jdElement.val();
    if (this.selData !== elVal) {
      this.selData = elVal;
      evItem = {value:-1, label:elVal};
    }
  } else {
    if (this.selData !== dItem) {
      this.selData = dItem;
      evItem = dItem;
    }
  }
  if (!jendo.isNull(this.eventChange) && !jendo.isNull(evItem)) {
    this.eventChange({type:"autocompletechange", target:this.jdElement, timeStamp:(new Date).getTime()}, evItem);
  }
};
JendoSelector.prototype.autocomplete = function(args) {
  if (jendo.isNull(args)) {
    args = {element:this};
  } else {
    args.element = this;
  }
  return new JendoAutocomplete(args);
};
function JendoCombobox(args) {
  var self = this;
  this.jdElement = args.element;
  if (args.disabled) {
    this.jdElement.prop("disabled", args.disabled);
  }
  if (!jendo.isNull(args.change)) {
    this.jdElement.change(function(e) {
      if (e.target.options && e.target.selectedIndex) {
        var selOpt = e.target.options[e.target.selectedIndex];
        var item = {label:selOpt.innerText, value:selOpt.value};
        args.change(e, item);
      } else {
        args.change(e);
      }
    });
  }
  if (args.source) {
    this.clear();
    var srcType = typeof args.source;
    if (srcType === "function") {
      args.source(function(data) {
        if (Array.isArray(data)) {
          var dLen = data.length;
          for (var di = 0; di < dLen; di++) {
            self.add(data[di]);
          }
          if (args.value) {
            self.jdElement.val(args.value);
          }
        }
      });
    } else {
      if (Array.isArray(args.source)) {
        var aLen = args.source.length;
        for (var ai = 0; ai < aLen; ai++) {
          self.add(args.source[ai]);
        }
        if (args.value) {
          self.jdElement.val(args.value);
        }
      }
    }
  }
  if (!jendo.isNull(args.create)) {
    args.create({type:"comboboxcreate", target:this.jdElement, timeStamp:(new Date).getTime()});
  }
}
JendoCombobox.prototype.clear = function() {
  this.jdElement.html("");
};
JendoCombobox.prototype.add = function(item) {
  var iType = typeof item;
  if (iType === "string") {
    this.jdElement.appendElement("option").text(item);
  } else {
    var idEl = this.jdElement.appendElement("option");
    idEl.text(item.label);
    idEl.val(item.value);
  }
};
JendoCombobox.prototype.disable = function() {
  this.jdElement.prop("disabled", true);
};
JendoCombobox.prototype.enable = function() {
  this.jdElement.prop("disabled", false);
};
JendoSelector.prototype.combobox = function(args) {
  if (jendo.isNull(args)) {
    args = {element:this};
  } else {
    args.element = this;
  }
  return new JendoCombobox(args);
};
function JendoTabPage(element, args) {
  this.jdTP = jendo(element);
  this.jdTabs = jendo(this.jdTP.find(".jd-tabs").first());
  if (this.jdTabs.length === 0) {
    this.jdTabs = this.jdTP.appendDiv();
    this.jdTabs.addClass("jd-tabs");
  }
  var isinit = this.jdTP.data("isinit");
  if (isinit !== "1") {
    var self = this;
    this.jdTP.data("isinit", 1);
    this.jdTP.data("tcount", this.jdTabs.children().length);
    var jdScroll = this.jdTP.appendDiv().addClass("jd-tab-scroll");
    var scrollLeft = null;
    var scrollRight = null;
    jdScroll.appendButton().attr("data-tabscroll", "left").html("&#10094;").click(function(e) {
      self.jdTabs.first().scrollLeft -= 40;
    }).mouseDown(function(e) {
      var jdSTabs = self.jdTabs.first();
      scrollLeft = setInterval(function() {
        jdSTabs.scrollLeft -= 40;
      }, 200);
    }).mouseUp(function(e) {
      if (scrollLeft !== null) {
        clearInterval(scrollLeft);
        scrollLeft = null;
      }
    }).mouseLeave(function(e) {
      if (scrollLeft !== null) {
        clearInterval(scrollLeft);
        scrollLeft = null;
      }
    });
    jdScroll.appendButton().attr("data-tabscroll", "right").html("&#10095;").click(function(e) {
      self.jdTabs.first().scrollLeft += 40;
    }).mouseDown(function(e) {
      var jdSTabs = self.jdTabs.first();
      scrollRight = setInterval(function() {
        jdSTabs.scrollLeft += 40;
      }, 200);
    }).mouseUp(function(e) {
      if (scrollRight !== null) {
        clearInterval(scrollRight);
        scrollRight = null;
      }
    }).mouseLeave(function(e) {
      if (scrollRight !== null) {
        clearInterval(scrollRight);
        scrollRight = null;
      }
    });
    window.addEventListener("resize", function(e) {
      self.showScrollBtn();
    });
    this.jdTabs.find("[data-tab]").click(function(e) {
      var jdTab = jendo(e.target);
      self.selectTab(self, jdTab);
    });
    if ("selected" in args) {
      var jdTabS = this.jdTabs.find('[data-tab="' + args.selected + '"]');
      this.selectTab(this, jdTabS);
    } else {
      var jdTabF = jendo(this.jdTabs.find("[data-tab]").first());
      this.selectTab(this, jdTabF);
    }
  }
  return this;
}
JendoTabPage.prototype.selectTab = function(self, jdTab) {
  var tab = jdTab.attr("data-tab");
  if (tab) {
    self.jdTabs.find("[data-tab]").removeClass("jd-active");
    jdTab.addClass("jd-active");
    self.jdTP.find(":scope > [data-tabpage]").hide();
    self.jdTP.find('[data-tabpage="' + tab + '"]').show();
  }
};
JendoTabPage.prototype.select = function(tab) {
  this.selectTab(this, this.jdTP.find('[data-tab="' + tab + '"]'));
};
JendoTabPage.prototype.exist = function(tab) {
  return this.jdTP.find('[data-tab="' + tab + '"]').length > 0;
};
JendoTabPage.prototype.append = function(args) {
  var tCount = parseInt(this.jdTP.data("tcount"));
  if (!("tab" in args)) {
    args.tab = "tab" + tCount;
  }
  var jdTBtn = this.jdTabs.find('[data-tab="' + args.tab + '"]');
  if (jdTBtn.length === 0) {
    var closeTab = false;
    if ("closeTab" in args) {
      closeTab = args.closeTab;
    }
    this.jdTP.data("tcount", tCount + 1);
    var jdTab = this.jdTabs.appendDiv().addClass("jd-tab");
    jdTBtn = jdTab.appendButton(args.title).addClass("jd-tab-btn").attr("data-tab", args.tab);
    var jdTPage = this.jdTP.appendDiv().addClass("jd-page").attr("data-tabpage", args.tab);
    if ("page" in args) {
      jdTPage.html(args.page);
    } else {
      if ("pageUrl" in args) {
        jdTPage.load(args.pageUrl);
      }
    }
    var self = this;
    if (closeTab) {
      jdTBtn.addClass("jd-tab-rpadd");
      var jdTClose = jdTab.appendButton().html("&#215;").addClass("jd-tab-close");
      jdTClose.click(function(e) {
        var jdCTab = jdTBtn.parent();
        if (jdTBtn.hasClass("jd-active")) {
          var jdTList = self.jdTabs.children();
          var cTab = jdCTab.first();
          var jdNTab = null;
          for (var i = 0; i < jdTList.items.length; i++) {
            if (cTab === jdTList.items[i]) {
              if (i + 1 < jdTList.items.length) {
                jdNTab = jendo(jdTList.items[i + 1]);
              } else {
                if (i - 1 >= 0) {
                  jdNTab = jendo(jdTList.items[i - 1]);
                }
              }
            }
          }
          if (jdNTab !== null) {
            self.selectTab(self, jdNTab.find("[data-tab]"));
          }
        }
        jdCTab.remove();
        jdTPage.remove();
        self.showScrollBtn();
      });
    }
    jdTBtn.click(function(e) {
      self.selectTab(self, jdTBtn);
    });
    this.showScrollBtn();
  }
  if (args.select) {
    this.selectTab(this, jdTBtn);
  }
  return jdTBtn;
};
JendoTabPage.prototype.showScrollBtn = function() {
  var tabs = this.jdTabs.first();
  if (tabs.scrollWidth > tabs.clientWidth) {
    this.jdTP.find("[data-tabscroll]").css("display", "inline-block");
  } else {
    this.jdTP.find("[data-tabscroll]").hide();
  }
};
jendo.tabpage = function(element, args) {
  return new JendoTabPage(element, args ? args : {});
};
function JendoTimepicker(element, options) {
  this.jdElement = element;
  this.dElement = this.jdElement.first();
  if (!this.isInit()) {
    var self = this;
    if (jendo.isNull(this.dElement.options)) {
      this.dElement.options = {format:"HH:mm", showButton:false, position:"left"};
      if (!jendo.isNull(options)) {
        if (typeof options.format === "string") {
          this.dElement.options.format = options.format;
        }
        if (typeof options.showButton === "boolean") {
          this.dElement.options.showButton = options.showButton;
        }
        if (typeof options.position === "string") {
          this.dElement.options.position = options.position;
        }
      }
    }
    this.jdTime = {};
    this.jdElement.on("change", function(e) {
      self.validate();
    });
    this.jdElement.on("blur", function(e) {
      if (e.target.value == self.getBlankValue()) {
        e.target.value = "";
      }
    });
    this.jdElement.on("click", function(e) {
      var tVal = e.target.value;
      var caretPos = e.target.selectionStart;
      if (tVal == "") {
        tVal = self.getBlankValue();
        e.target.value = tVal;
      }
      if (caretPos == 0) {
        var dIx = tVal.indexOf(" ", caretPos - 2);
        if (dIx > 0) {
          e.target.setSelectionRange(0, 1);
        } else {
          e.target.setSelectionRange(0, 2);
        }
      }
      if (caretPos >= tVal.length) {
        e.target.setSelectionRange(tVal.length - 2, tVal.length);
      } else {
        var selIx = tVal.indexOf(":", caretPos - 2);
        if (selIx > caretPos) {
          var dIx = tVal.indexOf(" ", caretPos - 2);
          if (dIx > 0) {
            if (dIx < caretPos) {
              e.target.setSelectionRange(dIx + 1, dIx + 3);
            } else {
              e.target.setSelectionRange(0, 1);
            }
          } else {
            e.target.setSelectionRange(0, 2);
          }
        } else {
          e.target.setSelectionRange(selIx + 1, selIx + 3);
        }
      }
    });
    var tpParent = this.jdElement.parent();
    this.dTimepicker = this.createTimepicker(this.jdElement.attrId() + "_timepicker");
    tpParent.insertAfter(this.dTimepicker, this.jdElement);
    var jdTimepicker = jendo(this.dTimepicker);
    if (this.dElement.options.showButton) {
      var dBtn = document.createElement("button");
      dBtn.setAttribute("type", "button");
      dBtn.setAttribute("class", "jd-timepicker-btn");
      tpParent.insertAfter(dBtn, this.jdElement);
      self.dTimepicker.srcbtn = dBtn;
      var dBtnI = document.createElement("i");
      dBtnI.setAttribute("class", "far fa-clock");
      dBtnI.setAttribute("aria-hidden", "true");
      dBtn.appendChild(dBtnI);
      jendo(dBtn).click(function(source) {
        if (jdTimepicker.isVisible()) {
          jdTimepicker.hide();
        } else {
          if (self.dElement.options.position === "right") {
            jdTimepicker.left(self.jdElement.left() + self.jdElement.width() - 228);
          } else {
            jdTimepicker.left(self.jdElement.left());
          }
          var time = self.validate();
          self.jdTime.day.innerText = time.day;
          self.jdTime.hour.innerText = time.hour;
          self.jdTime.minute.innerText = time.minute;
          self.jdTime.second.innerText = time.second;
          jdTimepicker.show();
          _JendoFuncCloseElement = self.hideElement;
          _JendoLastOpenElement = jdTimepicker;
        }
      });
    }
    this.isInit(true);
  }
}
JendoTimepicker.prototype.isInit = function(value) {
  if (jendo.isNull(value)) {
    var isInit = this.dElement.jdp_IsInit;
    return jendo.isNull(isInit) ? false : isInit;
  } else {
    this.dElement.jdp_IsInit = value;
    return this;
  }
};
JendoTimepicker.prototype.hide = function() {
  jendo(this.dTimepicker).hide();
  _JendoFuncCloseElement = null;
  _JendoLastOpenElement = null;
};
JendoTimepicker.prototype.hideElement = function(source) {
  var dTimepicker = _JendoLastOpenElement.first();
  if (!_JendoLastOpenElement.isContains(source) && !dTimepicker.srcbtn.contains(source)) {
    _JendoLastOpenElement.hide();
    _JendoFuncCloseElement = null;
    _JendoLastOpenElement = null;
  }
};
JendoTimepicker.prototype.format = function(pattern) {
  if (jendo.isNullOrWhiteSpace(pattern)) {
    return this.dElement.options.format;
  } else {
    this.dElement.options.format = pattern;
    this.validate();
    return this;
  }
};
JendoTimepicker.prototype.getBlankValue = function() {
  return this.dElement.options.format.replace(/d/g, "_").replace(/H/g, "_").replace(/m/g, "_").replace(/s/g, "_").replace(/h/g, "_");
};
JendoTimepicker.prototype.validate = function() {
  var tVal = this.jdElement.val().trim();
  if (tVal == "") {
    return {day:0, hour:0, minute:0, second:0};
  }
  var time = tVal.split(":");
  var tDay = 0;
  var tHour = 0;
  var tHour12 = 0;
  var tMinute = 0;
  var tSecond = 0;
  if (time.length >= 1) {
    var dTime = time[0].split(" ");
    if (dTime.length === 1) {
      tHour = parseInt(time[0]);
    } else {
      tDay = parseInt(dTime[0]);
      tHour = parseInt(dTime[1]);
      if (isNaN(tDay)) {
        tDay = 0;
      }
    }
    if (isNaN(tHour)) {
      tHour = 0;
    } else {
      if (tHour < 0 || tHour > 24) {
        tHour = 0;
      }
    }
    tHour12 = tHour > 12 ? tHour - 12 : tHour;
  }
  if (time.length >= 2) {
    tMinute = parseInt(time[1]);
    if (isNaN(tMinute)) {
      tMinute = 0;
    } else {
      if (tMinute < 0 || tMinute > 60) {
        tMinute = 0;
      }
    }
  }
  if (time.length >= 3) {
    tSecond = parseInt(time[2]);
    if (isNaN(tSecond)) {
      tSecond = 0;
    } else {
      if (tSecond < 0 || tSecond > 60) {
        tSecond = 0;
      }
    }
  }
  this.jdElement.val(this.dElement.options.format.replace(/d/g, tDay.toString()).replace(/HH/g, tHour.toString().padStart(2, "0")).replace(/H/g, tHour.toString()).replace(/mm/g, tMinute.toString().padStart(2, "0")).replace(/m/g, tMinute.toString()).replace(/ss/g, tSecond.toString().padStart(2, "0")).replace(/s/g, tSecond.toString()).replace(/hh/g, tHour12.toString().padStart(2, "0")).replace(/h/g, tHour12.toString()));
  return {day:tDay, hour:tHour, minute:tMinute, second:tSecond};
};
JendoTimepicker.prototype.setTextTime = function() {
  var tDay = parseInt(this.jdTime.day.innerText);
  var tHour = parseInt(this.jdTime.hour.innerText);
  var tHour12 = tHour > 12 ? tHour - 12 : tHour;
  var tMinute = parseInt(this.jdTime.minute.innerText);
  var tSecond = parseInt(this.jdTime.second.innerText);
  this.jdElement.val(this.dElement.options.format.replace(/d/g, tDay.toString()).replace(/HH/g, tHour.toString().padStart(2, "0")).replace(/H/g, tHour.toString()).replace(/mm/g, tMinute.toString().padStart(2, "0")).replace(/m/g, tMinute.toString()).replace(/ss/g, tSecond.toString().padStart(2, "0")).replace(/s/g, tSecond.toString()).replace(/hh/g, tHour12.toString().padStart(2, "0")).replace(/h/g, tHour12.toString()));
};
JendoTimepicker.prototype.positionLeft = function() {
  this.dElement.options.position = "left";
  return this;
};
JendoTimepicker.prototype.positionRight = function() {
  this.dElement.options.position = "right";
  return this;
};
JendoTimepicker.prototype.createTimepicker = function(timepickerId) {
  var tp = document.createElement("div");
  tp.setAttribute("class", "jd-timepicker");
  tp.id = timepickerId;
  var tpContent = document.createElement("div");
  tpContent.setAttribute("class", "jd-tp-content");
  tp.appendChild(tpContent);
  this.jdTime.day = this.appendTimeItem({parent:tpContent, separator:" ", value:"0", max:999});
  this.jdTime.hour = this.appendTimeItem({parent:tpContent, separator:" ", value:"0", max:24});
  this.jdTime.minute = this.appendTimeItem({parent:tpContent, separator:":", value:"0", max:60});
  this.jdTime.second = this.appendTimeItem({parent:tpContent, separator:":", value:"0", max:60});
  return tp;
};
JendoTimepicker.prototype.appendTimeItem = function(args) {
  var self = this;
  var tpItem = document.createElement("div");
  tpItem.setAttribute("class", "jd-tp-col");
  args.parent.appendChild(tpItem);
  var tpISep = document.createElement("div");
  tpISep.setAttribute("class", "jd-tp-sep");
  tpISep.innerText = args.separator;
  tpItem.appendChild(tpISep);
  var tpIVal = document.createElement("div");
  tpIVal.setAttribute("class", "jd-tp-col");
  args.parent.appendChild(tpIVal);
  var tpValUp = document.createElement("button");
  tpValUp.setAttribute("type", "button");
  tpValUp.setAttribute("class", "jd-tp-btn");
  tpValUp.innerHTML = "&#11165;";
  tpIVal.appendChild(tpValUp);
  var tpVal = document.createElement("div");
  tpVal.setAttribute("class", "jd-tp-val");
  tpVal.innerText = args.value;
  tpIVal.appendChild(tpVal);
  var tpValDown = document.createElement("button");
  tpValDown.setAttribute("type", "button");
  tpValDown.setAttribute("class", "jd-tp-btn");
  tpValDown.innerHTML = "&#11167;";
  tpIVal.appendChild(tpValDown);
  jendo(tpValUp).click(function(e) {
    var jdVal = e.data.jdVal;
    var iVal = parseInt(jdVal.text()) + 1;
    jdVal.text(iVal >= e.data.maxVal ? iVal - e.data.maxVal : iVal);
    self.setTextTime();
  }, {jdVal:jendo(tpVal), maxVal:args.max});
  jendo(tpValDown).click(function(e) {
    var jdVal = e.data.jdVal;
    var iVal = parseInt(jdVal.text()) - 1;
    jdVal.text(iVal < 0 ? e.data.maxVal + iVal : iVal);
    self.setTextTime();
  }, {jdVal:jendo(tpVal), maxVal:args.max});
  return tpVal;
};
jendo.timepicker = function(element, args) {
  if (!jendo.isNull(element)) {
    var targetElement = jendo(element);
    if (targetElement.length > 0) {
      return new JendoTimepicker(targetElement, args);
    }
  }
};
JendoSelector.prototype.timepicker = function(args) {
  if (jendo.isNull(args)) {
    args = {};
  }
  return new JendoTimepicker(this, args);
};
function JendoPopupMenu(jdBtn, args) {
  this.jdMenuUL = null;
  var jdMenu = null;
  if (typeof args === "string") {
    jdMenu = jendo(args);
    this.jdMenuUL = jdMenu.find("ul");
    if (this.jdMenuUL.length === 0) {
      this.jdMenuUL = jdMenu.appendUL();
    }
  } else {
    if (typeof args === "object") {
      jdMenu = jendo(args.menu);
      if (jdMenu.length === 0) {
        jdMenu = jendo(document.body).appendDiv();
      }
      this.jdMenuUL = jdMenu.find("ul");
      if (this.jdMenuUL.length === 0) {
        this.jdMenuUL = jdMenu.appendUL();
      }
      this.load(args.data);
    }
  }
  if (!jdMenu.hasClass("jd-popup-menu")) {
    jdMenu.addClass("jd-popup-menu");
    jdMenu.addClass("jd-popup-menu-default");
  }
  this.jdMBtn = jdBtn;
  var self = this;
  jendo(document.body).click(function(e) {
    if (jdMenu) {
      var jdMenuBtn = jdMenu.first().btn;
      if (jdMenuBtn && jdMenu.isVisible()) {
        if (jdBtn === jdMenuBtn && !jdMenuBtn.isContains(e.target)) {
          if (self._dispatchHideEvent) {
            self._dispatchHideEvent();
          }
          jdMenu.hide();
        }
      }
    }
  });
  jdBtn.click(function() {
    if (jdMenu) {
      if (jdMenu.isVisible()) {
        if (jdBtn === jdMenu.first().btn) {
          if (self._dispatchHideEvent) {
            self._dispatchHideEvent();
          }
          jdMenu.hide();
        }
      } else {
        if (self._dispatchShowEvent) {
          self._dispatchShowEvent();
        }
        if (typeof args === "object" && "init" in args && !jdMenu.isInit) {
          args.init(self, self.jdMenuUL);
          jdMenu.isInit = true;
        }
        jdMenu.first().btn = jdBtn;
        jdMenu.top(jdBtn.top() + jdBtn.offsetHeight());
        jdMenu.left(jdBtn.left());
        jdMenu.show();
      }
    }
  });
}
JendoPopupMenu.prototype.load = function(data) {
  if (Array.isArray(data) && this.jdMenuUL) {
    for (var i = 0; i < data.length; i++) {
      this.append(data[i]);
    }
  }
};
JendoPopupMenu.prototype.append = function(data) {
  if (this.jdMenuUL) {
    var jdLI = this.jdMenuUL.appendLI();
    if ("link" in data) {
      return jdLI.appendA(data.link, data.text);
    } else {
      var self = this;
      return jdLI.appendButton().html(data.text).click(function() {
        document.dispatchEvent(new CustomEvent("popupmenu.navigate", {detail:{src:self.jdMBtn, args:data.args}}));
      });
    }
  }
};
JendoPopupMenu.prototype.navigate = function(func) {
  var self = this;
  document.addEventListener("popupmenu.navigate", function(e) {
    if (e.detail.src === self.jdMBtn) {
      func(e, e.detail.args);
    }
  }, false);
  return this;
};
JendoPopupMenu.prototype._dispatchShowEvent = null;
JendoPopupMenu.prototype.showEvent = function(func) {
  this._dispatchShowEvent = function() {
    document.dispatchEvent(new CustomEvent("popup.menu.show", null));
  };
  document.addEventListener("popup.menu.show", func, false);
  return this;
};
JendoPopupMenu.prototype._dispatchHideEvent = null;
JendoPopupMenu.prototype.hideEvent = function(func) {
  this._dispatchHideEvent = function() {
    document.dispatchEvent(new CustomEvent("popup.menu.hide", null));
  };
  document.addEventListener("popup.menu.hide", func, false);
  return this;
};
JendoSelector.prototype.popupmenu = JendoSelector.prototype.popupMenu = function(args) {
  return new JendoPopupMenu(this, args);
};
jendo.popupmenu = jendo.popupMenu = function(el, args) {
  return new JendoPopupMenu(jendo(el), args);
};
function JendoContextMenu(jdBtn, args) {
  if (jdBtn.length === 0) {
    return this;
  }
  var menuAlign = "left";
  this.jdMBtn = jdBtn;
  var self = this;
  var jdMenuPanel = jdBtn.parent();
  this.jdMenuUL = null;
  var jdMenu = null;
  if (typeof args === "string") {
    jdMenu = jendo(args);
    if (jdMenu.length === 0) {
      jdMenu = jdMenuPanel.appendDiv();
    }
    this.jdMenuUL = jdMenu.find("ul");
    if (this.jdMenuUL.length === 0) {
      this.jdMenuUL = jdMenu.appendUL();
    }
  } else {
    if (typeof args === "object") {
      jdMenu = jendo(args.menu);
      if (jdMenu.length === 0) {
        jdMenu = jdMenuPanel.appendDiv();
      }
      this.jdMenuUL = jdMenu.find("ul");
      if (this.jdMenuUL.length === 0) {
        this.jdMenuUL = jdMenu.appendUL();
      }
      if ("align" in args) {
        if (args.align === "left" || args.align === "right") {
          menuAlign = args.align;
        }
      }
      this.load(args.data);
    }
  }
  if (!jdMenuPanel.hasClass("jd-context-menu")) {
    jdMenuPanel.addClass("jd-context-menu");
    jdMenuPanel.addClass("jd-context-menu-default");
  }
  if (!jdBtn.hasClass("jd-menu-btn")) {
    jdBtn.addClass("jd-menu-btn");
  }
  if (!jdMenu.hasClass("jd-menu-items")) {
    jdMenu.addClass("jd-menu-items");
  }
  if (menuAlign === "right") {
    jdMenu.addClass("jd-menu-right");
  }
  jendo(document.body).click(function(e) {
    if (jdMenu) {
      var jdMenuBtn = jdMenu.first().btn;
      if (jdMenuBtn && jdMenu.isVisible()) {
        if (jdBtn === jdMenuBtn && !jdMenuBtn.isContains(e.target)) {
          if (self._dispatchHideEvent) {
            self._dispatchHideEvent();
          }
          jdMenu.hide();
        }
      }
    }
  });
  jdBtn.click(function() {
    if (jdMenu) {
      if (jdMenu.isVisible()) {
        if (jdBtn === jdMenu.first().btn) {
          if (self._dispatchHideEvent) {
            self._dispatchHideEvent();
          }
          jdMenu.hide();
        }
      } else {
        if (self._dispatchShowEvent) {
          self._dispatchShowEvent();
        }
        if (typeof args === "object" && "init" in args && !jdMenu.isInit) {
          args.init(self, self.jdMenuUL);
          jdMenu.isInit = true;
        }
        jdMenu.first().btn = jdBtn;
        jdMenu.show();
      }
    }
  });
}
JendoContextMenu.prototype.load = function(data) {
  if (Array.isArray(data) && this.jdMenuUL) {
    for (var i = 0; i < data.length; i++) {
      this.append(data[i]);
    }
  }
};
JendoContextMenu.prototype.append = function(data) {
  if (this.jdMenuUL) {
    var jdLI = this.jdMenuUL.appendLI();
    if ("link" in data) {
      return jdLI.appendA(data.link, data.text);
    } else {
      var self = this;
      var jdBtn = jdLI.appendButton().html(data.text);
      jdBtn.click(function() {
        document.dispatchEvent(new CustomEvent("contextmenu.navigate", {detail:{src:self.jdMBtn, args:data.args}}));
      });
      if ("click" in data) {
        jdBtn.click(function() {
          data.click(jdBtn, data.args);
        });
      }
      return jdBtn;
    }
  }
};
JendoContextMenu.prototype.navigate = function(func) {
  var self = this;
  document.addEventListener("contextmenu.navigate", function(e) {
    if (e.detail.src === self.jdMBtn) {
      func(e, e.detail.args);
    }
  }, false);
  return this;
};
JendoContextMenu.prototype._dispatchShowEvent = null;
JendoContextMenu.prototype.showEvent = function(func) {
  this._dispatchShowEvent = function() {
    document.dispatchEvent(new CustomEvent("context.menu.show", null));
  };
  document.addEventListener("context.menu.show", func, false);
  return this;
};
JendoContextMenu.prototype._dispatchHideEvent = null;
JendoContextMenu.prototype.hideEvent = function(func) {
  this._dispatchHideEvent = function() {
    document.dispatchEvent(new CustomEvent("context.menu.hide", null));
  };
  document.addEventListener("context.menu.hide", func, false);
  return this;
};
JendoSelector.prototype.contextmenu = JendoSelector.prototype.contextMenu = function(args) {
  return new JendoContextMenu(this, args);
};
jendo.contextmenu = jendo.contextMenu = function(el, args) {
  return new JendoContextMenu(jendo(el), args);
};
function JendoKeyboard(el, args) {
  this.kbOptions = args;
  if (jendo.isNull(args)) {
    this.kbOptions = {showButton:true, theme:"default"};
  } else {
    if (!("showButton" in this.kbOptions)) {
      this.kbOptions.showButton = true;
    }
    if (!("theme" in this.kbOptions)) {
      this.kbOptions.theme = "default";
    }
  }
  this.themeClass = this.kbOptions.theme === "default" ? "jd-keyboard-default" : this.kbOptions.theme === "light" ? "jd-keyboard-light" : this.kbOptions.theme;
  this.characteKeys = [{lang:"EN", keys:[["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"], ["a", "s", "d", "f", "g", "h", "j", "k", "l"], ["z", "x", "c", "v", "b", "n", "m"]]}, {lang:"BG", keys:[["\u044f", "\u0432", "\u0435", "\u0440", "\u0442", "\u044a", "\u0443", "\u0438", "\u043e", "\u043f", "\u0447"], ["\u0430", "\u0441", "\u0434", "\u0444", "\u0433", "\u0445", "\u0439", "\u043a", "\u043b", "\u0448", "\u0449"], ["\u0437", "\u044c", "\u0446", "\u0436", "\u0431", "\u043d", "\u043c", "\u044e"]]}];
  this.characteKeysIndex = 0;
  this.symbolKeys = [["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "[", "]"], ["@", "#", "$", "%", "_", "&", "-", "+", "(", ")", "<", ">"], [",", "/", "*", '"', "'", ": ", "; ", "!", "?", "\\"]];
  this.jdEnterKey = null;
  this.jdKeyboard = this.createCharsKB(args);
  this.jdSelItem = null;
  this.isCapsLock = false;
  this.isShift = false;
  this.jdEl = el;
  var jdInput = this.jdEl.find("input");
  for (var i = 0; i < jdInput.items.length; i++) {
    this.attach(jdInput.items[i]);
  }
  var jdTTarea = this.jdEl.find("textarea");
  for (var t = 0; t < jdTTarea.items.length; t++) {
    this.attach(jdTTarea.items[t]);
  }
  var self = this;
  document.addEventListener("click", function(e) {
    if (self.jdSelItem !== null && self.jdSelItem.first() !== e.target) {
      if (self.jdKeyboard !== null && !self.jdKeyboard.isContains(e.target)) {
        if (e.target.getAttribute("data-role") !== "keyboard") {
          self.jdSelItem = null;
          self.hide();
        }
      }
    }
  }, true);
}
JendoKeyboard.prototype.attach = function(el) {
  var self = this;
  var jdItem = jendo(el);
  if (this.kbOptions.showButton) {
    var jdParent = jdItem.parent();
    var jdBtn = jendo.createButton().attr("data-role", "keyboard");
    jdBtn.append(jendo.createElement("i").attr("class", "far fa-keyboard").attr("aria-hidden", "true").attr("data-role", "keyboard"));
    jdParent.insertAfter(jdBtn.first(), jdItem);
    jdBtn.click(function(e) {
      if (self.jdSelItem !== null && self.jdSelItem === jdItem) {
        self.hide();
        self.jdSelItem = null;
      } else {
        self.jdSelItem = jdItem;
        jdItem.focus();
        self.show();
      }
    });
  }
  jdItem.focus(function(e) {
    if (self.jdSelItem === null || self.jdSelItem !== jdItem) {
      self.jdSelItem = jdItem;
      self.show();
    }
  });
};
JendoKeyboard.prototype.createCharsKB = function(args) {
  var self = this;
  var jdKBContent = jendo.isNull(this.jdKeyboard) ? jendo(document.body).appendDiv().class("jd-keyboard " + this.themeClass) : this.jdKeyboard.html("");
  var jdKB = jdKBContent.appendDiv().class("jd-keyboard-chars");
  var characteLang = this.characteKeys[this.characteKeysIndex].lang;
  var characteKeys = this.characteKeys[this.characteKeysIndex].keys;
  for (var l = 0; l < characteKeys.length && l < 3; l++) {
    var kbLine = characteKeys[l];
    var jdLine = jdKB.appendDiv().class("jd-keyboard-row");
    if (l === 1) {
      jdLine.appendButton().html("&#x21ea;").class("jd-keyboard-key jd-keyboard-fn").click(function(e) {
        if (self.isCapsLock) {
          self.isCapsLock = false;
          jendo(e.target).html("&#x21ea;");
        } else {
          self.isCapsLock = true;
          jendo(e.target).html("&#x21eb;");
        }
        self.setKBUpperCase();
      });
    } else {
      if (l === 2) {
        jdLine.appendButton().html("&#8679;").class("jd-keyboard-key jd-keyboard-fn").click(function(e) {
          if (self.isShift) {
            self.isShift = false;
          } else {
            self.isShift = true;
          }
          self.setKBUpperCase();
        });
      }
    }
    for (var i = 0; i < kbLine.length; i++) {
      var key = kbLine[i];
      jdLine.appendButton(key).class("jd-keyboard-key jd-keyboard-ch").click(function(e) {
        self.inputItemChar(e.data.key);
      }, {key:key});
    }
    if (l === 2) {
      jdLine.appendButton().html("&#x232B;").class("jd-keyboard-key jd-keyboard-bs").click(function(e) {
        if (self.jdSelItem !== null) {
          self.jdSelItem.focus();
          var dItem = self.jdSelItem.first();
          var itemText = self.jdSelItem.val();
          var pos = dItem.selectionStart - 1;
          self.jdSelItem.val(itemText.substring(0, pos) + itemText.substring(dItem.selectionEnd, itemText.length));
          dItem.selectionStart = pos;
          dItem.selectionEnd = pos;
        }
      });
    }
  }
  var jdEndLine = jdKB.appendDiv().class("jd-keyboard-row");
  jdEndLine.appendButton().html("?123").class("jd-keyboard-key jd-keyboard-knm").click(function(e) {
    self.createSymbolsKB(args);
  });
  jdEndLine.appendButton().html(characteLang).class("jd-keyboard-key jd-keyboard-knm").click(function(e) {
    self.characteKeysIndex += 1;
    if (self.characteKeysIndex >= self.characteKeys.length) {
      self.characteKeysIndex = 0;
    }
    self.createCharsKB(args);
  });
  jdEndLine.appendButton().html("&nbsp;").class("jd-keyboard-key jd-keyboard-space").click(function(e) {
    self.inputItemChar(" ");
  });
  self.jdEnterKey = jdEndLine.appendButton().html("&#x21a9;").class("jd-keyboard-key jd-keyboard-rn").click(function(e) {
    self.hide();
  });
  return jdKBContent;
};
JendoKeyboard.prototype.createSymbolsKB = function(args) {
  var self = this;
  var jdKBContent = jendo.isNull(this.jdKeyboard) ? jendo(document.body).appendDiv().class("jd-keyboard") : this.jdKeyboard.html("");
  var jdKB = jdKBContent.appendDiv().class("jd-keyboard-chars");
  for (var l = 0; l < this.symbolKeys.length && l < 3; l++) {
    var kbLine = this.symbolKeys[l];
    var jdLine = jdKB.appendDiv().class("jd-keyboard-row");
    for (var i = 0; i < kbLine.length; i++) {
      var key = kbLine[i];
      jdLine.appendButton(key).class("jd-keyboard-key jd-keyboard-ch").click(function(e) {
        self.inputItemChar(e.data.key);
      }, {key:key});
    }
    if (l === 2) {
      jdLine.appendButton().html("&#x232B;").class("jd-keyboard-key jd-keyboard-bs").click(function(e) {
        if (self.jdSelItem !== null) {
          self.jdSelItem.focus();
          var dItem = self.jdSelItem.first();
          var itemText = self.jdSelItem.val();
          var pos = dItem.selectionStart - 1;
          self.jdSelItem.val(itemText.substring(0, pos) + itemText.substring(dItem.selectionEnd, itemText.length));
          dItem.selectionStart = pos;
          dItem.selectionEnd = pos;
        }
      });
    }
  }
  var jdEndLine = jdKB.appendDiv().class("jd-keyboard-row");
  jdEndLine.appendButton().html("ABC").class("jd-keyboard-key jd-keyboard-knm").click(function(e) {
    self.createCharsKB(args);
  });
  jdEndLine.appendButton().html("&nbsp;").class("jd-keyboard-key jd-keyboard-space").click(function(e) {
    self.inputItemChar(" ");
  });
  jdEndLine.appendButton(".").class("jd-keyboard-key jd-keyboard-ch").click(function(e) {
    self.inputItemChar(e.data.key);
  }, {key:"."});
  self.jdEnterKey = jdEndLine.appendButton().html("&#x21a9;").class("jd-keyboard-key jd-keyboard-rn").click(function(e) {
    self.hide();
  });
  return jdKBContent;
};
JendoKeyboard.prototype.inputItemChar = function(key) {
  if (this.jdSelItem !== null) {
    this.jdSelItem.focus();
    var dItem = this.jdSelItem.first();
    var itemText = this.jdSelItem.val();
    var iPos = dItem.selectionStart + 1;
    var iKey = key;
    if (this.isShift) {
      iKey = iKey.toUpperCase();
      this.isShift = false;
      this.setKBUpperCase();
    } else {
      if (this.isCapsLock) {
        iKey = iKey.toUpperCase();
      }
    }
    this.jdSelItem.val(itemText.substring(0, dItem.selectionStart) + iKey + itemText.substring(dItem.selectionEnd, itemText.length));
    dItem.selectionStart = iPos;
    dItem.selectionEnd = iPos;
  }
};
JendoKeyboard.prototype.setKBUpperCase = function() {
  if (this.isShift || this.isCapsLock) {
    this.jdKeyboard.addClass("jd-keyboard-upper");
  } else {
    this.jdKeyboard.removeClass("jd-keyboard-upper");
  }
};
JendoKeyboard.prototype.show = function() {
  if (this.jdKeyboard !== null) {
    this.jdKeyboard.addClass("jd-keyboard-show");
  }
};
JendoKeyboard.prototype.hide = function() {
  if (this.jdKeyboard !== null) {
    this.jdKeyboard.removeClass("jd-keyboard-show");
  }
};
JendoKeyboard.prototype.enterKey = function(func) {
  if (this.jdEnterKey !== null) {
    this.jdEnterKey.click(func);
  }
};
jendo.keyboard = function(element, args) {
  if (!jendo.isNull(element)) {
    var targetElement = jendo(element);
    if (targetElement.length > 0) {
      return new JendoKeyboard(targetElement, args);
    }
  }
};
JendoSelector.prototype.keyboard = function(args) {
  if (jendo.isNull(args)) {
    args = {};
  }
  return new JendoKeyboard(this, args);
};
function JendoInlineList(element, options) {
  this.jdElement = element;
  if (this.jdElement) {
    this.jdList = this.jdElement.find("ul");
    if (this.jdList.length == 0) {
      this.jdList = this.jdElement.appendUL();
    }
  }
}
JendoInlineList.prototype.add = function(text, value) {
  if (this.jdList) {
    var self = this;
    var jdItem = this.jdList.appendLI();
    jdItem.attr("jd-value", value);
    jdItem.appendSpan(text);
    var jdBtn = jdItem.appendButton().html("&times;");
    jdBtn.click(function(e) {
      self.jdList.removeChild(jdItem);
    });
    return jdItem;
  } else {
    return undefined;
  }
};
JendoInlineList.prototype.get = function(index) {
  if (this.jdList) {
    if (index) {
      var ix = parseInt(index);
      if (isNaN(index)) {
        return null;
      } else {
        var cNodes = this.jdList.first().childNodes;
        var eItem = cNodes[ix];
        if (eItem) {
          var jdItem = jendo(eItem);
          return {value:jdItem.attr("jd-value"), text:jdItem.find("span").text()};
        } else {
          return null;
        }
      }
    } else {
      var items = [];
      var cNodes = this.jdList.first().childNodes;
      for (var i = 0; i < cNodes.length; i++) {
        var jdItem = jendo(cNodes[i]);
        items.push({value:jdItem.attr("jd-value"), text:jdItem.find("span").text()});
      }
      return items;
    }
  } else {
    return undefined;
  }
};
JendoInlineList.prototype.getValue = function(index) {
  if (this.jdList) {
    if (index) {
      var ix = parseInt(index);
      if (isNaN(index)) {
        return null;
      } else {
        var cNodes = this.jdList.first().childNodes;
        var eItem = cNodes[ix];
        if (eItem) {
          var jdItem = jendo(eItem);
          return jdItem.attr("jd-value");
        } else {
          return null;
        }
      }
    } else {
      var items = [];
      var cNodes = this.jdList.first().childNodes;
      for (var i = 0; i < cNodes.length; i++) {
        var jdItem = jendo(cNodes[i]);
        var vl = jdItem.attr("jd-value");
        if (!jendo.isNull(vl)) {
          items.push(vl);
        }
      }
      return items;
    }
  } else {
    return undefined;
  }
};
JendoInlineList.prototype.getText = function(index) {
  if (this.jdList) {
    if (index) {
      var ix = parseInt(index);
      if (isNaN(index)) {
        return null;
      } else {
        var cNodes = this.jdList.first().childNodes;
        var eItem = cNodes[ix];
        if (eItem) {
          var jdItem = jendo(eItem);
          return jdItem.find("span").text();
        } else {
          return null;
        }
      }
    } else {
      var items = [];
      var cNodes = this.jdList.first().childNodes;
      for (var i = 0; i < cNodes.length; i++) {
        var jdItem = jendo(cNodes[i]);
        items.push(jdItem.find("span").text());
      }
      return items;
    }
  } else {
    return undefined;
  }
};
JendoInlineList.prototype.clear = function() {
  if (this.jdList) {
    this.jdList.text("");
  }
};
JendoInlineList.prototype.size = function() {
  if (this.jdList) {
    return this.jdList.first().childNodes.length;
  } else {
    return 0;
  }
};
jendo.inlinelist = function(element, args) {
  if (!jendo.isNull(element)) {
    var targetElement = jendo(element);
    if (targetElement.length > 0) {
      return new JendoInlineList(targetElement, args);
    } else {
      return new JendoInlineList(undefined, args);
    }
  } else {
    return new JendoInlineList(undefined, args);
  }
};
JendoSelector.prototype.inlinelist = function(args) {
  if (jendo.isNull(args)) {
    args = {};
  }
  return new JendoInlineList(this, args);
};

