﻿/* BEGIN: Dialog
*/
// JendoDialog
function JendoDialog(element) {
    this.jDialog = jendo(element);
    if (this.jDialog.length === 0) {
        return;
    }
    // Скрива диалога
    // this.jDialog.hide();
    // Добавя роля, ако няма
    if (jendo.isNullOrEmpty(this.jDialog.attr('role'))) {
        this.jDialog.attr('role', 'dialog');
    }
    // Добавя клас, ако няма
    if (jendo.isNullOrEmpty(this.jDialog.attr('class'))) {
        this.jDialog.attr('class', 'jd-dialog');
    }
    // Заглавие на диалога
    this.dialogTitle = 'Jendo Dialog';
    // Model
    this.dialogModel = this.jDialog.prop('dialogModel');
    // Заглавна част?
    this.withTitlebar = true;
    // Заглавна част: Затварящ бутон отляво
    this.withCloseBtnLeft = false;
    // Заглавна част: Затварящ бутон отдясно
    this.withCloseBtnRight = true;
    // Модална форма?
    this.isModalForm = false;
}

// Елемент за преместване
JendoDialog.moveElement = null;
JendoDialog.moveTop = 0;
JendoDialog.moveLeft = 0;
// Позиция на диалога при отваряне
JendoDialog.showPositionTop = 50;

// изчиста заредения диалог
JendoDialog.prototype.clear = function () {
    this.jDialog.html('');
    this.jDialog.prop('srcUrl', '');
    this.dialogModel = null;
    // премахва EventListener
    var el = this.jDialog.first();
    elClone = el.cloneNode(true);
    el.parentNode.replaceChild(elClone, el);
    this.jDialog = jendo(elClone);

    return this;
};

// зарежда диалог от url
JendoDialog.prototype.load = function (args, callback) {
    var dialog = this.jDialog;
    var self = this;
    var url = typeof args === 'object' ? args.url : args;
    // Заредена форма
    var loadUrl = dialog.prop('srcUrl');
    if (loadUrl !== url) {
        // взема диалога
        jendo.get(url)
            .done(function (responseText) {
                dialog.prop('srcUrl', url);
                // зарежда html
                dialog.loadHTML(responseText);
                // Създава модел на диалога
                if (typeof args === 'object' && typeof args.model === 'string') {
                    var objModel = window[args.model]; // взема обекта
                    if (!jendo.isNull(objModel)) {
                        self.dialogModel = new objModel({ dialog: self });
                        if (self.dialogModel) {
                            if (self.dialogModel.onShow) {
                                self.onShow(self.dialogModel.onShow);
                            }
                            if (self.dialogModel.onShown) {
                                self.onShown(self.dialogModel.onShown);
                            }
                            if (self.dialogModel.onHide) {
                                self.onHide(self.dialogModel.onHide);
                            }
                            if (self.dialogModel.onHidden) {
                                self.onHidden(self.dialogModel.onHidden);
                            }
                        }
                        dialog.prop('dialogModel', self.dialogModel);
                    }
                }
                // диалога е зареден
                if (callback) {
                    callback(self);
                }
            })
            .send();
    }
    else {
        // диалога е зареден
        if (callback) {
            callback(self);
        }
    }
};

// Отваря формата
JendoDialog.prototype.show = function (args) {
    if (this.jDialog.length === 0) {
        return;
    }
    // Ако няма параметри на ф-ята
    if (jendo.isNull(args)) {
        args = {};
    }
    else {
        if (!jendo.isNull(args.style)) {
            this.jDialog.class('jd-dialog');
            this.jDialog.addClass(args.style);
        }
    }
    // добавя модела
    if (!jendo.isNull(this.dialogModel)) {
        if ('refresh' in args) {
            this.dialogModel.refresh = args.refresh;
        }
        args.model = this.dialogModel;
    }
    // събитие: show
    this.jDialog.dispatchJEvent('jendo.dialog.show', args);
    // Формата е модална
    if (this.isModalForm) {
        this.jDialog.addClass('jd-dialog-modal');
    }
    // Основното съдържание на диалога
    var jdDocument = this.jDialog.find('[role="document"]');
    // Ако не е открит, създава
    if (jdDocument.length === 0) {
        // Създава панел за съдържанието
        jdDocument = jendo.createDiv()
            .class('jd-dialog-document')
            .attr('role', 'document')
            .attr('tabindex', 0);
        // Добавя елементите от диалога
        jdDocument.append(this.jDialog.children());
        // 
        jdDocument.addClass(this.jDialog.data('class'));
        // Добава елемента за съдържание към диалога
        this.jDialog.append(jdDocument);

        // Натиснат бутон
        this.jDialog.keyPress(function (e) {
            // Esc - Затваря формата
            if (e.keyCode === 27) {
                jendo.dialog(this).hide();
            }
        });
    }

    // Търси по роля
    var jdContent = jdDocument.find('[data-dialog="content"]');
    // Ако не е открит, търси по клас
    if (jdContent.length === 0) {
        jdContent = jdDocument.find('.jd-dialog-content');
        // Ако не е открит, създава
        if (jdContent.length === 0) {
            // Създава панел за съдържанието
            jdContent = jendo.createDiv()
                .class('jd-dialog-content')
                .attr('data-dialog', 'content');
            // Добавя елементите от диалога
            jdContent.append(jdDocument.children());
            // Добава елемента за съдържание към диалога
            jdDocument.append(jdContent);
        }
        // Ако е открит
        else {
            jdContent.attr('data-dialog', 'content');
        }
    }

    // Заглавна част
    if (this.withTitlebar) {
        // Търси по роля
        var jdTitlebar = jdDocument.find('[data-dialog="titlebar"]');
        // Ако не е открит, търси по клас
        if (jdTitlebar.length === 0) {
            jdTitlebar = jdDocument.find('.jd-dialog-titlebar');
            // Ако не е открит, създава
            if (jdTitlebar.length === 0) {
                jdTitlebar = jdDocument.insertDiv()
                    .class('jd-dialog-titlebar')
                    .attr('data-dialog', 'titlebar');
                // Затварящ бутон отляво
                if (this.withCloseBtnLeft) {
                    jdTitlebar.appendElement()
                        .class('jd-dt-button jd-dt-button-left')
                        .appendButton()
                        .attr('data-dialog', 'close')
                        .appendI()
                        .class('fas fa-chevron-left')
                        .attr('aria-hidden', 'true');
                }
                // Заглавие
                jdTitlebar.appendDiv()
                    .class("jd-dt-title")
                    .text((jendo.isNullOrWhiteSpace(args.title) ? this.dialogTitle : args.title));
                // Затварящ бутон отдясно
                if (this.withCloseBtnRight) {
                    jdTitlebar.appendDiv()
                        .class('jd-dt-button jd-dt-button-right')
                        .appendButton()
                        .attr('data-dialog', 'close')
                        .class('jd-btn')
                        .appendI()
                        .class('fas fa-times')
                        .attr('aria-hidden', 'true');
                }
            }
            // Ако е открит
            else {
                jdTitlebar.attr('data-dialog', 'titlebar');
            }
        }
        else {
            // Добавя клас, ако няма
            if (jendo.isNullOrEmpty(jdDocument.attr('class'))) {
                jdDocument.attr('class', 'jd-dialog-titlebar');
            }
            // Променя заглавието
            if (!jendo.isNullOrWhiteSpace(args.title)) {
                jdTitlebar.find(".jd-dt-title").text(args.title);
            }
        }
        // Премества диалога
        var htmlDocument = jendo(document);
        // Премества диалога: begin
        jdTitlebar.mouseDown(function (e) {
            var targetEl = jendo(e.target);
            // Ако не е бутон за затваряне
            if (targetEl.attr('data-dialog') !== 'close') {
                // Търси диалоговия елемент
                var docEl = JendoDialog.findDocument(e.target);
                if (!jendo.isNull(docEl)) {
                    JendoDialog.moveElement = jendo(docEl);
                    JendoDialog.moveTop = e.offsetY + e.target.offsetTop;
                    JendoDialog.moveLeft = e.offsetX + e.target.offsetLeft;
                }
            }
        });
        // Премества диалога: end
        htmlDocument.mouseUp(function (e) {
            JendoDialog.moveElement = null;
            JendoDialog.moveTop = 0;
            JendoDialog.moveLeft = 0;
        });
        // Премества диалога: move
        htmlDocument.mouseMove(function (e) {
            if (JendoDialog.moveElement != null) {
                var pos = { top: 0, left: 0 };
                pos.top = e.clientY - JendoDialog.moveTop;
                if (pos.top < 0) {
                    pos.top = 0;
                }
                pos.left = e.clientX - JendoDialog.moveLeft;
                if (pos.left < 0) {
                    pos.left = 0;
                }
                JendoDialog.moveElement.position(pos);
                JendoDialog.moveElement.prop('position', pos);
            }
        });
    }
    // добавя събития за затваряне по атребут
    jdDocument.find('[data-dialog="close"]')
        .click(function (e) {
            JendoDialog.findDialog(e.target).hide();
        });

    // отваря диалога
    this.jDialog.show();

    // Позиция на диалога
    var pos = jdDocument.prop('position');
    if (jendo.isNull(pos)) {
        if (jendo.screenWidth() == jdDocument.offsetWidth()) {
            pos = {
                top: 0,
                left: 0
            };
        }
        else {
            pos = {
                top: ((jdDocument.height() < jendo.windowHeight()) ? JendoDialog.showPositionTop : 0),
                left: (jendo.windowWidth() / 2) - (jdDocument.width() / 2)
            };
            if (!this.isModalForm) {
                JendoDialog.showPositionTop += 30;
            }
            jdDocument.prop('position', pos);
        }
    }
    jdDocument.position(pos);
    // Преави формата активна
    jdDocument.focus();
    // събитие: shown
    this.jDialog.dispatchJEvent('jendo.dialog.shown', args);
};

// Отваря формата като модална
JendoDialog.prototype.showModal = function (args) {
    this.isModalForm = true;
    this.show(args);
};

// Затваря формата
JendoDialog.prototype.hide = function (args) {
    if (this.jDialog.length === 0) {
        return;
    }
    // Ако няма параметри на ф-ята
    if (jendo.isNull(args)) {
        args = {};
    }
    // събитие: hide
    this.jDialog.dispatchJEvent('jendo.dialog.hide', args);
    // затваря диалога
    this.jDialog.hide();
    // събитие: hidden
    this.jDialog.dispatchJEvent('jendo.dialog.hidden', args);
};

// Събитие: Форма ще се отваря
JendoDialog.prototype.onShow = function (listener) {
    this.jDialog.addJListener('jendo.dialog.show', listener);
    return this;
};

// Събитие: Форма е оворена
JendoDialog.prototype.onShown = function (listener) {
    this.jDialog.addJListener('jendo.dialog.shown', listener);
    return this;
};

// Събитие: Форма ще се затваря
JendoDialog.prototype.onHide = function (listener) {
    this.jDialog.addJListener('jendo.dialog.hide', listener);
    return this;
};

// Събитие: Форма е затворена
JendoDialog.prototype.onHidden = function (listener) {
    this.jDialog.addJListener('jendo.dialog.hidden', listener);
    return this;
};

// дава основния Element на диалога
JendoDialog.findDialog = function (target) {
    var elm = this.findParentElement(target, 'role', 'dialog');
    return new JendoDialog(elm);
};

// дава основния Element на диалога
JendoDialog.findDocument = function (target) {
    if (typeof target === 'undefined') {
        return this.jDialog.find('[role="document"]');
    }
    else {
        return this.findParentElement(target, 'role', 'document');
    }
};

// дава Titlebar Element на диалога
JendoDialog.findTitlebar = function (target) {
    return this.findParentElement(target, 'data-dialog', 'titlebar');
};

// търси parent елемент
JendoDialog.findParentElement = function (target, attrName, attrValue) {
    var element = target.parentElement;
    while (element != null) {
        var elementRole = element.attributes[attrName];
        if (!jendo.isNull(elementRole)
            && elementRole.value.toLowerCase() == attrValue) {
            return element;
        }
        element = element.parentElement;
    }
    return null;
};

jendo.dialog = function (element) {
    return new JendoDialog(element);
};

JendoSelector.prototype.dialog = function (args) {
    var jDlg = new JendoDialog(this);
    if (args.modal === true) {
        jDlg.showModal();
    }
    else {
        jDlg.show();
    }
    return jDlg;
};

// Инициализира бутоните за отваряне на диалог
jendo.dialogInit = function () {
    jendo('[data-toggle="jd-dialog"]').forEach(function (i, item) {
        var jdItem = jendo(item);
        var dTarget = jdItem.data('target');
        // има открита цел
        if (!jendo.isNullOrWhiteSpace(dTarget)) {
            // отваря диалог
            jdItem.click(function (e) {
                jendo.dialog(dTarget).show();
            });
        }
    });
};
/* END: Dialog
*/