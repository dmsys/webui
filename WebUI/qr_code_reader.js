﻿/* BEGIN: QR Code Reader
*/
function JendoQRCodeReader(element) {
    this._QRCodeReader = jendo(element);

    this._Height = this._QRCodeReader.height();
    this._Width = this._QRCodeReader.width();

    this._OnSuccess = null;
    this._OnError = null;
    this._OnVideoError = null;
}

JendoQRCodeReader.prototype.height = function (value) {
    if (jendo.isNull(value)) {
        return this._Height;
    }
    else {
        this._Height = value;
        return this;
    }
}

JendoQRCodeReader.prototype.width = function (value) {
    if (jendo.isNull(value)) {
        return this._Width;
    }
    else {
        this._Width = value;
        return this;
    }
}

JendoQRCodeReader.prototype.onSuccess = function (handler) {
    this._OnSuccess = handler;
    return this;
}

JendoQRCodeReader.prototype.onError = function (handler) {
    this._OnError = handler;
    return this;
}

JendoQRCodeReader.prototype.onVideoError = function (handler) {
    this._OnVideoError = handler;
    return this;
}

JendoQRCodeReader.prototype.start = function () {
    var qrcReader = this._QRCodeReader;
    var height = (this._Height == null) ? 250 : this._Height;
    var width = (this._Width == null) ? 300 : this._Width;
    var qrcodeSuccessEvent = this._OnSuccess;
    var qrcodeErrorEvent = this._OnError;
    var qrcodeVideoErrorEvent = this._OnVideoError;
    // създава video елемента
    var vidElem = this._QRCodeReader.find('#qr-video');
    if (vidElem.length == 0) {
        vidElem = this._QRCodeReader.appendElement('video')
            .attr('id', 'qr-video')
            .height(height)
            .width(width);
    }
    else {
        vidElem.show();
    }
    // създава canvas елемента
    var canvasElem = this._QRCodeReader.find('#qr-canvas');
    if (canvasElem.length == 0) {
        canvasElem = this._QRCodeReader.appendElement('canvas')
            .height(height - 2)
            .width(width - 2)
            .attr('id', 'qr-canvas')
            .style('display:none;');
    }
    else {
        canvasElem.hide();
    }

    var video = vidElem.first();
    var canvas = canvasElem.first();
    var context = canvas.getContext('2d');

    var qrcodeScan = function () {
        var mediaStream = qrcReader.data('stream');

        if (!jendo.isNull(mediaStream)) {
            context.drawImage(video, 0, 0, 307, 250);
            try {
                qrcode.decode();
            } catch (e) {
                if (!jendo.isNull(qrcodeErrorEvent)) {
                    qrcodeErrorEvent(e, mediaStream);
                }
            }
            setTimeout(qrcodeScan, 500);

        }
    };

    jendo.navigator.getUserMedia({
        video: true
    }, function (stream) {
        window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;

        video.src = (window.URL && window.URL.createObjectURL(stream)) || stream;
        qrcReader.data('stream', stream);

        video.play();

        setTimeout(qrcodeScan, 1000);
    }, function (error) {
        qrcodeVideoErrorEvent(error, qrcReader.data('stream'));
    });

    qrcode.callback = function (result) {
        if (!jendo.isNull(qrcodeSuccessEvent)) {
            qrcodeSuccessEvent(result, qrcReader.data('stream'));
        }
    };
}

JendoQRCodeReader.prototype.stop = function () {
    var stream = this._QRCodeReader.data('stream');
    if (!jendo.isNull(stream)) {
        stream.getVideoTracks().forEach(function (videoTrack) {
            videoTrack.stop();
        });
        this._QRCodeReader.dataRemove('stream');
    }
}

JendoQRCodeReader.prototype.files = function (value) {
    var qrcReader = this._QRCodeReader;
    var height = (this._Height == null) ? 250 : this._Height;
    var width = (this._Width == null) ? 300 : this._Width;
    var qrcodeSuccessEvent = this._OnSuccess;
    var qrcodeErrorEvent = this._OnError;
    var qrcodeVideoErrorEvent = this._OnVideoError;
    // video елемента
    var vidElem = this._QRCodeReader.find('#qr-video');
    if (vidElem.length > 0) {
        vidElem.hide();
    }
    // създава canvas елемента
    var canvasElem = this._QRCodeReader.find('#qr-canvas');
    if (canvasElem.length == 0) {
        canvasElem = this._QRCodeReader.appendElement('canvas')
            .height(height - 2)
            .width(width - 2)
            .attr('id', 'qr-canvas');
    }
    else {
        canvasElem.show();
    }

    var video = vidElem.first();
    var canvas = canvasElem.first();
    var context = canvas.getContext('2d');
    // Резултат от декодирането
    qrcode.callback = function (result) {
        if (!jendo.isNull(qrcodeSuccessEvent)) {
            qrcodeSuccessEvent(result, qrcReader.data('stream'));
        }
    };
    // Зарежда снимката
    var img = new Image();
    img.onload = function () {
        img.width = context.canvas.width;
        img.height = context.canvas.height;
        context.drawImage(img, 0, 0
            , context.canvas.width, context.canvas.height
        );
    }
    // Обхожда всички файлове
    for (var i = 0; i < value.length; i++) {
        var reader = new FileReader();
        reader.onload = (function (imgFile) {
            return function (imgReader) {
                try {
                    qrcode.decode(imgReader.target.result);
                    img.src = imgReader.target.result;
                } catch (e) {
                    if (!jendo.isNull(qrcodeErrorEvent)) {
                        qrcodeErrorEvent(e, mediaStream);
                    }
                }
            };
        })(value[i]);
        reader.readAsDataURL(value[i]);
    }
}

jendo.QRCodeReader = function (element) {
    return new JendoQRCodeReader(element);
}
/* END: QR Code Reader
*/
